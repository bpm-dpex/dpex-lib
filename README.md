Disclamer: The purpose of the project in its current state is to demonstrate the feasibility of the approach presented in the paper.  
Additional information of the paper will be included in the README file later.
# dpex-lib:  A library for decentralized process execution

This repository contains the **dpex-lib** project, a Java library for **d**ecentralized **p**rocess **ex**ecution that implements the conceptual architecture presented in the paper [t.b.d.]. **dpex-lib** is Java library, designed for the usage in Spring Boot applications. 

To see the **dpex-lib** in action, please visit the [**dpex-demo**](https://gitlab.com/bpm-dpex/dpex-demo) Demo Project.

# Getting Started

To use the dpex-lib, you can either [use the pre-build artefact](##Use-the-pre-build-library-in-your-spring-boot-project) that is available in the package registry of this project, or you can [build the artefact manually](##build-the-artefact-manually), described next.

## Build the artefact manually
The following steps guide through the process of manually build the source code and publish the build artefact to your local file system. This locally available artefact can then be integrated in any gradle project. Therefore, declare your local repository  in the ```build.gradle``` file: 
```
repositories {
		mavenLocal()
}
```
Then you can add the **dpex-lib** as dependency:
```
dependencies {
	implementation 'ubt.ai4:dpex:0.1.1'
}
```

### Requirements

- Java 17

### Build the Project 
- Clone the repository to your local system
	> ```git clone https://gitlab.com/bpm-dpex/dpex-lib.git```
- Build the project with
	> ```./gradlew build```
- Export the project to your local maven repository
	> ```./gradlew publishToMavenLocal```

The build artefacts of the dpex-lib project are now available in your local file system in the folder ```%HOMEPATH%\.m2\repository\ubt\ai4\dpex```.

## Use the pre-build library  in your Spring Boot Project

The pre-build build artefact is available in the package registry of this project. To use this artefact, you have to add the respective repository in your Spring Boot application:
```
repositories {
		maven {
			name = 'gitlab'
			url "https://gitlab.com/api/v4/projects/44730387/packages/maven"
			credentials(HttpHeaderCredentials) {
				name = "Job-Token"
				value = System.getenv("CI_JOB_TOKEN")
			}
			authentication {
				header(HttpHeaderAuthentication)
			}
		}
	}
```

# Structure of the Project

The most important packages of the **dpex-lib** project include ```dpex.bpm```, ```dpex.collaboration```, ```dpex.core``` and ```dpex.impl```. Their core classes and their role in dpex are described next.

### ```dpex.bpm``` 
t.b.d.
### ```dpex.collaboration``` 
t.b.d.
### ```dpex.core``` 
t.b.d.
### ```dpex.impl```
t.b.d.

# The communication module

In this readme the functioning of the communication module will be explained with the Matrix protocol as its underlying implementation in mind to make it more exemplary. However, new protocols can be integrated by extending the abstract ```Communication``` class. The framework is therefore not limited to Matrix.

## Using the matrix protocol in dpex

### Prerequisites

* The Javascript Matrix-Adapter must be running (see https://gitlab.com/bpm-dpex/dpex-communication-matrix-adapter)
* Every participant needs an own matrix account. Every organization or participant is free to choose a homeserver they wish or to set up an own one.
* All participants of the project must be saved in the user repository when creating the process model. Each user has to have a unique communication ID (i.e. Matrix ID), if necessary a homeserver URL, BPM ID and SCI ID. The communication IDs will only be used for the messaging via the communication protocol itself. The user interaction will be based on the BPM IDs.
* It is considered undefined behavior to use Matrix accounts in a dpex application that have not been assigned to a process participant beforehand.

### Logging in

* The ```login()``` function accepts a communication ID (i.e. Matrix ID), a password, if applicable the URL of the homeserver and the name of the communication module.
* Its underlying implementation will connect the user to his Matrix account and return an access token.
* This access token will be necessary to authenticate and authorize the user for all subsequent requests that use the matrix protocol.

### Preparing the communication module

* First the connector for the communication module has to be created
* In order to execute functions of the communication module, it is mandatory to create the alliance first because the communication protocol is only chosen at the time when the alliance is being created.
* Either the global alliance reference (gar) or the name of the communication module must be sent with every request (depending on the request) to the communication module in order to determine which one to use.
* Each alliance corresponds to a space in Matrix. One participant of the alliance is responsable for creating that space after the alliance has been created. The gar is saved in the description of the space to identify it clearly with an alliance.
* The Javascript backend will therefore prevent a creation of a second space for the same alliance. At this point it is important not to use an account that has not been specified in the organizational model!
* The space ID is now available to all participants of the alliance by calling the ```getRooms()``` method of the ```DPEXCommunicationService```.
* That one will be needed to create a new room in the space.
* It is assumed that if a user is member of multiple alliances, then his BPM ID has to be identical in every alliance.


# Hyperledger Network Test Setup

The Network was build with these Versions:
Hyperledger Fabric: **v2.5.3**
Docker Image Version: v2.5.3
CA local Version: v1.5.6
CA docker version: v1.5.6

The fabric version is the most important, if the network is cloned via the git link the rest of the versions are already matching

The prerequisites that needs to be taken are listed in https://hyperledger-fabric.readthedocs.io/en/release-2.2/prereqs.html

Please make sure to Install Git, Docker and docker-compose be before you start with the setup.
For Windows Users: Make Sure to enable WSL2 Support in the Docker settings and get a Ubuntu Linux Subsystem running
(e.g. with this Microsoft App: https://apps.microsoft.com/store/detail/ubuntu-22042-lts/9PN20MSR04DW?hl=de-de&gl=de&rtc=1)

- Create a directory for the chaincode
- Clone the chaincode repository > link to repo

  > git clone <link>

- In the Commandline of the project folder

  > gradlew clean build install

- Switch into Linux system and create a new directory for the hyperledger network
- Clone the fabric samples into the desired folder

  > git clone https://github.com/hyperledger/fabric-samples.git

- Install the test network

  > ./install-fabric.sh docker samples binary

- Switch into the /fabric-samples folder
- Start test network with authentication

  > ./network.sh up -ca

- Create Channel for the two peers (-c channelName optional, default value "mychannel" is implemented in the Hyperledger Adapter)

  > ./network.sh createChannel 

- Install Chaincode on the network, **make sure the path to the Hyperledger Chaincode folder is set correctly**

  > ./network.sh deployCC -ccn HyperledgerChaincode -ccp /mnt/c/Users/username/dpex/Hyperledger-chaincode -ccl java

- No instantiation of the chaincode is necessary. You can now start using the Hyperledger Adapter in the dpex library.