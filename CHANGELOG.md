
# Changelog

## [v0.2] (09.02.2024)
**BFT-SMaRt Support ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/25))**

Features:
- **BFT-SMaRt Adapter:** Added a new Adapter for `Collaboration` as secure communication infrastructure. The dpex framework has been enhanced to operate independently of a Blockchain environment. For the `BFT-SMaRt`-Adapter, also the `BFTSMaRtConfig`, `BFTSMaRtFactory` and `BFTSMaRtNetwork` were implemented.
- **Robust Restart**: The `Ethereum`- and `BFT-SMaRt`-Adapter now both support application restart, in particular events propagated during downtime are processed after restart, event handlers are re-registered and all data can be persisted in an external database.
- **Transactional Finality**: When the Ethereum-Adapter is linked to a Proof of Work-driven Ethereum Blockchain, dpex encounters the challenge of probabilistic finality, where transactions included in a newly mined block could be reversed due to blockchain forks. To mitigate this, it is advisable to delay the confirmation of a transaction's permanence in the blockchain by waiting for the addition of subsequent blocks. In dpex, this waiting period, quantified by the number of blocks, is now configurable, allowing for a transaction to be assessed and processed by a BPM-Adapter only after this predefined interval.

Minor Improvements:
- **Camunda Adapter**: The dynamically configured camundaUrl is used for process model deployment instead of a hard-coded url.
- **Activity Life Cycle**: Added an activity life cycle attribute to the `Event` class.
- **Auth Header**: `BaseNetwork` supports the definition of an auth header, e.g. for the usage with hosted Ethereum clients such as [`kaleido`](https://kaleido.io)
- **Communication Adapter**: The usage of a communication adapter is now optional for alliances.



## [v0.1.1] (13.10.2023)
**Communication in dpex ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/8))**

Features:
- **Alliance:** The Alliance encapsulates a new artefact, i.e. `Communication`.
- **Matrix Adapter**: An implementation of the new `Communication` artefact that communicates with the [`dpex-communication-matrix-adapter`](https://gitlab.com/bpm-dpex/dpex-communication-matrix-adapter)
- **Discord Adapter**: An implementation of the new `Communication` artefact that communicates with the [`dpex-communication-discord-adapter`](https://gitlab.com/bpm-dpex/dpex-communication-discord-adapter) 

Minor Improvements:
- **User Management**: Introduced 3-way user identification (bpmId, sciId, communicationId)
- **Added business key to instantiation**: Name the newly created instance
- **Event-driven architecture**: Added event bus that can be used in implementing applications, for example, to notify the frontend on events using an established web socket connection
- **Added enums for available adapter**: These enums can be used to display available configurable adapters in the frontend


## [v0.0.12] (28.09.2023)
**Hyperledger Connector ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/3))**

Features:
- **HyperledgerAdapter:** Added a new Adapter for `Collaboration` as secure communication infrastructure. For the `Hyperledger`-Adapter, also the `HyperledgerConfig`, `HyperledgerFactory` and `HyperledgerNetwork` was implemented.


## [v0.0.11] (26.08.2023)
**Informational Perspective ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/4))**

Features:
- **dpex:** Added support for data (in `ProcessVariables`)
- **BPMEngine:** add new functionality for retrieving and deleting process variables
- **CamundaAdapter:** Updated to the new `BPMEngine` and send process variables


## [v0.0.10] (22.08.2023)
**Camunda deploy process model ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/7))**

Features:
- **Camunda Adapter:** The Process Model is automatically deployed to the Camunda Engine when the Factory produces a `CamundaEngine`-Instance by calling `create(ProcessModel processModel);`



## [v.0.0.9] (20.08.2023)
**Flexible SCI Configuration ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/5))**

Features:
- **SCI:** The `CollaborationFactory` now uses a flexible `SCIConfig` object to configure the Collaboration instead of a raw `String`.
- **Ethereum Adapter**: The `EthereumRawFactory` was updated to use the new `SCIConfig` class. 

## [v0.0.8] (17.07.2023)
**SCI for Alliance ([Merge Request](https://gitlab.com/bpm-dpex/dpex-lib/-/merge_requests/1))**

Features:
- **SCI:** Instead of using explicit REST requests to dpex instances of collaborators for notifying on new process instances, this is now broadcasted directly over the SCI.
- **Network**: The `BaseNetwork` is now persisted as `Entity` in the database.  
- **Smart Contracts for Ethereum**: A new Smart Contract (*SCI Contract*) was introduced that handles the communication of process model instantiations. The *Process Instance Contract* will now handle all actions w.r.t. a single process instance. 
- **Ethereum Adapter:** In the constructor of the `EthereumNetwork`, an event listener to the *SCI Contract* is registered. Additionally, in the `handleInstantiation()` method, an event listener to the *Process Instance Contract* is registered.
- **Setup**: dpex expects that the *SCI Contract* is already deployed, when a new Alliance is defined.

