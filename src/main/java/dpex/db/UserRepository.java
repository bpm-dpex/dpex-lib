package dpex.db;

import dpex.core.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByBpmId(String bpmId);
    Optional<User> findByCommunicationId(String communicationId);
    Optional<User> findFirstByCommunicationId(String communicationId);
}
