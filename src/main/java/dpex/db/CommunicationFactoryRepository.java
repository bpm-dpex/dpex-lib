package dpex.db;

import dpex.communication.CommunicationFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunicationFactoryRepository extends JpaRepository<CommunicationFactory, String> {
}
