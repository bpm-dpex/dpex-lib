package dpex.db;

import dpex.bpm.model.ProcessModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessModelRepository extends JpaRepository<ProcessModel, String> {
}
