package dpex.db;

import dpex.collaboration.network.BaseNetwork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NetworkRepository extends JpaRepository<BaseNetwork, Integer> {
}
