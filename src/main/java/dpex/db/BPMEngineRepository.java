package dpex.db;

import dpex.bpm.execution.BPMEngine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BPMEngineRepository extends JpaRepository<BPMEngine, Long> {
}
