package dpex.db;

import dpex.core.Initializer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InitializerRepository extends JpaRepository<Initializer, Long> {
}
