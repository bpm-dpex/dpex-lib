package dpex.db;

import dpex.bpm.model.ProcessModelFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessModelFactoryRepository extends JpaRepository<ProcessModelFactory, String> {
}
