package dpex.db;

import dpex.bpm.execution.BPMEngineFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BPMEngineFactoryRepository extends JpaRepository<BPMEngineFactory, String> {
}
