package dpex.db;

import dpex.collaboration.CollaborationFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollaborationFactoryRepository extends JpaRepository<CollaborationFactory, String> {
}
