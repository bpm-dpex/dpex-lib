package dpex.db;

import dpex.bpm.execution.Instance;
import jnr.ffi.annotations.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InstanceRepository extends JpaRepository<Instance, String> {

    public Optional<Instance> findByLocalInstanceReference(String localInstanceReference);
    public Optional<Instance> findByGlobalInstanceReference(String globalInstanceReference);


}
