package dpex.db;

import dpex.core.Alliance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface AllianceRepository extends JpaRepository<Alliance,String>{

    //@Query("SELECT a FROM Alliance a JOIN ProcessModel bm ON a.model.id = bm.id WHERE bm.modelReference = ?1")
    //Optional<Alliance> findByModelReference(String modelReference);

    @Query("SELECT a FROM Alliance a JOIN BPMEngine bem ON a.engine.id = bem.id JOIN Instance i ON i MEMBER OF bem.instances WHERE i.localInstanceReference = ?1")
    Optional<Alliance> findByLocalInstanceReference(String localInstanceReference);

    @Query("SELECT a FROM Alliance a JOIN BPMEngine bem ON a.engine.id = bem.id JOIN Instance i ON i MEMBER OF bem.instances WHERE i.globalInstanceReference = ?1")
    Optional<Alliance> findByGlobalInstanceReference(String globalInstanceReference);

    @Query("SELECT a FROM Alliance a WHERE LOWER(a.globalAllianceReference) = LOWER(:globalAllianceReference)")
    Optional<Alliance> findByGlobalAllianceReference(String globalAllianceReference);

    @Query("SELECT a FROM Alliance a JOIN Collaboration bc ON a.collaboration.id = bc.id WHERE bc.type = ?1")
    Set<Alliance> findByCollaborationType(String collaborationType);


}
