package dpex.db;

/**
 * Utility class for managing the tenant context in a multi-tenant application. It uses
 * thread-local storage to hold the tenant identifier (tenantId) for the duration of a request
 * or processing thread. This allows different parts of the application, including data access
 * and security components, to access tenant-specific configurations or perform tenant-specific
 * operations transparently, based on the current tenant context.
 *
 * The tenantId is typically set at the beginning of request processing, based on the authentication
 * token or request parameters, and cleared at the end of the request to prevent leakage of tenant
 * context across different requests handled by the same thread. To not lose tenant context over asynchronous operations
 * like Blockchain event Listeners, it is saved in the {@link dpex.impl.collaboration.ethereum.raw.EthereumRawNetwork}
 * and {@link dpex.impl.collaboration.bftsmart.BftSmartNetwork}.
 */
public class TenantContext {
    private static final ThreadLocal<String> currentTenant = new ThreadLocal<>();

    /**
     * Sets the tenant identifier (tenantId) for the current thread. This should be called
     * at the beginning of request processing or whenever the tenant context changes.
     *
     * @param tenantId The identifier of the current tenant. This is usually extracted from
     *                 the authentication token or determined by some request-specific logic.
     */
    public static void setTenantId(String tenantId) {
        currentTenant.set(tenantId);
    }

    /**
     * Retrieves the tenant identifier (tenantId) for the current thread. This can be called
     * by any part of the application that needs to perform tenant-specific operations or
     * access tenant-specific data.
     *
     * @return The identifier of the current tenant, or null if no tenant context has been set.
     */
    public static String getTenantId() {
        return currentTenant.get();
    }

    /**
     * Clears the tenant context from the current thread. This should be called at the end
     * of request processing to prevent the tenant context from affecting subsequent requests
     * handled by the same thread.
     */
    public static void clear() {
        currentTenant.remove();
    }
}

