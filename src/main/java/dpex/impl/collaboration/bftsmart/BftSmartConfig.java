package dpex.impl.collaboration.bftsmart;

import dpex.collaboration.SCIConfig;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Configuration class for integrating with the BFT-SMaRt consensus framework.
 * This class extends the SCIConfig to provide specific configurations needed for BFT-SMaRt operations.
 *
 * Attributes:
 * - clientId: An integer value used as the unique identifier for the client in the BFT-SMaRt framework.
 *   This ID is essential for the client's interaction with the replicated state machines, as it differentiates
 *   each client from others in the system.
 *
 * - ipConfig: A list of strings representing the IP addresses and port numbers of the replicas in the BFT-SMaRt
 *   system. Each string in the list should follow the format "IP PORT1 PORT2", where IP is the IP address of a
 *   replica, PORT1 is the port number for regular client-replica communication, and PORT2 is used for communication
 *   between replicas.
 *
 * The EthereumNetwork class utilizes this configuration to dynamically update the 'config/hosts.config' and
 * 'config/system.config' files, ensuring the system's network settings are loaded correctly by BFT-SMaRt.
 *
 * @author Moritz Johlige
 * @version 0.2
 */
@Data
@Builder
public class BftSmartConfig extends SCIConfig {
    private int clientId;
    private String globalAllianceReference;
    private List<String> ipConfig;
    private boolean useDefaultKeys;
    private List<String> publicKeys;
    private String clientPrivateKey;
    private String clientPublicKey;
    private String allianceName;
}
