package dpex.impl.collaboration.bftsmart;

import dpex.collaboration.Collaboration;
import lombok.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Entity;

/**
 * The {@code BftSmart} class represents an implementation of the {@link Collaboration} interface
 * using the BFT-SMaRt framework to provide consensus.
 *
 * @author Moritz Johlige
 * @version 0.2
 */
@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
public class BftSmart extends Collaboration {

    private static final Logger logger = LogManager.getLogger(dpex.impl.collaboration.bftsmart.BftSmart.class);

}
