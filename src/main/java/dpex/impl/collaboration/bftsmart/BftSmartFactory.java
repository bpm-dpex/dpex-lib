package dpex.impl.collaboration.bftsmart;

import dpex.collaboration.CollaborationFactory;

import dpex.collaboration.SCIConfig;
import dpex.db.TenantContext;
import dpex.impl.collaboration.consensus.TrueConsensus;
import dpex.impl.collaboration.security.NoSecurity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * The {@code BftSmartFactory} class serves as a factory for creating {@link BftSmart} instances,
 * which are specific implementations of the {@link dpex.collaboration.Collaboration} for the BFT-SMaRt framework.
 * This factory manages the instantiation process, ensuring that each {@link BftSmart} instance is configured
 * with the correct {@link BftSmartNetwork} and security settings.
 * @author Moritz Johlige
 * @version 0.2
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class BftSmartFactory  extends CollaborationFactory{

    private static final Logger logger = LogManager.getLogger(dpex.impl.collaboration.bftsmart.BftSmartFactory.class);

    boolean useDefaultKeys;
    @ElementCollection
    List<String> publicKeys;
    String clientPrivateKey;
    String clientPublicKey;

    public BftSmart create(SCIConfig sciConfig) {
        if(!(sciConfig instanceof BftSmartConfig)) {
            throw new RuntimeException("The configuration object must be of type BftSmartConfig.");
        }
        BftSmartConfig config = (BftSmartConfig) sciConfig;
        logger.info("Create new Bft Smart Collaboration with client id:\t" + config.getClientId());
        BftSmartNetwork bftn = new BftSmartNetwork(config.getClientId(), config.getIpConfig(), TenantContext.getTenantId(), config.getGlobalAllianceReference(), config.getPublicKeys(), config.getClientPrivateKey(), config.getClientPublicKey(), config.isUseDefaultKeys(), config.getAllianceName());
        BftSmart bft = BftSmart.builder().build();
        bft.setNetwork(bftn);
        bft.setSecurity(NoSecurity.class);
        bft.setConsensus(TrueConsensus.class);

        bft.setName(super.getName());

        // A connection URL is not set in this BftSmart Instance.
        // As it is inherited from Collaboration it would have to be a single valid URL. However, BftSmart uses multiple
        // connection URLs. BFT Smart reads all connection URLs from config/hosts.config and config/system.config, which
        // are changed according to config.getIpConfig() on demand by BFTSmartNetworks method updateConfiguration

        return bft;
    }
}
