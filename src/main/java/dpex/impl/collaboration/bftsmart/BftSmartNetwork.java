package dpex.impl.collaboration.bftsmart;


import bftsmartcommunicationtypes.ProcessInstancePM;
import bftsmartcommunicationtypes.RequestType;
import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.network.BaseNetwork;
import dpex.core.*;
import dpex.db.TenantContext;
import dpex.util.EncryptionUtil;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


import bftsmart.tom.ServiceProxy;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import bftsmartcommunicationtypes.PEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;


/**
 * Represents a network configuration and interaction layer for the BFT-SMaRt consensus protocol, extending the
 * BaseNetwork class functionalities to provide integration with BFT-SMaRt specific operations. It manages the
 * network's instances, events, and consensus activities, ensuring secure and fault-tolerant communication and
 * process instantiation within the BFT SMaRt network. This class includes mechanisms for event polling, task
 * action handling, and dynamic network configuration.
 *
 * @author Moritz Johlige
 * @version 0.2
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class BftSmartNetwork extends BaseNetwork {
    /**
     * Mutexes in this class are crucial for ensuring thread safety and preventing issues during the reconstruction
     * of BftSmartNetwork instances from the database. Given the potential for simultaneous creation of multiple
     * instances, there's a risk of duplicating resources, such as timers, which could then poll the same process
     * instance concurrently. This redundancy could lead to inefficient resource use and potential conflicts.
     * Employing mutexes ensures that each process instance is polled by only one timer, regardless of the number
     * of BftSmartNetwork instances instantiated.
     */


    // Security provider for cryptographic operations
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private static final Logger logger = LogManager.getLogger(dpex.impl.collaboration.bftsmart.BftSmartNetwork.class);

    // Inner class representing a timer task used to poll new events fo a specific process instance that can be saved
    // to the database
    @Entity
    @NoArgsConstructor
    public static class NewInstancePollerSerializable {
        public String processIdToPoll;
        public int delay;
        public int period;

        public String networkString;
        public String tenantID;
        public String allianceName;


        public NewInstancePollerSerializable(String processIdToPoll, int delay, int period, String networkString, String tenantID, String allianceName) {
            this.processIdToPoll = processIdToPoll;
            this.delay = delay;
            this.period = period;
            this.networkString = networkString;
            this.tenantID = tenantID;
            this.allianceName = allianceName;
        }

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

    }

    // Timer task for polling new events of instances in the network, a new process instance gets created -> a new timer
    // task will be created
    private class NewInstancePoller extends TimerTask {

        private String processIdToPoll;
        private int delay;
        private int period;

        private String networkString;
        private String tenantID;
        private String allianceName;


        public NewInstancePoller(String processIdToPoll, int delay, int period, String networkString, String tenantID, String allianceName) {
            this.processIdToPoll = processIdToPoll;
            this.delay = delay;
            this.period = period;
            this.networkString = networkString;
            this.tenantID = tenantID;
            this.allianceName = allianceName;
        }

        public String getProcessIdToPoll() {
            return processIdToPoll;
        }

        public int getDelay() {
            return delay;
        }

        public int getPeriod() {
            return period;
        }


        public String getNetworkString() {
            return networkString;
        }

        public String getTenantID() {
            return tenantID;
        }

        public String getAllianceName() {
            return allianceName;
        }

        public boolean isCancelled() {
            return this.scheduledExecutionTime() <= 0;
        }


        public void run() {
            logger.info("Polling new events for instance: " + processIdToPoll);
            Map<Integer, PEvent> current_eventlist = getAllEventsFromInstance(processIdToPoll);
            if (current_eventlist != null) {
                if (!current_eventlist.isEmpty()) {
                    for (Integer id : current_eventlist.keySet()) {
                        if (alreadyNoticedEvents.get(tenantID + allianceName).get(processIdToPoll) == null || !alreadyNoticedEvents.get(tenantID + allianceName).get(processIdToPoll).contains(id)) {
                            TenantContext.setTenantId(tenantIdForThisNetworkInstance);
                            DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);
                            dpexService.acceptTaskActionAndSaveToDB(globalAllianceReference, id, current_eventlist, processIdToPoll);
                            TenantContext.clear();
                        }
                    }
                }
            }
        }
    }

    /**
     * Add a newly noticed event to the class field to prevent reprocessing. Updates both transient and serialized representations.
     * This method ensures that newly noticed events are tracked accurately across instances, even after a restart or reload
     * from database.
     *
     * @param id the ID of the new event that has been noticed
     * @param current_eventlist a map of event IDs to PEvent objects, representing the current list of events for a process instance
     * @param processIdToPoll the process instance identifier to which the event belongs
     */
    public void addNoticedEvent(int id, Map<Integer, PEvent> current_eventlist, String processIdToPoll) {
        if (alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).get(processIdToPoll) != null) {
            alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).get(processIdToPoll).add(id);
        } else {
            alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).put(processIdToPoll, new TreeSet<>());
            alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).get(processIdToPoll).add(id);
        }
        if (alreadyNoticedEventsSerializable.get(processIdToPoll) != null) {
            alreadyNoticedEventsSerializable.get(processIdToPoll).add(id);
        } else {
            alreadyNoticedEventsSerializable.put(processIdToPoll, new TreeSet<>());
            alreadyNoticedEventsSerializable.get(processIdToPoll).add(id);
        }
        loadNoticedEventsIntoSerializedForm();
        handleTaskAction(current_eventlist.get(id));
    }


    // Unique client identifier within the BFT-SMaRt network, has to be unique for every BftSmart class instance
    private int clientIdf;

    // GlobalAllianceReference of the alliance containing the network
    String globalAllianceReference;

    // Maps BFT SMaRt network connections to their corresponding ServiceProxy for communication with the BFT SMaRt Replicas
    @Transient
    private static Map<String, ServiceProxy> serviceProxies = new TreeMap<>();
    @Transient
    private static final Object serviceProxyMutex = new Object();

    // Maps BFT SMaRt network connections to their corresponding timer for polling new events for process instances represented
    // by timer tasks
    @Transient
    public static Map<String, Timer> polling_timers = new TreeMap<>();
    @Transient
    private static Map<String, Object> pollingTimerMutexes = new TreeMap<>();

    // Tracks events that have already been noticed to avoid reprocessing
    // Keys of first Map: tenantID + alliance name (globally unique per alliance)
    // Keys of inner Map: processID of the process instance in this alliance
    @Transient
    private static Map<String, Map<String, TreeSet<Integer>>> alreadyNoticedEvents = new TreeMap<>();
    @Transient
    private static Map<String, Object> alreadyNoticedEventsMutexes = new TreeMap<>();


    // Timer Task Map, that contains all the Timer Tasks for querying updates of specific process instances
    // Key: tenantID + allianceName + processIdToPoll (globally unique per process instance)
    @Transient
    private static final Map<String, NewInstancePoller> pollers = new HashMap<>(); // per allianceName


    // List that can be saved to the database that includes all the necessary information to re-setup the Timer Tasks
    // and Timers that poll new events for this network's alliance's the process instances
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<NewInstancePollerSerializable> pollersSerializable; // More like pollers savable similar to alreadyNoticedEventsSerialized;

    // Serialized form of noticed events for persistence in the database
    @Lob
    private byte[] alreadyNoticedEventsSerialized;
    // Version of alreadyNoticedEvents that can be serialized
    @Transient
    private Map<String, TreeSet<Integer>> alreadyNoticedEventsSerializable; // Indirectly maps to a specific BFT Smart Network Instance since not static

    // Tracks already noticed instantiations for this network's alliance
    @ElementCollection
    private Set<String> instantiations;

    public void add_instantiation(String inst) {
        instantiations.add(inst);
    }

    // A list of strings representing the IP addresses and port numbers of the replicas in the BFT-SMaRt
    // system. Each string in the list should follow the format "IP PORT1 PORT2", where IP is the IP address of a
    // replica, PORT1 is the port number for regular client-replica communication, and PORT2 is used for communication
    // between replicas.
    @ElementCollection
    private List<String> ip_configuration;

    // A list of strings representing the public keys of each replica
    @ElementCollection
    private List<String> publicKeys;

    @Column(columnDefinition = "TEXT")
    private String clientPrivateKey;
    private String clientPublicKey;
    private boolean useDefaultKeys;
    // clientID + allIpAddresses of the BFT SMaRt Network
    public String networkString;
    private String allianceName;

    @Value("${app.encryption-key}")
    private String encryptionKey;
    @Transient
    private EncryptionUtil encryptionUtil;

    /**
     * Updates the encryption key. Retrieves the key from application.properties without using Spring
     * as Springs auto-injection will only run after constructor, but this key is needed within the constructor.
     *
     * @return the encryption key loaded from the configuration
     * @throws RuntimeException if the encryption key could not be loaded due to file access issues
     */
    private String loadEncryptionKey() {
        try {
            Properties props = PropertiesLoaderUtils.loadProperties(new ClassPathResource("application.properties"));
            return props.getProperty("app.encryption-key");
        } catch (IOException e) {
            throw new RuntimeException("Failed to load encryption key from properties file", e);
        }
    }


    // Used to create a new BftSmartNetwork in the corresponding factory
    public BftSmartNetwork(int clientId, List<String> hosts, String tenantIdForThisNetworkInstance, String globalAllianceReference, List<String> publicKeys, String clientPrivateKey, String clientPublicKey, boolean useDefaultKeys, String allianceName) {
        logger.info("Instantiate new BFTSmart Network not from a JPA Repository.");
        clientIdf = clientId;
        ip_configuration = hosts;
        pollersSerializable = new ArrayList<>();
        alreadyNoticedEventsSerializable = new TreeMap<>();
        this.globalAllianceReference = globalAllianceReference;
        super.tenantIdForThisNetworkInstance = tenantIdForThisNetworkInstance;
        this.clientPublicKey = clientPublicKey;
        this.publicKeys = publicKeys;
        this.useDefaultKeys = useDefaultKeys;
        networkString = clientId + hosts.toString();
        this.allianceName = allianceName;
        encryptionUtil = new EncryptionUtil(loadEncryptionKey());
        if (!useDefaultKeys) {
            try {
                this.clientPrivateKey = encryptionUtil.encrypt(clientPrivateKey);
            } catch (Exception e) {
                logger.error("Could not encrypt the client private Key in BftSmartNetwork.");
            }
        }
        reinitialize_after_deserialization();
    }

    /**
     * Checks if an alliance with the specified global alliance reference already exists within the BFT-SMaRt network.
     * If the alliance is found, this method will handle the instantiation of each found process instance
     * that is not already instantiated in the current context. This is crucial to ensure synchronization
     * across different instances.
     */
    public void checkForExistingAlliance() {
        // Check if the alliance already exists on the BFTSmartNetwork and if handle its instantiations
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);) {
            objOut.writeObject(RequestType.CHECK_FOR_ALLIANCE_EXISTENCE);
            objOut.writeObject(globalAllianceReference);
            objOut.flush();
            byteOut.flush();
            byte[] reply = serviceProxies.get(tenantIdForThisNetworkInstance + networkString).invokeUnordered(byteOut.toByteArray());
            if (reply.length == 0)
                logger.info("No existing alliance found");
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
                 ObjectInput objIn = new ObjectInputStream(byteIn)) {
                List<ProcessInstancePM> foundInstancesForAlliance = (ArrayList<ProcessInstancePM>) objIn.readObject();
                for (ProcessInstancePM inst : foundInstancesForAlliance) {
                    if (!instantiations.contains(inst.getGlobalInstanceReference())) {
                        setupInstancesOfExistingAlliance(inst);
                    }
                }

            }
        } catch (IOException | ClassNotFoundException | RuntimeException e) {
            logger.error("Exception getting events: " + e.getMessage());
        }
    }

    /**
     * Handles the setup of existing alliances by initializing necessary components such as polling timers and
     * ensuring events are properly noticed and processed. This includes the instantiation of process instances
     * and setting up the polling mechanism to continuously monitor for new events. It also saves these instances
     * to the database for persistence and consistent state management.
     *
     * @param instance the process instance metadata retrieved from the network which needs to be set up
     */
    private void setupInstancesOfExistingAlliance(ProcessInstancePM instance) {
        handleInstantiation(instance);
        String processId = instance.getGlobalInstanceReference();
        alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).put(processId, new TreeSet<>());
        alreadyNoticedEventsSerializable.put(processId, new TreeSet<>());
        ApplicationContextProvider.bean(DPEXService.class).acceptInstantiationAndSaveToDB(instance.getGlobalAllianceReference(), processId);
        NewInstancePoller temp_pol = new NewInstancePoller(processId, 0, 10000, networkString, tenantIdForThisNetworkInstance, allianceName);
        polling_timers.get(tenantIdForThisNetworkInstance + networkString).scheduleAtFixedRate(temp_pol, temp_pol.getDelay(), temp_pol.getPeriod());
        pollers.put(tenantIdForThisNetworkInstance + allianceName + processId, temp_pol);
        pollersSerializable.add(new NewInstancePollerSerializable(processId, temp_pol.getDelay(), temp_pol.getPeriod(), networkString, tenantIdForThisNetworkInstance, allianceName));

    }

    // Only used for reconstruction from a JPA repository
    public BftSmartNetwork() {
    }


    /**
     * Reinitializes transient and non-serializable fields after the BftSmartNetwork is loaded from the database.
     * Ensures that service proxies, polling timers, client keys for the BFTSMaRt client and noticed events are correctly
     * setup to maintain network operation continuity after a restart.
     */
    private void reinitialize_after_deserialization() {
        logger.info("Reinitialize with clientID: " + clientIdf);
        // Reload the noticed events that have been serialized for the current Client ID
        if (!alreadyNoticedEventsMutexes.containsKey(tenantIdForThisNetworkInstance + allianceName)) {
            alreadyNoticedEventsMutexes.put(tenantIdForThisNetworkInstance + allianceName, new Object());
        }
        synchronized (alreadyNoticedEventsMutexes.get(tenantIdForThisNetworkInstance + allianceName)) {
            if (!alreadyNoticedEvents.containsKey(tenantIdForThisNetworkInstance + allianceName)) {
                alreadyNoticedEvents.put(tenantIdForThisNetworkInstance + allianceName, new HashMap<>());
            }
            if (alreadyNoticedEventsSerializable != null && !alreadyNoticedEventsSerializable.isEmpty()) {
                for (Map.Entry<String, TreeSet<Integer>> entry : alreadyNoticedEventsSerializable.entrySet()) {
                    String key = entry.getKey();
                    TreeSet<Integer> values = entry.getValue();
                    for (Integer value : values) {
                        // Check if the target map contains the key
                        if (!alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).containsKey(key)) {
                            // If not, create a new TreeSet and add the value
                            alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).put(key, new TreeSet<>());
                        }
                        // Since clientID will be different for different Clients, it is no problem to not differentiate them when saving to the database
                        TreeSet<Integer> targetValues = alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).get(key);
                        if (!targetValues.contains(value)) {
                            targetValues.add(value);
                        }
                    }
                }
            }

        }

        if (!pollingTimerMutexes.containsKey(tenantIdForThisNetworkInstance + networkString)) {
            pollingTimerMutexes.put(tenantIdForThisNetworkInstance + networkString, new Object());
        }


        synchronized (serviceProxyMutex) {
            updateConfiguration(ip_configuration, useDefaultKeys);
            if (!useDefaultKeys) {
                try {
                    String unencryptedPrivateKey = encryptionUtil.decrypt(clientPrivateKey);
                    writeKeys(publicKeys, unencryptedPrivateKey, clientPublicKey, clientIdf);
                } catch (Exception e) {
                    logger.error("Could not decrypt client private Key.");
                }
            }
            if (!serviceProxies.containsKey(tenantIdForThisNetworkInstance + networkString)) {
                serviceProxies.put(tenantIdForThisNetworkInstance + networkString, new ServiceProxy(clientIdf));
            }
        }

        // Create Timer for current clientId if it doesn't exist yet
        synchronized (pollingTimerMutexes.get(tenantIdForThisNetworkInstance + networkString)) {
            if (!polling_timers.containsKey(tenantIdForThisNetworkInstance + networkString)) {
                polling_timers.put(tenantIdForThisNetworkInstance + networkString, new Timer());
            }
        }

        // Add Timer Tasks for ALL ClientIDs after deserialization if they have not yet been added
        synchronized (pollers) {
            if (!pollersSerializable.isEmpty()) {
                for (NewInstancePollerSerializable curr_poll : this.pollersSerializable) {
                    if (!pollers.containsKey(curr_poll.tenantID + curr_poll.allianceName + curr_poll.processIdToPoll)) {
                        pollers.put(curr_poll.tenantID + curr_poll.allianceName + curr_poll.processIdToPoll, new NewInstancePoller(curr_poll.processIdToPoll, curr_poll.delay, curr_poll.period, curr_poll.networkString, curr_poll.tenantID, curr_poll.allianceName));
                    }
                }
            }
        }
        synchronized (pollers) {
            if (!pollers.isEmpty()) { // has to stop existing pollers on every reload as they may have already benn initialized, every time the network alliance gets loaded
                List<NewInstancePoller> to_add = new ArrayList<>();
                List<NewInstancePoller> to_delete = new ArrayList<>();
                for (NewInstancePoller currInst : pollers.values()) {
                    if (currInst.isCancelled()) {
                        synchronized (pollingTimerMutexes.get(currInst.tenantID + currInst.networkString)) {
                            // One timer for one collaboration connector
                            if (!polling_timers.containsKey(currInst.tenantID + currInst.networkString)) {
                                polling_timers.put(currInst.tenantID + currInst.networkString, new Timer());
                            }
                            NewInstancePoller tempInst = new NewInstancePoller(currInst.getProcessIdToPoll(), currInst.getDelay(), currInst.getPeriod(), currInst.networkString, currInst.tenantID, currInst.allianceName);
                            to_add.add(tempInst);
                            polling_timers.get(currInst.tenantID + currInst.networkString).scheduleAtFixedRate(tempInst, tempInst.getDelay(), tempInst.getPeriod());
                            to_delete.add(currInst);
                        }

                    }
                }

                for (NewInstancePoller del : to_delete) {
                    pollers.remove(del.tenantID + del.allianceName + del.processIdToPoll);
                }
                for (NewInstancePoller add : to_add) {
                    pollers.put(add.tenantID + add.allianceName + add.processIdToPoll, add);
                }
            }
        }


    }


    /**
     * Retrieves all events associated with a given process instance from the BFT-SMaRt network.
     *
     * @param processId The identifier of the process instance to query events for.
     * @return A map of event IDs to PEvent objects representing the events of the queried process instance.
     */
    private Map<Integer, PEvent> getAllEventsFromInstance(String processId) {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);) {
            objOut.writeObject(RequestType.GET_ALL_EVENTS_FROM_INSTANCE);
            objOut.writeObject(processId);
            objOut.flush();
            byteOut.flush();
            byte[] reply = serviceProxies.get(tenantIdForThisNetworkInstance + networkString).invokeUnordered(byteOut.toByteArray());
            if (reply.length == 0)
                throw new RuntimeException("Instance with requested ID could not be found.");
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
                 ObjectInput objIn = new ObjectInputStream(byteIn)) {
                return (Map<Integer, PEvent>) objIn.readObject();
            }
        } catch (IOException | ClassNotFoundException | RuntimeException e) {
            logger.error("Exception getting events: " + e.getMessage());
        }
        return null;
    }


    /**
     * Adds a new event to a specified process instance within the BFT-SMaRt network and returns the event's ID.
     * New events in the network will be noticed in the corresponding TimerTask of the process instance.
     *
     * @param processId The ID of the process instance to add the event to.
     * @param activity  The activity associated with the event.
     * @param stage     The lifecycle stage of the task when the event was emitted.
     * @param variables JSON string representing the process variables at the event time.
     * @param user      The user who initiated the event.
     * @return The ID of the newly added event within the process instance.
     */
    public int put_event(String processId, String activity, String stage, String variables, String user) {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);) {
            objOut.writeObject(RequestType.PUT_EVENT);
            objOut.writeObject(processId);
            objOut.writeObject(new PEvent(activity, stage, variables, user, processId, null));
            objOut.flush();
            byteOut.flush();

            byte[] reply = serviceProxies.get(tenantIdForThisNetworkInstance + networkString).invokeOrdered(byteOut.toByteArray());
            if (reply.length == 0)
                throw new RuntimeException("Instance with requested ID could not be found.");
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
                 ObjectInput objIn = new ObjectInputStream(byteIn)) {
                return (int) objIn.readObject();
            }
        } catch (IOException | ClassNotFoundException | RuntimeException e) {
            logger.error("Exception inserting event: " + e.getMessage());
        }
        return -1;
    }

    /**
     * Sends a process instantiation request to the BFT-SMaRt network. If the process instance is correctly instantiated
     * a new TimerTask will be created to regularly fetch potential updates (new events) of this process instance in the
     * BFT SMaRt network.
     *
     * @param instantiation The instantiation details of the process to be instantiated.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void sendInstantiation(Instantiation instantiation) {
        logger.info("Instantiate new BFT Smart process instance:\t" + instantiation);
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);) {
            objOut.writeObject(RequestType.INSTANTIATE);
            objOut.writeObject(instantiation.getGlobalAllianceReference());
            objOut.writeObject(instantiation.getName());
            objOut.writeObject(instantiation.getJSONVariables());

            objOut.flush();
            byteOut.flush();

            byte[] reply = serviceProxies.get(tenantIdForThisNetworkInstance + networkString).invokeOrdered(byteOut.toByteArray());
            if (reply.length == 0)
                throw new IllegalStateException("An illegal operation occured, an old process instance was overwritten.");
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
                 ObjectInput objIn = new ObjectInputStream(byteIn)) {
                String processId = (String) objIn.readObject();

                alreadyNoticedEvents.get(tenantIdForThisNetworkInstance + allianceName).put(processId, new TreeSet<>());
                logger.info("Putting a new TreeSet at ID: " + processId);
                alreadyNoticedEventsSerializable.put(processId, new TreeSet<>());
                ApplicationContextProvider.bean(DPEXService.class).acceptInstantiationAndSaveToDB(globalAllianceReference, processId);
                NewInstancePoller temp_pol = new NewInstancePoller(processId, 0, 10000, networkString, tenantIdForThisNetworkInstance, allianceName);
                polling_timers.get(tenantIdForThisNetworkInstance + networkString).scheduleAtFixedRate(temp_pol, temp_pol.getDelay(), temp_pol.getPeriod());
                pollers.put(tenantIdForThisNetworkInstance + allianceName + processId, temp_pol);
                pollersSerializable.add(new NewInstancePollerSerializable(processId, temp_pol.getDelay(), temp_pol.getPeriod(), networkString, tenantIdForThisNetworkInstance, allianceName));
                handleInstantiation(objIn.readObject());
            }
        } catch (IOException | ClassNotFoundException | IllegalStateException e) {
            logger.error("Exception instantiating BFT Smart process instance: " + e.getMessage());
        }
    }

    /**
     * Handles the instantiation process after a successful creation of a process instance in the BFT-SMaRt network.
     *
     * @param event The event object containing details of the instantiated process.
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void handleInstantiation(Object event) {
        logger.info("Handle Instantiation:\t" + event);

        ProcessInstancePM pi = (ProcessInstancePM) event;

        String globalAllianceReference = pi.getGlobalAllianceReference();
        String globalInstanceReference = pi.getGlobalInstanceReference();
        String JSONVariables = pi.getProcessVariables();
        String name = pi.getName();

        TenantContext.setTenantId(tenantIdForThisNetworkInstance);
        DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);
        // Since the globalInstanceReference is only unique per alliance name and across one tenant that's why we have to add this here
        dpexService.handleInstantiation(new Instantiation(globalAllianceReference, tenantIdForThisNetworkInstance + allianceName + globalInstanceReference, "", "", JSONVariables, name));
        TenantContext.clear();
    }

    /**
     * Sends a task action to the BFT-SMaRt network, which is then processed and recorded as an event.
     *
     * @param taskAction The task action to be sent to the network.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void sendTaskAction(TaskAction taskAction) {
        logger.info("Send Task Action (with BFT Smart):\t" + taskAction);
        // Remove the tenantid and alliance name again since the BFT SmartNetwork only works with globalInstanceReferences that are unique in the network
        put_event(taskAction.getInstance().getGlobalInstanceReference().substring((tenantIdForThisNetworkInstance + allianceName).length()), taskAction.getTask().getActivity(), taskAction.getTaskLifeCycleStage().toString(), taskAction.getProcessVariables().generateJSONString(), taskAction.getUser().getSciId());

    }

    /**
     * Handles task actions by building and sending acknowledgements for completed tasks to the bpm connector.
     *
     * @param event The event object associated with the task action.
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void handleTaskAction(Object event) {

        logger.info("Handle Task Action:\t" + event);

        PEvent pevent = (PEvent) event;

        TenantContext.setTenantId(tenantIdForThisNetworkInstance);

        DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);
        DPEXUtils dpexUtils = ApplicationContextProvider.bean(DPEXUtils.class);

        // Since the globalInstanceReference is only unique per alliance name and across one tenant that's why we have to add this here
        Acknowledgement acknowledgement = dpexUtils.buildAcknowledgement(
                tenantIdForThisNetworkInstance + allianceName + pevent.getGlobalInstanceReference(),
                Task.builder().activity(pevent.getActivity()).build(),
                TaskLifeCycleStage.valueOf(pevent.getStage()),
                User.builder().sciId(pevent.getUser().toLowerCase()).build(),
                new ProcessVariables(pevent.getVariables())
        );
        dpexService.handleAcknowledgement(acknowledgement);
        TenantContext.clear();
    }

    @Override
    public void sendAcknowledgement(Acknowledgement acknowledgement) {

    }

    @Override
    public void handleAcknowledgement(Object event) {

    }

    @Override
    public void sendDataUpdate(String to) {

    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public void getInstances() {
        logger.info("Get Instances");

    }

    /**
     * Writes public and private keys for the BFT-SMaRt network to the config files. This method is called to set up
     * new key configurations when the network is re-initialized or when new hosts are added.
     *
     * @param publicKeys list of public keys as strings
     * @param clientPrivateKey the private key for the client
     * @param clientPublicKey the public key for the client
     * @param clientId the identifier of the client in the BFT-SMaRt network
     */
    public static void writeKeys(List<String> publicKeys, String clientPrivateKey, String clientPublicKey, int clientId) {
        String viewPath = "./config/currentView";

        // Delete currentView file
        try {
            Files.deleteIfExists(Paths.get(viewPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String keysDirectory = "./config/keysECDSA";
        File directory = new File(keysDirectory);

        // Delete the directory if it exists along with all its content
        if (directory.exists()) {
            deleteDirectory(directory);
        }
        // Create the directory anew
        directory.mkdirs();

        // Write public keys to files
        try {
            for (int i = 0; i < publicKeys.size(); i++) {
                String publicKeyFilename = keysDirectory + "/publickey" + i;
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(publicKeyFilename))) {
                    writer.write(publicKeys.get(i));
                }
            }

            // Write private key to file
            String privateKeyFilename = keysDirectory + "/privatekey" + clientId;
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(privateKeyFilename))) {
                writer.write(clientPrivateKey);
            }

            // Write private key to file
            String publicClientKeyFilename = keysDirectory + "/publickey" + clientId;
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(publicClientKeyFilename))) {
                writer.write(clientPublicKey);
            }

        } catch (IOException e) {
            logger.error("An error occurred while writing keys to files: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Deletes a directory and all of its contents recursively. This is necessary to clean up old keys before adding new
     * ones.
     *
     * @param directory the directory to delete
     */
    private static void deleteDirectory(File directory) {
        File[] allContents = directory.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        directory.delete();
    }

    /**
     * Updates the network configuration based on a new set of IP addresses and port numbers. This method ensures that
     * all necessary configuration files are updated to reflect changes in the network's topology.
     *
     * @param newHosts a list of new host configurations (IP and port numbers)
     * @param useDefaultKeys a boolean flag to determine whether default keys should be used for the network communication
     */
    public static void updateConfiguration(List<String> newHosts, boolean useDefaultKeys) {
        String systemPath = "./config/system.config";
        String configPath = "./config/hosts.config";
        String viewPath = "./config/currentView";

        // Delete currentView file
        try {
            Files.deleteIfExists(Paths.get(viewPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Read existing config file and retain comments and headers
        List<String> linesToRetain = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(configPath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.trim().startsWith("#") || line.trim().isEmpty()) {
                    linesToRetain.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Write new configuration
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(configPath))) {
            for (String line : linesToRetain) {
                bw.write(line);
                bw.newLine();
            }
            int serverId = 0;
            for (String host : newHosts) {
                bw.write(serverId + " " + host);
                bw.newLine();
                serverId++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Calculate faulty replicas and initial view for system config
        int numberOfNodes = newHosts.size();
        int faultyReplicas = (numberOfNodes - 1) / 3;
        String initialView = String.join(",", IntStream.range(0, numberOfNodes).mapToObj(String::valueOf).collect(Collectors.toList()));

        // Read and modify system config file
        try {
            Path sysPath = Path.of(systemPath);
            List<String> systemConfigLines = Files.readAllLines(sysPath);
            for (int i = 0; i < systemConfigLines.size(); i++) {
                if (systemConfigLines.get(i).startsWith("system.servers.num")) {
                    systemConfigLines.set(i, "system.servers.num = " + numberOfNodes);
                } else if (systemConfigLines.get(i).startsWith("system.servers.f")) {
                    systemConfigLines.set(i, "system.servers.f = " + faultyReplicas);
                } else if (systemConfigLines.get(i).startsWith("system.initial.view")) {
                    systemConfigLines.set(i, "system.initial.view = " + initialView);
                } else if (systemConfigLines.get(i).startsWith("system.communication.useSignatures")) {
                    systemConfigLines.set(i, "system.communication.useSignatures = " + (useDefaultKeys ? "0" : "1"));
                } else if (systemConfigLines.get(i).startsWith("system.communication.defaultkeys")) {
                    systemConfigLines.set(i, "system.communication.defaultkeys = " + (useDefaultKeys ? "true" : "false"));
                }
            }
            Files.write(sysPath, systemConfigLines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Serializes the currently noticed events into a byte array for persistence in the database.
     */
    public void loadNoticedEventsIntoSerializedForm() {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(byteOut)) {
            out.writeObject(alreadyNoticedEventsSerializable);
            alreadyNoticedEventsSerialized = byteOut.toByteArray();
        } catch (IOException e) {
            logger.error("Error turning alreadyNoticedEvents in serializable Form: " + e);
        }
    }


    /**
     * Initializes class fields from serialized data post-load from the database.
     * Ensures all non-serializable fields are correctly populated based on the serialized data.
     */
    @PostLoad
    private void init() {
        encryptionUtil = new EncryptionUtil(loadEncryptionKey());
        if (alreadyNoticedEventsSerialized != null) {
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(alreadyNoticedEventsSerialized);
                 ObjectInputStream in = new ObjectInputStream(byteIn)) {
                alreadyNoticedEventsSerializable = (TreeMap<String, TreeSet<Integer>>) in.readObject();
            } catch (ClassNotFoundException | IOException e) {
                logger.error("Error while trying to deserialize noticed events from the database: " + e);
            }
        }

        if (pollersSerializable == null) {
            pollersSerializable = new ArrayList<>();
        }
        if (alreadyNoticedEventsSerializable == null) {
            alreadyNoticedEventsSerializable = new TreeMap<>();
        }

        logger.info("Instantiate new BFTSmart Network from a JPA Repository.");
        reinitialize_after_deserialization();
    }

}
