package dpex.impl.collaboration.hyperledger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.network.BaseNetwork;
import dpex.core.ApplicationContextProvider;
import dpex.core.DPEXService;
import dpex.core.DPEXUtils;
import dpex.core.User;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hyperledger.fabric.client.*;
import org.hyperledger.fabric.client.identity.*;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class HyperledgerNetwork extends BaseNetwork {

    private static final Logger logger = LogManager.getLogger(HyperledgerNetwork.class);

    private static HyperledgerConfig config;

    private static String connectionAddress;

    private static ManagedChannel channel;

    public HyperledgerNetwork(String connection, HyperledgerConfig hyperledgerConfig) throws CertificateException, IOException, InvalidKeyException {

        config = hyperledgerConfig;
        connectionAddress = connection.substring(7);

        logger.info("Instantiate new HyperledgerNetwork.");


//        HYPERLEDGERPATH = hyperledgerPath;

        super.setSciAddress(config.getHyperledgerChaincode());

        Gateway.Builder builder = connect();

        logger.info("Subscribe for SCI events on: " + config.getHyperledgerChaincode());

        try (Gateway gateway = builder.connect()) {
            Network network = gateway.getNetwork(config.getHyperledgerChannel());

            logger.info("start Listening on Event of " + config.getHyperledgerChaincode());

            CloseableIterator<ChaincodeEvent> eventIterator = network.getChaincodeEvents(config.getHyperledgerChaincode());

            CompletableFuture.runAsync(() -> eventIterator.forEachRemaining(contractEvent -> {
                logger.info("new Hyperledger Event received: " + contractEvent.getEventName());
                ObjectMapper mapper = new ObjectMapper();
                try {
                    if (contractEvent.getEventName().equals("instantiate")) {
                        byte[] payload = contractEvent.getPayload();
                        TypeReference<Map<String, String>> typeReference = new TypeReference<>() {};
                        Map<String, String> map = mapper.readValue(payload, typeReference);
                        this.handleInstantiation(map);
                    } else if (contractEvent.getEventName().equals("taskAction")) {
                        byte[] payload = contractEvent.getPayload();
                        TypeReference<Map<String, Object>> typeReference = new TypeReference<>() {};
                        Map<String, Object> map = mapper.readValue(payload, typeReference);
                        this.handleTaskAction(map);
                    } else {
                        logger.error("unknown Blockchain Event occurred on Hyperledger:" + contractEvent.getEventName());
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

            }));
        }

    }

    @Override
    public void sendInstantiation(Instantiation instantiation) {

        logger.info("Send Instantiation with DpexSci: \t" + instantiation);

        Gateway.Builder builder = connect();

        try (Gateway gateway = builder.connect()) {
            Network network = gateway.getNetwork(config.getHyperledgerChannel());
            Contract contract = network.getContract(super.getSciAddress());

            contract.submitTransaction("instantiate", instantiation.getJSONVariables());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private Gateway.Builder connect() {
        try {
            if (channel == null || channel.isShutdown()) {
                channel = newGrpcConnection();
            }

            Gateway.Builder builder = Gateway.newInstance()
                    .identity(newIdentity())
                    .signer(newSigner())
                    .connection(channel)
                    .evaluateOptions(options -> options.withDeadlineAfter(5, TimeUnit.SECONDS))
                    .endorseOptions(options -> options.withDeadlineAfter(15, TimeUnit.SECONDS))
                    .submitOptions(options -> options.withDeadlineAfter(5, TimeUnit.SECONDS))
                    .commitStatusOptions(options -> options.withDeadlineAfter(1, TimeUnit.MINUTES));

            return builder;

        } catch (IOException | InvalidKeyException | CertificateException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void handleInstantiation(Object event) {
        logger.info("handle instantiation (Hyperledger):\t" + event);

        HashMap<String, String> hyperledgerInstantiateEvent = (HashMap) event;

        //TODO get from event
        String globalAllianceReference = config.getGAR();

        Instantiation instantiation = new Instantiation(globalAllianceReference, hyperledgerInstantiateEvent.get("globalInstanceReference"), "", "", hyperledgerInstantiateEvent.get("variables"), "TODO:name");

        logger.debug("New event from Hyperledger for instance " + globalAllianceReference);
        logger.debug("Event payload: " + hyperledgerInstantiateEvent);

        //Due to the Hyperledger design, no new Contract Listener is added on this point.

        logger.info("Global instance reference:" + hyperledgerInstantiateEvent.get("globalInstanceReference"));

        DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);
        dpexService.handleInstantiation(instantiation);

    }

    @Override
    public void sendTaskAction(TaskAction taskAction) {

        logger.info("Send Task Action (with Hyperledger):\t" + taskAction);

        Gateway.Builder builder = connect();

        try (Gateway gateway = builder.connect()) {
            Network network = gateway.getNetwork(config.getHyperledgerChannel());
            Contract contract = network.getContract(config.getHyperledgerChaincode());

            contract.submitTransaction("taskAction",
                    taskAction.getInstance().getGlobalInstanceReference(),
                    taskAction.getTask().getActivity(),
                    taskAction.getTaskLifeCycleStage().toString(),
                    taskAction.getUser().getSciId(),
                    taskAction.getProcessVariables().generateJSONString());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public void handleTaskAction(Object event) {
        logger.info("Handle Task Action (Hyperledger): \t" + event);

        HashMap<String, String> hyperledgerTaskEvent = (HashMap) event;

        logger.info("New event from Hyperledger for instance " + hyperledgerTaskEvent.get("resource") + ": (" + hyperledgerTaskEvent.get("globalInstanceReference") + ")");
        logger.info("Event payload: " + hyperledgerTaskEvent);

        DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);
        DPEXUtils dpexUtils = ApplicationContextProvider.bean(DPEXUtils.class);


        Acknowledgement acknowledgement = dpexUtils.buildAcknowledgement(
                hyperledgerTaskEvent.get("globalInstanceReference"),
                Task.builder().activity(hyperledgerTaskEvent.get("activity")).build(),
                TaskLifeCycleStage.valueOf(hyperledgerTaskEvent.get("stage")),
                User.builder().sciId(hyperledgerTaskEvent.get("resource")).build(),
                new ProcessVariables(hyperledgerTaskEvent.get("variables"))
        );
        dpexService.handleAcknowledgement(acknowledgement);
    }

    @Override
    public void sendAcknowledgement(Acknowledgement acknowledgement) {

    }

    @Override
    public void handleAcknowledgement(Object event) {

    }

    @Override
    public void sendDataUpdate(String recipient) {

    }

    private static ManagedChannel newGrpcConnection() throws IOException, CertificateException {
        var tlsCertReader = Files.newBufferedReader(config.getTLSCertificatePath());
        var tlsCert = Identities.readX509Certificate(tlsCertReader);

        return NettyChannelBuilder.forTarget(connectionAddress)
                .sslContext(GrpcSslContexts.forClient().trustManager(tlsCert).build()).overrideAuthority(config.getOverrideAuth())
                .build();
    }

    private static Identity newIdentity() throws IOException, CertificateException {
        var certReader = Files.newBufferedReader(config.getCertificatePath());
        var certificate = Identities.readX509Certificate(certReader);

        return new X509Identity(config.getMSP_ID(), certificate);
    }

    private static Signer newSigner() throws IOException, InvalidKeyException {
        var keyReader = Files.newBufferedReader(getPrivateKeyPath());
        var privateKey = Identities.readPrivateKey(keyReader);

        return Signers.newPrivateKeySigner(privateKey);
    }

    private static Path getPrivateKeyPath() throws IOException {
        try (var keyFiles = Files.list(config.getKeyDirectoryPath())) {
            return keyFiles.findFirst().orElseThrow();
        }
    }

}