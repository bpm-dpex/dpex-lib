package dpex.impl.collaboration.hyperledger;

import dpex.collaboration.SCIConfig;
import lombok.Builder;
import lombok.Data;

import java.nio.file.Path;

@Data
@Builder
public class HyperledgerConfig extends SCIConfig {

    private String contractName;
    private String GAR;

    private String hyperledgerPath;
    private String hyperledgerChannel;
    private String hyperledgerChaincode;
    private String MSP_ID;
    private String overrideAuth;

    private Path cryptoPath;
    private Path certificatePath;
    private Path keyDirectoryPath;
    private Path TLSCertificatePath;

}
