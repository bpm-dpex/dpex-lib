package dpex.impl.collaboration.hyperledger;

import dpex.collaboration.Collaboration;
import lombok.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Entity;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Hyperledger extends Collaboration {

    private static final Logger logger = LogManager.getLogger(Hyperledger.class);

    //7054
    private int port = 7054;

    private String HYPERLEGER_PATH = "\\\\wsl.localhost\\Ubuntu-22.04\\home\\tobias\\hyperledger\\fabric-samples\\";

    private String contract_name;

}
