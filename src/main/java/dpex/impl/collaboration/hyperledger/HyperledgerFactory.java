package dpex.impl.collaboration.hyperledger;

import dpex.collaboration.CollaborationFactory;
import dpex.collaboration.SCIConfig;
import dpex.impl.collaboration.consensus.TrueConsensus;
import dpex.impl.collaboration.security.NoSecurity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Entity;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.cert.CertificateException;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HyperledgerFactory extends CollaborationFactory {

    private static final Logger logger = LogManager.getLogger(HyperledgerFactory.class);
    private String connection;

    @Override
    public Hyperledger create(SCIConfig sciConfig) {

        HyperledgerConfig hyperledgerConfig = (HyperledgerConfig) sciConfig;

        logger.info("Create new Hyperledger Collaboration with Contract:\t" + hyperledgerConfig.getContractName());

        HyperledgerNetwork hyperledgerNetwork;
        try {
            hyperledgerNetwork = new HyperledgerNetwork(connection, hyperledgerConfig);
        } catch (CertificateException | IOException | InvalidKeyException e) {
            throw new RuntimeException("Hyperledger Network can not be instantiated by the HyperledgerFactory" + e);
        }

        Hyperledger hyperledger = new Hyperledger().builder().build();

        hyperledger.setNetwork(hyperledgerNetwork);
        hyperledger.setSecurity(NoSecurity.class);
        hyperledger.setConsensus(TrueConsensus.class);

        hyperledger.setName(super.getName());

        try {
            hyperledger.setConnection(new URL(connection));
            return hyperledger;
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error creating SimpleCollaboration: " + e.getMessage());
        }
    }
}