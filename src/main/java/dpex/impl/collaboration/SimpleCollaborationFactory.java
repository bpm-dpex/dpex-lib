package dpex.impl.collaboration;

import dpex.collaboration.CollaborationFactory;
import dpex.collaboration.SCIConfig;
import dpex.collaboration.network.BaseNetwork;
import dpex.impl.collaboration.consensus.ThresholdConsensus;
import dpex.impl.collaboration.network.HttpNetwork;
import dpex.impl.collaboration.security.NoSecurity;
import lombok.*;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SimpleCollaborationFactory extends CollaborationFactory {


    int network_port;
    int consensus_numberOfAgreementsRequired;

    public SimpleCollaboration create(SCIConfig unused) {
        //TODO: Use SuperBuilder
        SimpleCollaboration sc = SimpleCollaboration.builder()
                .network_port(this.network_port)
                .consensus_numberOfAgreementsRequired(this.consensus_numberOfAgreementsRequired)
                .build();
        sc.setConsensus(ThresholdConsensus.class);
        sc.setSecurity(NoSecurity.class);
        sc.setNetwork(new HttpNetwork());
        sc.setName(super.getName());
        return sc;

    }


}
