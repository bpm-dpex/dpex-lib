package dpex.impl.collaboration;

import com.fasterxml.jackson.databind.ObjectMapper;
import dpex.action.Instantiation;
import dpex.collaboration.Collaboration;
import lombok.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Entity;
import java.net.URL;

/**
 * A SimpleCollaboration uses a HttpNetwork, a ThresholdConsensus and RSASecurity.
 */
@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SimpleCollaboration extends Collaboration {

    private static final Logger logger = LogManager.getLogger(SimpleCollaboration.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    int network_port;
    int consensus_numberOfAgreementsRequired;

    public SimpleCollaboration(URL connection, int port, int numberOfAgreementsRequired) {
        this.network_port = port;
        this.consensus_numberOfAgreementsRequired = numberOfAgreementsRequired;
        super.setConnection(connection);
    }

}
