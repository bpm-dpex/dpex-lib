package dpex.impl.collaboration.consensus;

import dpex.action.TaskAction;
import dpex.collaboration.CandidateLog;
import dpex.collaboration.consensus.ConsensusEngine;
import dpex.collaboration.consensus.Acknowledgement;
import lombok.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The ThresholdConsensus is parametrized with the number of agreements that are required
 * to assume that agreement is reached on a certain event.
 *
 * The number of agreements may be equal to the number of participants.
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ThresholdConsensus extends ConsensusEngine {

    private static final Logger logger = LogManager.getLogger(ThresholdConsensus.class);

    private int numberOfAgreementsRequired;

    @Override
    public boolean isAgreementReached(Acknowledgement acknowledgement, CandidateLog clog, String... values) {
        int numberOfAgreementsRequired = Integer.parseInt(values[0]);
        TaskAction taskAction = acknowledgement.getAction();
        if(clog.getNumberOfAcknowledgements().get(taskAction.getTask()) >= numberOfAgreementsRequired) {
            return true;
        }
        return false;
    }
}
