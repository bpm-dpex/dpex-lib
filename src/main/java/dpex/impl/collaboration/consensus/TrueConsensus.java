package dpex.impl.collaboration.consensus;

import dpex.collaboration.CandidateLog;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.consensus.ConsensusEngine;

public class TrueConsensus  extends ConsensusEngine {
    @Override
    public boolean isAgreementReached(Acknowledgement a, CandidateLog clog, String... values) {
        return true;
    }
}
