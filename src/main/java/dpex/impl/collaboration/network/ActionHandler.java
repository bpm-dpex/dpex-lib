package dpex.impl.collaboration.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dpex.action.TaskAction;
import dpex.collaboration.Collaboration;
import dpex.core.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ActionHandler implements HttpHandler {

    private static final Logger logger = LogManager.getLogger(ActionHandler.class);

    ObjectMapper objectMapper = new ObjectMapper();
    Collaboration collaboration;

    public ActionHandler(Collaboration collaboration) {
        this.collaboration = collaboration;
    }


    /**
     * Provided by HttpHandler interface from com.sun.net.httpserver package.
     * This method is called, when the HttpServer in this class receives a message.
     * Messages will come from other participants and include messages and acknowledgements.
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        logger.info("Message received in ActionHandler...");

        // Create Message-Object from Request
        String requestBodyText = Util.getTextFromInputStream(exchange.getRequestBody());
        TaskAction taskAction = objectMapper.readValue(requestBodyText, TaskAction.class);

        logger.info(taskAction.getUser().getBpmId() + " wants to execute " + taskAction.getTask());

//        collaboration.taskExecutionReceived(taskCompletion);
    }
}
