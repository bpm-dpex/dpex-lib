package dpex.impl.collaboration.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dpex.action.Instantiation;
import dpex.collaboration.Collaboration;
import dpex.core.DPEXService;
import dpex.core.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class DeploymentHandler implements HttpHandler {

    DPEXService dpexService;

    private static final Logger logger = LogManager.getLogger(DeploymentHandler.class);

    ObjectMapper objectMapper = new ObjectMapper();
    Collaboration simpleCollaboration;

    public DeploymentHandler(Collaboration collaboration) {
        this.simpleCollaboration = collaboration;
    }

    /**
     * Provided by HttpHandler interface from com.sun.net.httpserver package.
     * This method is called, when the HttpServer in this class receives a message.
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        logger.info("Message received in DeploymentHandler...");

        System.out.println("GOING TO RECEIVE THAT!!");


        // Create Message-Object from Request
        String requestBodyText = Util.getTextFromInputStream(exchange.getRequestBody());
        Instantiation instantiation = objectMapper.readValue(requestBodyText, Instantiation.class);

        //simpleCollaboration.instantiationReceived(instantiation);

    }
}
