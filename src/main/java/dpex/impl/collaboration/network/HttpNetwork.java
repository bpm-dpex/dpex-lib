package dpex.impl.collaboration.network;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpServer;
import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.network.BaseNetwork;
import dpex.core.DPEXService;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)

public class HttpNetwork extends BaseNetwork {

    //TODO: Usage introduces error, when application is shut down: "Build cancelled while executing task ':app:App.main()'"

    @Transient
    private HttpClient httpClient = HttpClient.newHttpClient();

    @Override
    public void sendTaskAction(TaskAction taskCompletion) {
        ObjectMapper objectMapper = new ObjectMapper();

        //Build message object
        String message = "";

        try {
            message = objectMapper.writeValueAsString(taskCompletion);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        for(String recipient : List.of(super.getSciAddress())) {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(recipient))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(message))
                    .build();
            this.httpClient.sendAsync(request, HttpResponse.BodyHandlers.discarding());
        }
    }

    @Override
    public void handleTaskAction(Object event) {

    }

    @Override
    public void sendDataUpdate(String to) {

    }

    @Override
    public void sendAcknowledgement(Acknowledgement acknowledgement) {

        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient httpClient = HttpClient.newHttpClient();

        //Build message object
        String message = "";

        try {
            message = objectMapper.writeValueAsString(acknowledgement);
        } catch (JsonProcessingException e) {
            //logger.error("Jackson Error while parsing Acknowledgement object: " + e.getMessage());
            e.printStackTrace();
        }

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(super.getSciAddress() + "/ack"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(message))
                    .build();

            httpClient.sendAsync(request, HttpResponse.BodyHandlers.discarding());
        }

    @Override
    public void handleAcknowledgement(Object event) {

    }

    @Override
    public void sendInstantiation(Instantiation instantiation) {

        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient httpClient = HttpClient.newHttpClient();

        //Build message object
        String message = "";
        try {
            message = objectMapper.writeValueAsString(instantiation);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(super.getSciAddress()))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(message))
                .build();

        this.httpClient.sendAsync(request, HttpResponse.BodyHandlers.discarding());

    }

    @Override
    public void handleInstantiation(Object event) {
    }

    @Transient
    private HttpServer httpServer;

    /**
     * TODO: Is this autowired? Maybe make this class a component
     */
    public void initialize(DPEXService service, String arg) {
        /*this.httpServer = HttpServer.create(new InetSocketAddress(8444), 0);

        httpServer.createContext("/ack", new AcknowledgementHandler(collaboration));
        httpServer.createContext("/action", new ActionHandler(collaboration));
        httpServer.createContext("/dply", new DeploymentHandler(collaboration));
        httpServer.setExecutor(null); // creates a default executor
        httpServer.start();
        */
    }

}
