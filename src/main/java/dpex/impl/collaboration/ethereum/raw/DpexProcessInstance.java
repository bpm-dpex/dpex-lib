package dpex.impl.collaboration.ethereum.raw;

import io.reactivex.Flowable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.DynamicStruct;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.BaseEventResponse;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 1.5.0.
 */
@SuppressWarnings("rawtypes")
public class DpexProcessInstance extends Contract {
    public static final String BINARY = "0x608060405234801561001057600080fd5b50610cfd806100206000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c80634ae6971314610046578063909e4ab614610062578063941e0f5514610080575b600080fd5b610060600480360381019061005b9190610624565b61009e565b005b61006a6101cd565b60405161007791906108be565b60405180910390f35b610088610438565b604051610095919061092a565b60405180910390f35b600060405180608001604052808581526020018481526020013373ffffffffffffffffffffffffffffffffffffffff1681526020018381525090506002819080600181540180825580915050600190039060005260206000209060040201600090919091909150600082015181600001908161011a9190610b62565b5060208201518160010190816101309190610b62565b5060408201518160020160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550606082015181600301908161018d9190610b62565b5050507fc2d459d79fb8e2fb46827a680c4a2dc16377b8aaba5c4d5a755c2f55bc2d019e816040516101bf9190610ca5565b60405180910390a150505050565b60606002805480602002602001604051908101604052809291908181526020016000905b8282101561042f57838290600052602060002090600402016040518060800160405290816000820180546102249061097b565b80601f01602080910402602001604051908101604052809291908181526020018280546102509061097b565b801561029d5780601f106102725761010080835404028352916020019161029d565b820191906000526020600020905b81548152906001019060200180831161028057829003601f168201915b505050505081526020016001820180546102b69061097b565b80601f01602080910402602001604051908101604052809291908181526020018280546102e29061097b565b801561032f5780601f106103045761010080835404028352916020019161032f565b820191906000526020600020905b81548152906001019060200180831161031257829003601f168201915b505050505081526020016002820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200160038201805461039e9061097b565b80601f01602080910402602001604051908101604052809291908181526020018280546103ca9061097b565b80156104175780601f106103ec57610100808354040283529160200191610417565b820191906000526020600020905b8154815290600101906020018083116103fa57829003601f168201915b505050505081525050815260200190600101906101f1565b50505050905090565b6060600080546104479061097b565b80601f01602080910402602001604051908101604052809291908181526020018280546104739061097b565b80156104c05780601f10610495576101008083540402835291602001916104c0565b820191906000526020600020905b8154815290600101906020018083116104a357829003601f168201915b5050505050905090565b6000604051905090565b600080fd5b600080fd5b600080fd5b600080fd5b6000601f19601f8301169050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fd5b610531826104e8565b810181811067ffffffffffffffff821117156105505761054f6104f9565b5b80604052505050565b60006105636104ca565b905061056f8282610528565b919050565b600067ffffffffffffffff82111561058f5761058e6104f9565b5b610598826104e8565b9050602081019050919050565b82818337600083830152505050565b60006105c76105c284610574565b610559565b9050828152602081018484840111156105e3576105e26104e3565b5b6105ee8482856105a5565b509392505050565b600082601f83011261060b5761060a6104de565b5b813561061b8482602086016105b4565b91505092915050565b60008060006060848603121561063d5761063c6104d4565b5b600084013567ffffffffffffffff81111561065b5761065a6104d9565b5b610667868287016105f6565b935050602084013567ffffffffffffffff811115610688576106876104d9565b5b610694868287016105f6565b925050604084013567ffffffffffffffff8111156106b5576106b46104d9565b5b6106c1868287016105f6565b9150509250925092565b600081519050919050565b600082825260208201905092915050565b6000819050602082019050919050565b600081519050919050565b600082825260208201905092915050565b60005b83811015610731578082015181840152602081019050610716565b60008484015250505050565b6000610748826106f7565b6107528185610702565b9350610762818560208601610713565b61076b816104e8565b840191505092915050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b60006107a182610776565b9050919050565b6107b181610796565b82525050565b600060808301600083015184820360008601526107d4828261073d565b915050602083015184820360208601526107ee828261073d565b915050604083015161080360408601826107a8565b506060830151848203606086015261081b828261073d565b9150508091505092915050565b600061083483836107b7565b905092915050565b6000602082019050919050565b6000610854826106cb565b61085e81856106d6565b935083602082028501610870856106e7565b8060005b858110156108ac578484038952815161088d8582610828565b94506108988361083c565b925060208a01995050600181019050610874565b50829750879550505050505092915050565b600060208201905081810360008301526108d88184610849565b905092915050565b600082825260208201905092915050565b60006108fc826106f7565b61090681856108e0565b9350610916818560208601610713565b61091f816104e8565b840191505092915050565b6000602082019050818103600083015261094481846108f1565b905092915050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b6000600282049050600182168061099357607f821691505b6020821081036109a6576109a561094c565b5b50919050565b60008190508160005260206000209050919050565b60006020601f8301049050919050565b600082821b905092915050565b600060088302610a0e7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff826109d1565b610a1886836109d1565b95508019841693508086168417925050509392505050565b6000819050919050565b6000819050919050565b6000610a5f610a5a610a5584610a30565b610a3a565b610a30565b9050919050565b6000819050919050565b610a7983610a44565b610a8d610a8582610a66565b8484546109de565b825550505050565b600090565b610aa2610a95565b610aad818484610a70565b505050565b5b81811015610ad157610ac6600082610a9a565b600181019050610ab3565b5050565b601f821115610b1657610ae7816109ac565b610af0846109c1565b81016020851015610aff578190505b610b13610b0b856109c1565b830182610ab2565b50505b505050565b600082821c905092915050565b6000610b3960001984600802610b1b565b1980831691505092915050565b6000610b528383610b28565b9150826002028217905092915050565b610b6b826106f7565b67ffffffffffffffff811115610b8457610b836104f9565b5b610b8e825461097b565b610b99828285610ad5565b600060209050601f831160018114610bcc5760008415610bba578287015190505b610bc48582610b46565b865550610c2c565b601f198416610bda866109ac565b60005b82811015610c0257848901518255600182019150602085019450602081019050610bdd565b86831015610c1f5784890151610c1b601f891682610b28565b8355505b6001600288020188555050505b505050505050565b60006080830160008301518482036000860152610c51828261073d565b91505060208301518482036020860152610c6b828261073d565b9150506040830151610c8060408601826107a8565b5060608301518482036060860152610c98828261073d565b9150508091505092915050565b60006020820190508181036000830152610cbf8184610c34565b90509291505056fea264697066735822122014f56d8c7fb064aa42049f0dd3db9b4a14a01faf43231d4ed895c0e81246ad5064736f6c63430008130033";

    public static final String FUNC_STORE = "store";

    public static final String FUNC_GETGLOBALINSTANCEREF = "getGlobalInstanceRef";

    public static final String FUNC_GETLOG = "getLog";

    public static final org.web3j.abi.datatypes.Event EVENTEMITTER_EVENT = new org.web3j.abi.datatypes.Event("EventEmitter", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Event>() {}));
    ;

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
    }

    @Deprecated
    protected DpexProcessInstance(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected DpexProcessInstance(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected DpexProcessInstance(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected DpexProcessInstance(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static List<EventEmitterEventResponse> getEventEmitterEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = staticExtractEventParametersWithLog(EVENTEMITTER_EVENT, transactionReceipt);
        ArrayList<EventEmitterEventResponse> responses = new ArrayList<EventEmitterEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            EventEmitterEventResponse typedResponse = new EventEmitterEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.logEntry = (Event) eventValues.getNonIndexedValues().get(0);
            responses.add(typedResponse);
        }
        return responses;
    }

    public static EventEmitterEventResponse getEventEmitterEventFromLog(Log log) {
        Contract.EventValuesWithLog eventValues = staticExtractEventParametersWithLog(EVENTEMITTER_EVENT, log);
        EventEmitterEventResponse typedResponse = new EventEmitterEventResponse();
        typedResponse.log = log;
        typedResponse.logEntry = (Event) eventValues.getNonIndexedValues().get(0);
        return typedResponse;
    }

    public Flowable<EventEmitterEventResponse> eventEmitterEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(log -> getEventEmitterEventFromLog(log));
    }

    public Flowable<EventEmitterEventResponse> eventEmitterEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(EVENTEMITTER_EVENT));
        return eventEmitterEventFlowable(filter);
    }

    public RemoteFunctionCall<TransactionReceipt> store(String activity, String stage, String variables) {
        final Function function = new Function(
                FUNC_STORE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(activity), 
                new org.web3j.abi.datatypes.Utf8String(stage), 
                new org.web3j.abi.datatypes.Utf8String(variables)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<String> getGlobalInstanceRef() {
        final Function function = new Function(FUNC_GETGLOBALINSTANCEREF, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<List> getLog() {
        final Function function = new Function(FUNC_GETLOG, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Event>>() {}));
        return new RemoteFunctionCall<List>(function,
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    @Deprecated
    public static DpexProcessInstance load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new DpexProcessInstance(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static DpexProcessInstance load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new DpexProcessInstance(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static DpexProcessInstance load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new DpexProcessInstance(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static DpexProcessInstance load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new DpexProcessInstance(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<DpexProcessInstance> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(DpexProcessInstance.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<DpexProcessInstance> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(DpexProcessInstance.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<DpexProcessInstance> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(DpexProcessInstance.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<DpexProcessInstance> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(DpexProcessInstance.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class Event extends DynamicStruct {
        public String activity;

        public String stage;

        public String resource;

        public String variables;

        public Event(String activity, String stage, String resource, String variables) {
            super(new org.web3j.abi.datatypes.Utf8String(activity), 
                    new org.web3j.abi.datatypes.Utf8String(stage), 
                    new org.web3j.abi.datatypes.Address(resource), 
                    new org.web3j.abi.datatypes.Utf8String(variables));
            this.activity = activity;
            this.stage = stage;
            this.resource = resource;
            this.variables = variables;
        }

        public Event(Utf8String activity, Utf8String stage, Address resource, Utf8String variables) {
            super(activity, stage, resource, variables);
            this.activity = activity.getValue();
            this.stage = stage.getValue();
            this.resource = resource.getValue();
            this.variables = variables.getValue();
        }
    }

    public static class EventEmitterEventResponse extends BaseEventResponse {
        public Event logEntry;
    }
}
