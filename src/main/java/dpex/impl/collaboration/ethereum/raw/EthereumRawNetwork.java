package dpex.impl.collaboration.ethereum.raw;

import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.network.BaseNetwork;
import dpex.core.ApplicationContextProvider;
import dpex.core.DPEXService;
import dpex.core.DPEXUtils;
import dpex.core.User;
import dpex.db.TenantContext;
import dpex.error.ReflectionException;
import dpex.events.SendInstantiationToFrontendEvent;
import dpex.events.SendTaskActionToFrontendEvent;
import io.reactivex.disposables.Disposable;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.datatypes.Type;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.ClientTransactionManager;
import org.web3j.tx.gas.DefaultGasProvider;

import javax.persistence.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;

@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class EthereumRawNetwork extends BaseNetwork {

    private static final Logger logger = LogManager.getLogger(EthereumRawNetwork.class);

    // Version of latestProcessedBlocks that can be saved to the database
    @Lob
    private byte[] latestProcessedBlocksPersistable;
    @Transient
    Disposable bpmHandler, sciHandler;

    /**
     * Disposes of ethereum event listeners to prevent memory leaks or unnecessary resource consumption.
     * This method should be called when the network is being shutdown, like when a tenant gets deleted.
     */
    public void disposeListerners() {
        if (bpmHandler != null) {
            if (!bpmHandler.isDisposed()) {
                bpmHandler.dispose();
            }
        }
        if (sciHandler != null) {
            if (!sciHandler.isDisposed()) {
                sciHandler.dispose();
            }
        }

    }

    @Transient
    private static final Object latestProcessedBlocks_mutex = new Object();
    // Tracks events already processed for alliances globally
    // Key: tenantID + allianceName + GIR -> Event Variables
    @Transient
    private static TreeMap<String, TreeSet<String>> latestProcessedBlocks;
    @Transient
    private static final Object has_setup_event_listeners_mutex = new Object();
    // Tracks the event listeners that have been set up for all alliances globally
    // Listener for an alliance represented by: tenantId + allianceName
    @Transient
    private static Set<String> has_setup_event_listeners = new HashSet<>();

    private String sciContractAddressP;
    private String allianceName;
    private int blocksToWaitForFinalityP;


    /**
     * @author Moritz Johlige
     * @version 0.2
     */
    @PostLoad
    private void init() {

        synchronized (latestProcessedBlocks_mutex) {
            if (latestProcessedBlocksPersistable != null) {
                try (ByteArrayInputStream byteIn = new ByteArrayInputStream(latestProcessedBlocksPersistable);
                     ObjectInputStream in = new ObjectInputStream(byteIn)) {
                    TreeMap<String, TreeSet<String>> deserializedMap = (TreeMap<String, TreeSet<String>>) in.readObject();
                    if (latestProcessedBlocks == null) {
                        latestProcessedBlocks = new TreeMap<>();
                    }
                    for (Map.Entry<String, TreeSet<String>> entry : deserializedMap.entrySet()) {
                        if (!latestProcessedBlocks.containsKey(entry.getKey())) {
                            latestProcessedBlocks.put(entry.getKey(), entry.getValue());
                        } else {
                            TreeSet<String> existingSet = latestProcessedBlocks.get(entry.getKey());
                            for (String value : entry.getValue()) {
                                existingSet.add(value);
                            }
                        }
                    }
                } catch (ClassNotFoundException | IOException e) {
                    logger.error("Error while trying to deserialize latestProcessedBlockNumbers from database: ", e);
                }
            }

            if (latestProcessedBlocks == null) {
                latestProcessedBlocks = new TreeMap<>();
            }
        }


        synchronized (has_setup_event_listeners_mutex) {
            if (!has_setup_event_listeners.contains(tenantIdForThisNetworkInstance + allianceName)) {
                has_setup_event_listeners.add(tenantIdForThisNetworkInstance + allianceName);
                if (latestProcessedBlocks != null) {
                    HttpService httpService = new HttpService(super.getSciAddress());
                    httpService.addHeader("Authorization", super.getSciAuthHeader());
                    Web3j web3 = Web3j.build(httpService);

                    // Handle instantiations already seen once will be skipped
                    EthFilter filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, sciContractAddressP);
                    sciHandler = web3.ethLogFlowable(filter).subscribe(this::handleInstantiation);

                    // Re-setup event listeners for instantiations already seen
                    for (String inst : latestProcessedBlocks.keySet()) {
                        String prefix = tenantIdForThisNetworkInstance + allianceName;
                        if (inst.startsWith(prefix)) {
                            int startIndex = prefix.length();
                            String globalinstref = inst.substring(startIndex);
                            EthFilter filter2 = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, globalinstref);
                            bpmHandler = web3.ethLogFlowable(filter2).subscribe(this::handleTaskAction);
                        }
                    }
                }
            }
        }
    }

    /**
     * Serializes the `latestProcessedBlocks` object into a byte array for persistence. Is called whenever
     * latestProcessedBlocks gets updated to also update the persistable form.
     * This method attempts to convert the current state of `latestProcessedBlocks`
     * into a serialized form stored in `latestProcessedBlocksPersistable`.
     * If successful, this byte array can be persisted to storage by JPA (the original data structure is not directly supported by JPA).
     * Any IOException during serialization is caught and logged as an error, indicating
     * a failure to prepare the data for persistence.
     *
     * @author Moritz Johlige
     * @version 0.2
     */
    private void prepareLatestProcessedBlocksForPersistence() {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(byteOut)) {
            out.writeObject(latestProcessedBlocks);
            latestProcessedBlocksPersistable = byteOut.toByteArray();
        } catch (IOException e) {
            logger.error("Error turning latestProcessedBlockNumbers in persistable Form: ", e);
        }
    }

    // Used to create a new EthereumRawNetwork in the corresponding factory
    public EthereumRawNetwork(String connection, String sciContractAddress, String authHeader, int blocksToWaitForFinality, String tenantIdForThisNetworkInstance, String allianceName) {
        logger.info("Instantiate new EthereumRawNetwork.");

        super.setSciAddress(connection);
        super.setSciAuthHeader(authHeader);
        sciContractAddressP = sciContractAddress;


        this.allianceName = allianceName;
        super.tenantIdForThisNetworkInstance = tenantIdForThisNetworkInstance;
        synchronized (has_setup_event_listeners_mutex) {
            has_setup_event_listeners.add(tenantIdForThisNetworkInstance + allianceName);
        }
        synchronized (latestProcessedBlocks_mutex) {
            if (latestProcessedBlocks == null) {
                latestProcessedBlocks = new TreeMap<>();
            }
        }
        blocksToWaitForFinalityP = blocksToWaitForFinality;

        logger.info("Subscribe for SCI events on: " + sciContractAddress + ", in this ERN::eventReceived.");

        HttpService httpService = new HttpService(connection);
        httpService.addHeader("Authorization", authHeader);
        Web3j web3 = Web3j.build(httpService);
        EthFilter filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, sciContractAddress);
        sciHandler = web3.ethLogFlowable(filter).subscribe(this::handleInstantiation);

    }

    /**
     * Sends an instantiation event to the frontend for signing.
     *
     * @param instantiation the instantiation
     */
    @Override
    @SuppressWarnings("unchecked")
    public void sendInstantiation(Instantiation instantiation) {
        logger.info("Send instantiation with DpexSci:\t" + instantiation);
        DPEXService.eventBus.post(new SendInstantiationToFrontendEvent(super.getSciAddress(), super.getSciAuthHeader(), instantiation.getGlobalAllianceReference(), instantiation.getJSONVariables(), instantiation.getName(), instantiation.getSender().getUser().getSciId(), tenantIdForThisNetworkInstance));

    }

    /**
     * Handles the task action from the blockchain. This method processes the event logs from Ethereum transactions
     * that represent task actions. It decodes the event data, constructs a TaskAction object,
     * and forwards it to the DPEXService for further processing.
     *
     * @param event the event log from Ethereum that needs to be processed
     */
    @Override
    @SuppressWarnings("rawtypes")
    public void handleInstantiation(Object event) {


        logger.info("Handle Instantiation:\t" + event);

        Log log = (Log) event;
        List<Type> args = FunctionReturnDecoder.decode(log.getData(), DpexSci.EVENTEMITTER_EVENT.getParameters());

        String globalAllianceReference = log.getAddress();
        String globalInstanceReference = args.get(0).toString();
        String JSONVariables = args.get(1).toString();
        String name = args.get(2).toString();

        // This conditional block ensures that only instances not yet processed by a BPM connector (such as Camunda) are forwarded to the connector for handling.
        // This is necessary as setting up event listeners in Ethereum is only possible on a per block
        // basis which means after a restart also already processed events will be received.
        if (!latestProcessedBlocks.containsKey(tenantIdForThisNetworkInstance + allianceName + globalInstanceReference)) {
            logger.info("Ursprüngliche GAR: " + globalAllianceReference);
            Instantiation instantiation = new Instantiation(globalAllianceReference, globalInstanceReference, "", "", JSONVariables, name);

            logger.info("Subscribe for BPM events on: " + instantiation.getGlobalInstanceReference() + ", in EthereumRawNetwork::handleTaskAction.");

            HttpService httpService = new HttpService(super.getSciAddress());
            httpService.addHeader("Authorization", super.getSciAuthHeader());
            Web3j web3 = Web3j.build(httpService);

            //TODO: provide this field in constructor
            DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);

            String transactionHash = log.getTransactionHash();

            setupBlockConfirmationListener(web3, transactionHash, transactionReceipt -> {
                TenantContext.setTenantId(tenantIdForThisNetworkInstance);
                logger.info("In handle instantiation block listener the current tenant context is: " + TenantContext.getTenantId());
                dpexService.saveChangeInstantiationEthereum(globalInstanceReference, globalAllianceReference);
                // Continue after 15 block confirmations
                dpexService.handleInstantiation(instantiation);

                // Moved filter setup to hear so TaskActions are noticed only after instantiation is properly setup
                EthFilter filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, instantiation.getGlobalInstanceReference());
                bpmHandler = web3.ethLogFlowable(filter).subscribe(this::handleTaskAction);
                TenantContext.clear();

            });
        } else {
            logger.info("Instance skipped since already handled.");
        }
    }

    /**
     * Adds a new instantiation identified by its global instance reference to the latestProcessedBlocks
     * map to track processed instantiations. This is crucial for ensuring that the system only processes
     * new instantiations after a restart, thereby avoiding duplicate processing.
     *
     * @param globalinstref The global instance reference of the new instantiation
     */
    public void addInstantiation(String globalinstref) {
        // Safe current instantiation to make sure only new Instantiations are handled on restart
        synchronized (latestProcessedBlocks_mutex) {
            latestProcessedBlocks.put(tenantIdForThisNetworkInstance + allianceName + globalinstref, new TreeSet<>());
            prepareLatestProcessedBlocksForPersistence();
        }
    }

    /**
     * Sends a task action event to the frontend for signing.
     *
     * @param taskAction The task action
     */
    @Override
    @SuppressWarnings("unchecked")
    public void sendTaskAction(TaskAction taskAction) {
        logger.info("Send Task Action (with BpmRaw):\t" + taskAction);
        DPEXService.eventBus.post(new SendTaskActionToFrontendEvent(super.getSciAddress(), super.getSciAddress(), taskAction.getInstance().getGlobalInstanceReference(), taskAction.getTask().getActivity(), taskAction.getTaskLifeCycleStage().toString(), taskAction.getProcessVariables().generateJSONString(), taskAction.getUser().getSciId(), tenantIdForThisNetworkInstance));
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void handleTaskAction(Object event) {

        logger.info("Handle Task Action:\t" + event);

        Log log = (Log) event;

        List<Type> args = FunctionReturnDecoder.decode(log.getData(), DpexProcessInstance.EVENTEMITTER_EVENT.getParameters());

        logger.info("New event received for instance " + log.getAddress() + ": (" + args.get(0).getValue().toString() + ")");

        DpexProcessInstance.Event dpiEvent = (DpexProcessInstance.Event) args.get(0);

        // This conditional block ensures that only events not yet processed by a BPM connector (such as Camunda) are forwarded to the connector for handling.
        // This is necessary as setting up event listeners in Ethereum is only possible on a per-block
        // basis which means after a restart also already processed events will be received.
        if (!latestProcessedBlocks.get(tenantIdForThisNetworkInstance + allianceName + log.getAddress()).contains(dpiEvent.activity + dpiEvent.stage + dpiEvent.variables + dpiEvent.resource)) {
            TenantContext.setTenantId(tenantIdForThisNetworkInstance);
            DPEXService dpexService = ApplicationContextProvider.bean(DPEXService.class);
            DPEXUtils dpexUtils = ApplicationContextProvider.bean(DPEXUtils.class);

            Acknowledgement acknowledgement = dpexUtils.buildAcknowledgement(
                    log.getAddress(),
                    Task.builder().activity(dpiEvent.activity).build(),
                    TaskLifeCycleStage.valueOf(dpiEvent.stage),
                    User.builder().sciId(dpiEvent.resource).build(),
                    new ProcessVariables(dpiEvent.variables)
            );

            // Setup HttpService and Web3j
            HttpService httpService = new HttpService(super.getSciAddress());
            httpService.addHeader("Authorization", super.getSciAuthHeader());
            Web3j web3 = Web3j.build(httpService);

            String transactionHash = log.getTransactionHash();
            TenantContext.clear();


            setupBlockConfirmationListener(web3, transactionHash, transactionReceipt -> {
                TenantContext.setTenantId(tenantIdForThisNetworkInstance);
                // Necessary to go over service to have the change reflected in the database
                dpexService.saveChangeTaskActionEthereum(log.getAddress(), dpiEvent.activity + dpiEvent.stage + dpiEvent.variables + dpiEvent.resource);
                // Continue after 15 block confirmations
                dpexService.handleAcknowledgement(acknowledgement);
                TenantContext.clear();
            });
        } else {
            logger.info("TaskAction " + dpiEvent.activity + dpiEvent.stage + dpiEvent.variables + dpiEvent.resource + " skipped as it was already handled.");
        }
    }

    /**
     * Adds a task action identified by its unique event identifier to latestProcessedBlocks
     * that tracks processed events for a given address/instance. This method ensures that only new events
     * are loaded when the application restarts by updating the latestProcessedBlocks and its serialized form
     * latestProcessedBlocksPersistable.
     *
     * @param address               The address of the instance associated with the event (TaskAction).
     * @param uniqueEventIdentifier The unique identifier of the event (TaskAction) to be added.
     * @author Moritz Johlige
     * @version 0.2
     */
    public void addTaskAction(String address, String uniqueEventIdentifier) {
        // Safe current Block Number to make sure only new events are loaded once the application restarts
        synchronized (latestProcessedBlocks_mutex) {
            if (latestProcessedBlocks.get(tenantIdForThisNetworkInstance + allianceName + address) != null) {
                latestProcessedBlocks.get(tenantIdForThisNetworkInstance + allianceName + address).add(uniqueEventIdentifier);
            } else {
                logger.error("Event" + uniqueEventIdentifier + " could not be saved to DB because the instance does not exist in the corresponding data structure");
            }
            prepareLatestProcessedBlocksForPersistence();
        }
    }

    /**
     * Sets up an event listener for monitoring the confirmation status of a specific transaction
     * The callback function will be called after a certain number of blocks have been generated after the transaction.
     *
     * @param web3            The Web3j instance used for Ethereum interaction.
     * @param transactionHash The hash of the transaction to monitor.
     * @param onConfirmation  A callback function to be executed when the transaction is confirmed.
     * @author Moritz Johlige
     * @version 0.2
     */
    private void setupBlockConfirmationListener(Web3j web3, String transactionHash, Consumer<TransactionReceipt> onConfirmation) {
        final Disposable[] blockSubscriptionWrapper = new Disposable[1]; // Wrapper array

        blockSubscriptionWrapper[0] = web3.blockFlowable(false).subscribe(block -> {
            try {
                EthGetTransactionReceipt transactionReceipt = web3.ethGetTransactionReceipt(transactionHash).send();
                if (transactionReceipt.getTransactionReceipt().isPresent()) {
                    BigInteger transactionBlockNumber = transactionReceipt.getTransactionReceipt().get().getBlockNumber();
                    BigInteger latestBlockNumber = block.getBlock().getNumber();
                    logger.info("Currently at block: " + latestBlockNumber.subtract(transactionBlockNumber));
                    if (latestBlockNumber.subtract(transactionBlockNumber).compareTo(BigInteger.valueOf(this.blocksToWaitForFinalityP)) >= 0) {
                        onConfirmation.accept(transactionReceipt.getTransactionReceipt().get());
                        blockSubscriptionWrapper[0].dispose(); // Unsubscribe after confirmation
                    }
                }
            } catch (IOException e) {
                logger.error("Error while checking transaction receipt", e);
            }
        });
    }


    @Override
    public void sendAcknowledgement(Acknowledgement acknowledgement) {

    }

    @Override
    public void handleAcknowledgement(Object event) {

    }

    @Override
    public void sendDataUpdate(String to) {

    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public void getInstances() {

        logger.info("Get Instances");

        Web3j web3 = Web3j.build(new HttpService("http://localhost:8545"));

        DpexSci dpexSci = DpexSci.load(
                "0x3750be6d627b600d498150cc847a49e3f677c6c7",
                web3,
                new ClientTransactionManager(web3, "0xE076df4e49182f0AB6f4B98219F721Cccc38f9be"),
                new DefaultGasProvider());

        try {
            RemoteFunctionCall<TransactionReceipt> rfc = (RemoteFunctionCall<TransactionReceipt>) dpexSci.getClass().getMethod("get").invoke(dpexSci);
            Object returnObject = rfc.send();
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new ReflectionException();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
