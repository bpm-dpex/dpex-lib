package dpex.impl.collaboration.ethereum.raw;

import java.net.MalformedURLException;
import java.net.URL;

import javax.persistence.Entity;

import dpex.db.TenantContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dpex.collaboration.CollaborationFactory;
import dpex.collaboration.SCIConfig;
import dpex.impl.collaboration.consensus.TrueConsensus;
import dpex.impl.collaboration.security.NoSecurity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EthereumRawFactory extends CollaborationFactory {

    private static final Logger logger = LogManager.getLogger(EthereumRawFactory.class);

    private String connection;
    private String authHeader;

    public EthereumRaw create(SCIConfig sciConfig) {

        if(!(sciConfig instanceof EthereumRawConfig)) {
            throw new RuntimeException("The configuration object must be of type EthereumRawConfig.");
        }

        EthereumRawConfig ethereumRawConfig = (EthereumRawConfig) sciConfig;

        String sciContractAddress = ethereumRawConfig.getSciContractAddress();
        int blocksToWaitForFinality = ethereumRawConfig.getBlocksToWaitForFinality();
        EthereumRawNetwork ern = new EthereumRawNetwork(connection, sciContractAddress, authHeader, blocksToWaitForFinality, TenantContext.getTenantId(), ethereumRawConfig.getAllianceName());
        logger.info("Create new EthereumRaw Collaboration with:\t" + sciContractAddress);

        //TODO: Use SuperBuilder
        EthereumRaw eraw = EthereumRaw.builder()
                .build();
        eraw.setNetwork(ern);
        eraw.setSecurity(NoSecurity.class);
        eraw.setConsensus(TrueConsensus.class);

        eraw.setName(super.getName());

        try {
            eraw.setConnection(new URL(connection));
            return eraw;
        }
        catch(MalformedURLException malformedURLException) {
            throw new RuntimeException("Error creating SimpleCollaboration: " + malformedURLException.getMessage());
        }

    }

}
