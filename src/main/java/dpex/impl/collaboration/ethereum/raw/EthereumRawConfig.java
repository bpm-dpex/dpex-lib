package dpex.impl.collaboration.ethereum.raw;

import dpex.collaboration.SCIConfig;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EthereumRawConfig extends SCIConfig {
    private String sciContractAddress;
    private int blocksToWaitForFinality;
    private String allianceName;
}
