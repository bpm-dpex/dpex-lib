package dpex.impl.collaboration.ethereum.raw;

import dpex.collaboration.Collaboration;
import lombok.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.web3j.protocol.Web3j;
import org.web3j.tx.ClientTransactionManager;

import javax.persistence.Entity;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EthereumRaw extends Collaboration {

    private static final Logger logger = LogManager.getLogger(EthereumRaw.class);

    private static Web3j web3;
    private static ClientTransactionManager ctm;

    private String smartContractAddress;

}
