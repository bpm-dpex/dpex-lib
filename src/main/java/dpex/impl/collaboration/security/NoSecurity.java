package dpex.impl.collaboration.security;

import dpex.core.Message;
import dpex.collaboration.security.Security;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.function.BiFunction;
import java.util.function.Function;

@NoArgsConstructor
@Getter
@Setter
public class NoSecurity extends Security {


    @Override
    public String sign(Message message) { return ""; }

    @Override
    public boolean verify(String text, String signature, String publicKey) {
        return true;
    }

    @Override
    public boolean verify(Message message) { return true; }


    public BiFunction<String, String, String> sign = (text, secret) -> {
      return "";
    };

    public Function<Message, Boolean> verifyMessage = (message) -> {
        return true;
    };

}
