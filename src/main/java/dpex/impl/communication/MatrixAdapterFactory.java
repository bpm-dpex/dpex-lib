package dpex.impl.communication;

import javax.persistence.Entity;

import dpex.communication.Communication;
import dpex.communication.CommunicationFactory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class MatrixAdapterFactory extends CommunicationFactory {

    private String matrixServiceUrl;

    @Override
    public Communication create() {
        MatrixAdapter matrixAdapter = MatrixAdapter.builder().matrixServiceUrl(this.matrixServiceUrl).build();
        matrixAdapter.setName(super.getName());
        return matrixAdapter;
    }
}
