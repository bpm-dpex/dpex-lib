package dpex.impl.communication;

import javax.persistence.Entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import dpex.communication.Communication;
import dpex.communication.GetRoomMessageDTO;
import dpex.communication.RoomDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DiscordAdapter extends Communication {
	
	private static final Logger logger = LogManager.getLogger(MatrixAdapter.class);
    
    private static final RestTemplate restTemplate = new RestTemplate();

    private String discordServiceUrl;
    private String discordServerId;

    public String login(String userId, String accessToken, String notUsed) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		DiscordLoginDTO loginDTO = new DiscordLoginDTO(userId, accessToken);
		
		HttpEntity<DiscordLoginDTO> request = new HttpEntity<>(loginDTO, headers);
		String url = discordServiceUrl + "/login";
		
		ResponseEntity<DiscordLoginResponseDTO> result = restTemplate.postForEntity(url, request, DiscordLoginResponseDTO.class);
		DiscordLoginResponseDTO responseBody = result.getBody();
		
		logger.info("Logged in " + responseBody.getUsername());
		
		return accessToken;
	}
	
	public void logout(String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		LogoutDTO logoutDTO = new LogoutDTO(accessToken);
		
		HttpEntity<LogoutDTO> request = new HttpEntity<>(logoutDTO, headers);
		String url = discordServiceUrl + "/logout";
		
		ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.DELETE, request, String.class);
		String responseBody = result.getBody();
		
		logger.info(responseBody);
	}
	
	public String sendDirectMessage(String accessToken, String recipient, String message, String file, boolean fileIsImage) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		SendDirectMessageDTO sendDirectMessageDTO = new SendDirectMessageDTO(recipient, message, file, fileIsImage);
		
		HttpEntity<SendDirectMessageDTO> request = new HttpEntity<>(sendDirectMessageDTO, headers);
		String url = discordServiceUrl + "/sendDirectMessage";
		
		ResponseEntity<String> result = restTemplate.postForEntity(url, request, String.class);
		return result.getBody();
	}
	
	public void sendRoomMessage(String accessToken, String roomId, String message, String file, boolean fileIsImage) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		SendRoomMessageDTO sendRoomMessageDTO = new SendRoomMessageDTO(roomId, message, file, fileIsImage);
		
		HttpEntity<SendRoomMessageDTO> request = new HttpEntity<>(sendRoomMessageDTO, headers);
		String url = discordServiceUrl + "/sendRoomMessage";
		
		restTemplate.postForEntity(url, request, Void.class);
	}
	
	public String createSpace(String accessToken, String name, String[] invite, String gar) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		CreateSpaceDiscordDTO createSpaceDTO = new CreateSpaceDiscordDTO(name, invite, gar, discordServerId);
		
		HttpEntity<CreateSpaceDiscordDTO> request = new HttpEntity<>(createSpaceDTO, headers);
		String url = discordServiceUrl + "/createSpace";
		
		ResponseEntity<CreateSpaceResponseDTO> result = restTemplate.postForEntity(url, request, CreateSpaceResponseDTO.class);
		CreateSpaceResponseDTO responseBody = result.getBody();
		
		return responseBody.getSpaceId();
	}
	
	public String createRoom(String accessToken, String spaceId, String name, String[] invite) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		CreateRoomDiscordDTO createRoomDTO = new CreateRoomDiscordDTO(spaceId, name, invite, discordServerId);
		
		HttpEntity<CreateRoomDiscordDTO> request = new HttpEntity<>(createRoomDTO, headers);
		String url = discordServiceUrl + "/createRoom";
		
		ResponseEntity<CreateRoomResponseDTO> result = restTemplate.postForEntity(url, request, CreateRoomResponseDTO.class);
		CreateRoomResponseDTO responseBody = result.getBody();
		
		return responseBody.getRoomId();
	}
	
	public void leaveRoom(String accessToken, String roomId) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		LeaveRoomDiscordDTO leaveRoomDTO = new LeaveRoomDiscordDTO(roomId, discordServerId);
		
		HttpEntity<LeaveRoomDiscordDTO> request = new HttpEntity<>(leaveRoomDTO, headers);
		String url = discordServiceUrl + "/leaveRoom";
		
		ResponseEntity<String> result = restTemplate.postForEntity(url, request, String.class);
		String responseBody = result.getBody();
		
		logger.info(responseBody);
	}
	
	public void inviteToRoom(String accessToken, String roomId, String[] invite) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		InviteToRoomDiscordDTO inviteToRoomDTO = new InviteToRoomDiscordDTO(roomId, invite, discordServerId);
		
		HttpEntity<InviteToRoomDiscordDTO> request = new HttpEntity<>(inviteToRoomDTO, headers);
		String url = discordServiceUrl + "/inviteToRoom";
		
		restTemplate.postForEntity(url, request, Void.class);
	}
	
	public RoomDTO[] getRooms(String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		String url = discordServiceUrl + "/rooms/" + discordServerId;
		
		ResponseEntity<RoomDTO[]> result = restTemplate.exchange(url, HttpMethod.GET, request, RoomDTO[].class);
		return result.getBody();
	}
	
	public GetRoomMessageDTO[] getRoomMessages(String accessToken, String roomId) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		String url = discordServiceUrl + "/roomMessages/" + roomId;
		
		ResponseEntity<GetRoomMessageDTO[]> result = restTemplate.exchange(url, HttpMethod.GET, request, GetRoomMessageDTO[].class);
		return result.getBody();
	}

	@Override
	public String[] getRoomMembers(String accessToken, String roomId) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(accessToken);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		String url = discordServiceUrl + "/roomMembers/" + roomId;
		
		ResponseEntity<String[]> result = restTemplate.exchange(url, HttpMethod.GET, request, String[].class);
		return result.getBody();
	}

}
