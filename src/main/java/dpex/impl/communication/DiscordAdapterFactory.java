package dpex.impl.communication;

import javax.persistence.Entity;

import dpex.communication.Communication;
import dpex.communication.CommunicationFactory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class DiscordAdapterFactory extends CommunicationFactory {
	private String discordServiceUrl;
	private String discordServerId;

    @Override
    public Communication create() {
        DiscordAdapter discordAdapter = DiscordAdapter.builder().discordServiceUrl(discordServiceUrl).discordServerId(discordServerId).build();
        discordAdapter.setName(super.getName());
        return discordAdapter;
    }
}
