package dpex.impl.communication;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class MatrixLoginDTO {
	@JsonProperty(required = true)
	private String userId;
	
	@JsonProperty(required = true)
	private String password;
	
	@JsonProperty(required = true)
	private String baseUrl;
}

@Data
@AllArgsConstructor
class DiscordLoginDTO{
	@JsonProperty(required = true)
	private String userId;
	
	@JsonProperty(required = true)
	private String accessToken;
}

@Data
class MatrixLoginResponseDTO {
	@JsonProperty(required = true)
	private String accessToken;
}

@Data
class DiscordLoginResponseDTO {
	@JsonProperty(required = true)
	private String username;
}

@Data
@AllArgsConstructor
class LogoutDTO{
	@JsonProperty(required = true)
	private String accessToken;
}

@Data
@AllArgsConstructor
class SendDirectMessageDTO{
	@JsonProperty(required = true)
	private String recipient;
	
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = false)
	private String file;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}

@Data
@AllArgsConstructor
class SendRoomMessageDTO{
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = false)
	private String file;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}

@Data
@AllArgsConstructor
class CreateSpaceDTO{
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = true)
	private String[] invite;
	
	@JsonProperty(required = true)
	private String gar;
}

@Data
@AllArgsConstructor
class CreateSpaceDiscordDTO{
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = true)
	private String[] invite;
	
	@JsonProperty(required = true)
	private String gar;
	private String serverId;
}

@Data
class CreateSpaceResponseDTO{
	@JsonProperty(required = true)
	private String spaceId;
}

@Data
@AllArgsConstructor
class CreateRoomDTO{
	@JsonProperty(required = true)
	private String spaceId;
	
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = true)
	private String[] invite;
}

@Data
@AllArgsConstructor
class CreateRoomDiscordDTO{
	@JsonProperty(required = true)
	private String spaceId;
	
	@JsonProperty(required = true)
	private String name;
	
	@JsonProperty(required = true)
	private String[] invite;
	private String serverId;
}

@Data
class CreateRoomResponseDTO{
	@JsonProperty(required = true)
	private String roomId;
}

@Data
@AllArgsConstructor
class LeaveRoomDTO{
	@JsonProperty(required = true)
	private String roomId;
}

@Data
@AllArgsConstructor
class LeaveRoomDiscordDTO{
	@JsonProperty(required = true)
	private String roomId;
	private String serverId;
}

@Data
@AllArgsConstructor
class InviteToRoomDTO{
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private String[] invite;
}

@Data
@AllArgsConstructor
class InviteToRoomDiscordDTO{
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private String[] invite;
	private String serverId;
}