package dpex.impl.bpm.model.bpmn;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Activity extends BPMNElement {

    String name;

    SequenceFlow incoming;
    SequenceFlow outgoing;

    String annotation;

    public Activity(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {

        if(!(o instanceof Activity)) {
            return false;
        }

        Activity other = (Activity) o;

        return other.getName().equals(this.getName());
    }
}
