package dpex.impl.bpm.model.bpmn;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class EndEvent extends BPMNElement {

    SequenceFlow incoming;

    public EndEvent(SequenceFlow incoming) {
        this.incoming = incoming;
        super.setName("End");

    }
}
