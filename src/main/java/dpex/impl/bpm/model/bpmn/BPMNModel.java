package dpex.impl.bpm.model.bpmn;

import dpex.bpm.model.ProcessModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

/**
 * This is a container class for all elements of a BPMN model.
 * Currently, only few BPMN elements are supported, i.e.
 * Start Node, Activity, Sequence Flow, (XOR)-Gateway, End Node.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BPMNModel extends ProcessModel {

    StartEvent start;
    EndEvent end;
    Set<Activity> activities = new HashSet<>();
    Set<Gateway> gateways = new HashSet<>();
    Set<SequenceFlow> sequenceFlows = new HashSet<>();
    String processId;

    public Set<BPMNElement> getBPMNElements() {
        Set<BPMNElement> elements = new HashSet<>();
        elements.addAll(activities);
        elements.addAll(gateways);
        elements.addAll(sequenceFlows);
        elements.add(start);
        elements.add(end);

        return elements;
    }

    public static BPMNModel fromString(String model) {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        String processId;
        List<Node> startEventNodes = new ArrayList<>();
        List<Node> sequenceFlowNodes = new ArrayList<>();
        List<Node> taskNodes = new ArrayList<>();
        List<Node> parallelGatewayNodes = new ArrayList<>();
        List<Node> endEventNodes = new ArrayList<>();

        List<Node> textAnnotationNodes = new ArrayList<>();
        List<Node> associationNodes = new ArrayList<>();

        List<Node> allElements = new ArrayList<>();

        try {

            // parse xml
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(model.getBytes()));
            NodeList bpmnProcesses = doc.getElementsByTagName("bpmn:process");

            // only one process
            Node node = bpmnProcesses.item(0);

            processId = ((Element) node).getAttribute("id");

            NodeList bpmnElements = node.getChildNodes();

            // fill lists
            for (int i = 0; i < bpmnElements.getLength(); i++) {
                Node bpmnElement = bpmnElements.item(i);
                if (bpmnElement.getNodeType() == Node.ELEMENT_NODE) {

                    switch(bpmnElement.getNodeName()) {
                        case "bpmn:startEvent":
                            startEventNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                        case "bpmn:sequenceFlow":
                            sequenceFlowNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                        case "bpmn:userTask":
                            taskNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                        case "bpmn:parallelGateway":
                            parallelGatewayNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                        case "bpmn:endEvent":
                            endEventNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                        case "bpmn:textAnnotation":
                            textAnnotationNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                        case "bpmn:association":
                            associationNodes.add(bpmnElement);
                            allElements.add(bpmnElement);
                            break;
                    }
                }
            }

        } catch (ParserConfigurationException | IOException | RuntimeException | SAXException e) {
            throw new RuntimeException(e);
        }

        /**
         *      SEQUENCE FLOWS
         */
        Map<String, SequenceFlow> sequenceFlowIdMap = new HashMap<>();
        Set<SequenceFlow> sequenceFlows = new HashSet<>();
        for(Node sequenceFlowNode : sequenceFlowNodes) {

            String id = ((Element) sequenceFlowNode).getAttributeNode("id").getValue();

            SequenceFlow sequenceFlow = new SequenceFlow();

            sequenceFlows.add(sequenceFlow);
            sequenceFlowIdMap.put(id, sequenceFlow);
        }

        /**
         *      TASKS
         */
        Map<String, Activity> activityIdMap = new HashMap<>();
        Set<Activity> activities = new HashSet<>();
        for(Node taskNode : taskNodes) {

            String id = ((Element) taskNode).getAttributeNode("id").getValue();
            String name = ((Element) taskNode).getAttributeNode("name").getValue();

            Activity activity = new Activity(name);
            activity.setName(name);

            // Sequence Flow
            NodeList flows = taskNode.getChildNodes();
            for(int i = 0; i < flows.getLength(); i++) {
                Node flow = flows.item(i);
                switch(flow.getNodeName()) {
                    case "bpmn:incoming":
                        SequenceFlow isf = sequenceFlowIdMap.get(flow.getTextContent());
                        activity.setIncoming(isf);
                        isf.setTargetRef(activity);
                        break;
                    case "bpmn:outgoing":
                        SequenceFlow osf = sequenceFlowIdMap.get(flow.getTextContent());
                        activity.setOutgoing(osf);
                        osf.setSourceRef(activity);
                        break;

                }
            }

            activities.add(activity);
            activityIdMap.put(id, activity);

        }

        /**
         *      GATEWAYS
         */
        Set<Gateway> parallelGateways = new HashSet<>();
        for(Node parallelGatewayNode : parallelGatewayNodes) {

            String id = ((Element) parallelGatewayNode).getAttributeNode("id").getValue();

            Gateway gateway = new Gateway();
            Set<SequenceFlow> incomming = new HashSet<>();
            Set<SequenceFlow> outgoing = new HashSet<>();

            // Sequence Flow
            NodeList flows = parallelGatewayNode.getChildNodes();

            for(int i = 0; i < flows.getLength(); i++) {
                Node flow = flows.item(i);
                switch(flow.getNodeName()) {
                    case "bpmn:incoming":
                        SequenceFlow isf = sequenceFlowIdMap.get(flow.getTextContent());
                        incomming.add(isf);
                        isf.setTargetRef(gateway);
                        break;
                    case "bpmn:outgoing":
                        SequenceFlow osf = sequenceFlowIdMap.get(flow.getTextContent());
                        outgoing.add(osf);
                        osf.setSourceRef(gateway);
                        break;
                }
            }

            gateway.setIncoming(incomming);
            gateway.setOutgoing(outgoing);

            parallelGateways.add(gateway);

        }


        /**
         *      START EVENTS (1)
         */
        StartEvent startEvent = new StartEvent();
        for(Node startEventNode : startEventNodes) {

            String id = ((Element) startEventNode).getAttributeNode("id").getValue();


            // Sequence Flow
            Node outgoingFlow = startEventNode.getChildNodes().item(1);
            String a = outgoingFlow.getTextContent();
            SequenceFlow osf = sequenceFlowIdMap.get(outgoingFlow.getTextContent());
            startEvent.outgoing = osf;
            osf.setSourceRef(startEvent);

        }


        /**
         *      END EVENTS (1)
         */
        EndEvent endEvent = new EndEvent();
        for(Node endEventNode : endEventNodes) {

            String id = ((Element) endEventNode).getAttributeNode("id").getValue();


            // Sequence Flow
            Node incomingFlow = endEventNode.getChildNodes().item(1);
            SequenceFlow isf = sequenceFlowIdMap.get(incomingFlow.getTextContent());
            endEvent.incoming = isf;
            isf.setTargetRef(endEvent);

        }

        /**
         *      TEXT ANNOTATIONS
         */
         for(Node textAnnotationNode : textAnnotationNodes) {

            String idOfAnnotation = ((Element) textAnnotationNode).getAttributeNode("id").getValue();

            // find corresponding task
            Node associationNode = associationNodes.stream().filter(node -> ((Element) node).getAttributeNode("targetRef").getValue().equals(idOfAnnotation)).findFirst().orElseThrow();
            String activityId = ((Element) associationNode).getAttribute("sourceRef");
            Activity correspondingActivity = activityIdMap.get(activityId);

            // Annotation
            Node textNode = textAnnotationNode.getChildNodes().item(1);
            String annotation = textNode.getTextContent();

            correspondingActivity.setAnnotation(annotation);

        }

        BPMNModel bpmnModel = new BPMNModel();
        bpmnModel.setProcessId(processId);
        bpmnModel.setStart(startEvent);
        bpmnModel.setEnd(endEvent);
        bpmnModel.setActivities(activities);
        bpmnModel.setGateways(parallelGateways);
        bpmnModel.setSequenceFlows(sequenceFlows);

        return bpmnModel;

    }

}
