package dpex.impl.bpm.model.bpmn;

import lombok.Getter;
import lombok.Setter;

public abstract class BPMNElement {

    @Getter
    @Setter
    private String name;
}
