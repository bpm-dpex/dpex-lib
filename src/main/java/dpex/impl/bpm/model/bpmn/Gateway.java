package dpex.impl.bpm.model.bpmn;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class Gateway extends BPMNElement {

    Set<SequenceFlow> incoming;
    Set<SequenceFlow> outgoing;

    public Gateway(Set<SequenceFlow> incoming, Set<SequenceFlow> outgoing) {
        this.incoming = incoming;
        this.outgoing = outgoing;

        super.setName("Gateway");

    }

}
