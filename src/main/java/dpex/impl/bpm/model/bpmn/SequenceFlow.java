package dpex.impl.bpm.model.bpmn;


public class SequenceFlow extends BPMNElement {

    public BPMNElement sourceRef;
    public BPMNElement targetRef;


    public SequenceFlow() {
        super.setName("Flow");
    }

    public void setSourceRef(BPMNElement sourceRef) {
        this.sourceRef = sourceRef;
    }

    public void setTargetRef(BPMNElement targetRef) {
        this.targetRef = targetRef;
    }

}
