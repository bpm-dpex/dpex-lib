package dpex.impl.bpm.model.bpmn;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class StartEvent extends BPMNElement {

    SequenceFlow outgoing;

    public StartEvent(SequenceFlow outgoing) {
        this.outgoing = outgoing;
        super.setName("Start");
    }
}
