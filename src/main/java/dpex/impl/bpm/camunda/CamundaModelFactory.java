package dpex.impl.bpm.camunda;

import dpex.bpm.model.ProcessModelFactory;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CamundaModelFactory extends ProcessModelFactory {

    @Lob
    private byte[] bpmnModel;

    public CamundaModel create() {
        CamundaModel camundaModel = CamundaModel.builder().bpmnModel(this.bpmnModel).build();
        return CamundaModel.builder().bpmnModel(this.bpmnModel).build();
    }

}
