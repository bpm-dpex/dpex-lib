package dpex.impl.bpm.camunda;

import dpex.bpm.execution.BPMEngineFactory;
import dpex.bpm.model.ProcessModel;
import lombok.*;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CamundaExecutionEngineFactory extends BPMEngineFactory {

    private String camundaUrl;

    public CamundaExecutionEngine create(ProcessModel processModel) {

        CamundaModel camundaModel = (CamundaModel) processModel;

        CamundaExecutionEngine cem = CamundaExecutionEngine.builder().camundaUrl(this.camundaUrl).build();
        cem.setName(super.getName());
        cem.deploy(camundaModel.getBpmnModel());
        return cem;
    }

}
