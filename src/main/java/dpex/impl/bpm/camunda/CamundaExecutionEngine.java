package dpex.impl.bpm.camunda;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dpex.action.Action;
import dpex.action.TaskAction;
import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.model.ProcessModel;
import dpex.db.TenantContext;
import dpex.util.DpexHttpResponse;
import dpex.error.TaskNotFoundException;
import dpex.error.UnknownProcessModelException;
import dpex.impl.bpm.camunda.dto.GetTasksDTO;
import dpex.impl.bpm.camunda.dto.GetWorkListDTO;
import dpex.impl.bpm.camunda.dto.GetWorkListResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.Entity;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * Mapping namespaces DPEX -> Camunda:
 * ModelReference                       -> Definition Key
 * [not used] (local ModelReference)    -> Definition ID
 * Instance.localInstanceReference      -> processInstanceId
 */
@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
public class CamundaExecutionEngine extends BPMEngine {

    private static final Logger logger = LogManager.getLogger(CamundaExecutionEngine.class);
    private String camundaUrl;
    private static HttpClient httpClient = HttpClient.newHttpClient();
    private static ObjectMapper objectMapper = new ObjectMapper();

    public CamundaExecutionEngine(String camundaUrl) {
        this.camundaUrl = camundaUrl;
    }

    /**
     * Deploys a BPMN model to Camunda for the current tenant, utilizing the tenant ID from the TenantContext.
     * @param byteModel The BPMN model in bytes.
     */
    public void deploy(byte[] byteModel) {
        String boundary = UUID.randomUUID().toString();

        String part1 = "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"tenant-id\"" + "\r\n\r\n" +
                TenantContext.getTenantId() + "\r\n" +
                "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"data\"; filename=\"filename.bpmn\"" + "\r\n" +
                "Content-Type: application/octet-stream\r\n\r\n";

        String part2 = "\r\n--" + boundary + "--";

        byte[] beforeBytes = part1.getBytes(StandardCharsets.UTF_8);
        byte[] afterBytes = part2.getBytes(StandardCharsets.UTF_8);
        byte[] finalData = new byte[beforeBytes.length + byteModel.length + afterBytes.length];

        System.arraycopy(beforeBytes, 0, finalData, 0, beforeBytes.length);
        System.arraycopy(byteModel, 0, finalData, beforeBytes.length, byteModel.length);
        System.arraycopy(afterBytes, 0, finalData, beforeBytes.length + byteModel.length, afterBytes.length);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(this.camundaUrl + "deployment/create"))
                .header("Content-Type", "multipart/form-data; boundary=" + boundary)
                .POST(HttpRequest.BodyPublishers.ofByteArray(finalData))
                .build();

        try {
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Error deploying process model to Camunda.", e);
        }
    }

    /**
     * Instantiates a process modell within the current tenant
     * @param modelReference
     * @param businessKey    buinessKey to be set in the instantiation
     * @param JSONVariables  at least "[{}]", variables in the form:
     *                       {\"IntKey\" : 1, \"StringKey\" : \"TestValue\", \"BooleanKey\" : true}
     * @param name			 the name of the new instance
     * @return Instance Object of the instantiated process
     * @throws UnknownProcessModelException
     */
    @Override
    public Instance instantiate(String modelReference, String businessKey, String JSONVariables, String name) throws UnknownProcessModelException {
        String tenantId = TenantContext.getTenantId();

        String executeUrl = camundaUrl + "process-definition/key/" + modelReference + "/tenant-id/" + tenantId + "/start";

        ProcessVariables processVariables = new ProcessVariables(JSONVariables);
        String body = processVariables.generateJSONCamundaString();

        DpexHttpResponse response = sendRequest(executeUrl, body);
        if (response.getCode() == 200) {
            JSONObject obj = new JSONObject(response.getBody());
            String localInstanceString = obj.getString("id");

            return new Instance(localInstanceString, null, name);
        } else {
            throw new UnknownProcessModelException(modelReference);
        }
    }

    /**
     * Check if the given task is listed in the work list
     *
     * @param instance
     * @param taskAction
     * @return
     */
    @Override
    public boolean isConform(ProcessModel processModel, Instance instance, TaskAction taskAction) {

        switch (taskAction.getTaskLifeCycleStage()) {
            case CLAIM: {
                List<Task> workList = this.getWorkList(taskAction.getInstance());
                for (Task t : workList) {
                    if (t.getActivity().equals(taskAction.getTask().getActivity())) {
                        if (!(t.getResource() == null)) {
                            throw new RuntimeException("Already claimed.");
                        }
                        return true;
                    }
                }
            }
            break;
            case COMPLETE: {
                List<Task> userWorkList = this.getWorkListByUser(taskAction.getInstance(), taskAction.getUser().getBpmId());
                if(userWorkList.contains(new Task(taskAction.getTask().getActivity(), taskAction.getUser().getBpmId()))) {
                    if (checkVarType(taskAction.getProcessVariables())) {
                        return true;
                    }
                }
            }
            break;
        }

        return false;

    }

    /**
     *
     * @param processVariables
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    private boolean checkVarType(ProcessVariables processVariables) {
        HashMap<String, Object> variables = processVariables.getVariables();

        String[] validTypes = {"Boolean", "Short", "Integer", "Long", "Double", "String"};
        List<String> list = Arrays.stream(validTypes).toList();
        if(!variables.isEmpty()) {
            for (Map.Entry<String, Object> variable : variables.entrySet()) {
                if (!(list.contains(variable.getValue().getClass().getSimpleName()))) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void claim(Instance instance, TaskAction taskAction) throws TaskNotFoundException {

        String taskName = taskAction.getTask().getActivity();
        String taskId = getTaskIdFromTaskName(taskName, instance.getLocalInstanceReference());

        String executeUrl = camundaUrl + "task/" + taskId + "/claim";
        sendRequest(executeUrl, "{\"userId\": \""+taskAction.getUser().getBpmId()+"\"}");

    }


    /**
     * In camunda, the task id is unique over all instances. The instance id is not used.
     *
     * @param instance
     * @param action
     */
    @Override
    public void execute(Instance instance, Action action) throws TaskNotFoundException {

        String taskName = ((TaskAction) action).getTask().getActivity();
        String taskId = getTaskIdFromTaskName(taskName, instance.getLocalInstanceReference());

        String executeUrl = camundaUrl + "task/" + taskId + "/complete";

        ProcessVariables processVariables = ((TaskAction) action).getProcessVariables();

        //process variable update not possible to combine with the task complete REST Call
        if(!processVariables.getVariables().isEmpty()) {
            logger.info("update process Variables" + processVariables);
            try {
                updateInstanceVariables(instance.getLocalInstanceReference(), processVariables);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }

        sendRequest(executeUrl, "{}");

    }

    @Override
    public List<Task> getWorkList(Instance instanceReference) {

        String getWorkListUrl = camundaUrl + "task";
        String body = "";

        try {
            body = objectMapper.writeValueAsString(new GetWorkListDTO(instanceReference.getLocalInstanceReference(), null));
        } catch (JsonProcessingException jpe) {
        }

        return getTasks(getWorkListUrl, body);

    }

    @Override
    public List<Task> getWorkListByUser(Instance instanceReference, String userReference) {

        String getWorkListUrl = camundaUrl + "task";
        String body = "";

        try {
            body = objectMapper.writeValueAsString(new GetWorkListDTO(instanceReference.getLocalInstanceReference(), userReference));
        } catch (JsonProcessingException jpe) {
        }

        return getTasks(getWorkListUrl, body);

    }
    
    /*The dpex framework does not specify any organizational model
	So in addition to all claimed tasks by the user all unclaimed tasks will be returned
	This method may be overridden by subclasses to implement e.g. organizational constraints*/
	@Override
	public List<Task> getIndividualWorkList(ProcessModel processModel, Instance instanceReference, String userReference) {
		//all tasks that have been claimed by the user
		List<Task> worklist = getWorkListByUser(instanceReference, userReference);
		//all tasks that can be executed now but are unclaimed
		worklist.addAll(getWorkList(instanceReference).stream().filter(task -> task.getResource() == null).collect(Collectors.toList()));
		return worklist;
	}

    @NotNull
    private List<Task> getTasks(String getWorkListUrl, String body) {
        DpexHttpResponse response = sendRequest(getWorkListUrl, body);
        if (response.getCode() == 200) {

            JSONArray arr = new JSONArray(response.getBody());
            List<GetWorkListResponse> myObjects = new ArrayList<>();

            arr.forEach(obj -> {
                try {
                    String thstring = ((JSONObject) obj).toString();
                    GetWorkListResponse gworkListResponselr = objectMapper.readValue(thstring, GetWorkListResponse.class);
                    myObjects.add(gworkListResponselr);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });

            List<Task> returnList = new ArrayList<>();

            for (GetWorkListResponse workListResponse : myObjects) {
                returnList.add(new Task(workListResponse.getName(), workListResponse.getAssignee()));
            }
            return returnList;

        } else {
            throw new RuntimeException("TODO: Handle this exception");
        }
    }

    /**
     * Method to update the process variables with an arbitrary amount of variables
     *
     * @param instanceReference process instance where the variables should be updated/created
     * @param processVariables  processVariable Instance that contains all variables to be updated
     * @throws JsonProcessingException
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    private void updateInstanceVariables(String instanceReference, ProcessVariables processVariables) throws JsonProcessingException {
        String executeUrl = camundaUrl + "process-instance/" + instanceReference + "/variables/";

        Map<String, Object> vars = processVariables.getVariables();
        for (Map.Entry<String, Object> variable : vars.entrySet()) {
            String varName = variable.getKey();
            Object value = variable.getValue();
            String varType = value.getClass().getSimpleName();

            String body = "{\"value\" : \"" + value + "\" , \"type\" : \"" + varType + "\" }";

            sendPutRequest(executeUrl + varName, body);
        }
    }

    /**
     *
     * @param localInstanceReference
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    @Override
    public ProcessVariables getInstanceVariables(String localInstanceReference) {
        String varUrl = camundaUrl + "process-instance/" + localInstanceReference + "/variables";

        DpexHttpResponse response = sendRequest(varUrl, "{}");
        ProcessVariables processVariables = new ProcessVariables();

        processVariables.addFromJSON(response.getBody());

        return processVariables;
    }

    /**
     *
     * @param localInstanceReference
     * @param varName
     * @return
     *
     * @author Tobias Gretsch
     * @verison v0.0.11
     */
    @Override
    public String deleteInstanceVariable(String localInstanceReference, String varName) {
        String varUrl = camundaUrl + "process-instance/" + localInstanceReference + "variables/" + varName;

        DpexHttpResponse response = sendDeleteRequest(varUrl);
        return response.getBody();
    }

    /**
     *
     * @param taskId
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    private String getTaskVariables(String taskId) {
        //TODO Müssen hier die Werte als deserialized Value von Camunda kommen oder erleichtert es die Sache wenn hier
        // einfach mit den "normalen" Rückgabetypen gearbeitet wird
        String taskUrl = camundaUrl + "task/" + taskId + "variables?deserializeValues=false";

        DpexHttpResponse response = sendRequest(taskUrl, "{}");
        return response.getBody();
    }

    private String getTaskIdFromTaskName(String taskName, String instance) throws TaskNotFoundException {

        String getTasksUrl = camundaUrl + "task/";
        String body = "";

        try {
            body = objectMapper.writeValueAsString(new GetTasksDTO(instance, taskName));
        } catch (JsonProcessingException jpe) {
            //TODO handle Exception
        }

        DpexHttpResponse response = sendRequest(getTasksUrl, body);

        List<GetWorkListResponse> myObjects = new ArrayList<>();
        try {
            myObjects = objectMapper.readValue(response.getBody(), new TypeReference<List<GetWorkListResponse>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        if (myObjects.size() > 1) {
            throw new TaskNotFoundException("should not unique");
        }
        if (myObjects.isEmpty()) {
            throw new TaskNotFoundException("task not found");
        }

        return myObjects.get(0).getId();

    }

    public DpexHttpResponse sendRequest(String url, String body) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        try {
            HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return new DpexHttpResponse(response.statusCode(), response.body());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;

    }

    /**
     *
     * @param url
     * @param body
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    public DpexHttpResponse sendPutRequest(String url, String body) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString(body))
                .build();

        try {
            HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return new DpexHttpResponse(response.statusCode(), response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     *
     * @param url
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    public DpexHttpResponse sendDeleteRequest(String url) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .DELETE()
                .build();

        try {
            HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return new DpexHttpResponse(response.statusCode(), response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

}







