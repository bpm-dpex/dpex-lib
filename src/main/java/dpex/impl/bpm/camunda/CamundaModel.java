package dpex.impl.bpm.camunda;

import dpex.bpm.model.ProcessModel;
import dpex.error.IncompatibleModelException;
import lombok.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
public class CamundaModel extends ProcessModel {

    @Lob
    private byte[] bpmnModel;

    public CamundaModel(byte[] bpmnModel) {
        this.bpmnModel = bpmnModel;
    }


}
