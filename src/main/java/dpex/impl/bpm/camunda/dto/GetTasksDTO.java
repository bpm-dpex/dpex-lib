package dpex.impl.bpm.camunda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public
class GetTasksDTO {
    String processInstanceId;
    String name;
}
