package dpex.impl.bpm.camunda.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public
class GetWorkListResponse {
    String id;
    String name;
    String assignee;
    Date created;
    String executionId;
    String processDefinitionId;
    String processInstanceId;
    String taskDefinitionKey;

}
