package dpex.impl.bpm.camunda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public
class GetWorkListByUserDTO {
    String processInstanceId;
}
