package dpex.action;

import dpex.core.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


/**
 * The UserAction class encapsulates every action that is triggered by a human user such as TaskCompletion.
 * In contrast, the automated execution of a (service) task is not an UserAction.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public abstract class UserAction extends Action {

    private User user;

}
