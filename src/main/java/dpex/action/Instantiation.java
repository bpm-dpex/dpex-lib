package dpex.action;

import dpex.core.Message;
import lombok.*;

/**
 * The instantiation message is used, when a user wants to instantiate a process model.
 * //TODO: Discuss whether the instantiation class should be a subclass of Action.
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Instantiation extends Message {

    String globalAllianceReference;
    String globalInstanceReference;
    String modelReference;
    String businessKey = "";
    String JSONVariables;
    String name;

}
