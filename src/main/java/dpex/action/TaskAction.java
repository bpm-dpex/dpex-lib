package dpex.action;

import dpex.bpm.execution.Instance;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class TaskAction extends UserAction {

    private Instance instance;
    private Task task;
    private ProcessVariables processVariables;

    private TaskLifeCycleStage taskLifeCycleStage;

    @Override
    public boolean equals(Object o) {

        if(!(o instanceof TaskAction)) {
            return false;
        }

        TaskAction other = (TaskAction) o;

        return other.getTask().equals(this.getTask()) && other.getInstance().getGlobalInstanceReference().equals(this.getInstance().getGlobalInstanceReference());

    }
}
