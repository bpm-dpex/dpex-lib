package dpex.action;

import dpex.core.Message;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * This abstract class encapsulates every action that can happen during process execution.
 * For example, the automated execution of a (script) task, the user triggered execution of a (manual) task,
 * or the update of a data value.
 */
@SuperBuilder
@NoArgsConstructor
public abstract class Action extends Message {
}
