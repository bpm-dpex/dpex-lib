package dpex.collaboration.network;

import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.collaboration.consensus.Acknowledgement;
import lombok.Data;

import javax.persistence.*;

/**
 * This interface is implemented by any class that is responsible for receiving and sending messages in applications
 * that include the dpex-lib.
 *
 * In some cases, the dpex-lib is not automatically notified on new events, for example when a external collaboration system such as a blockchain is used.
 * Then, ConnectorListeners must be implemented.
 */
@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class BaseNetwork {

    @Id
    @GeneratedValue
    private Long id;

    private String sciAddress;
    private String sciAuthHeader;
    public String tenantIdForThisNetworkInstance;

    public abstract void sendInstantiation(Instantiation instantiation);
    public abstract void handleInstantiation(Object event);

    public abstract void sendTaskAction(TaskAction taskAction);
    public abstract void handleTaskAction(Object event);

    public abstract void sendAcknowledgement(Acknowledgement acknowledgement);
    public abstract void handleAcknowledgement(Object event);

    public abstract void sendDataUpdate(String recipient);

}
