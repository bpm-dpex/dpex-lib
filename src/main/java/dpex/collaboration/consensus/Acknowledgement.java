package dpex.collaboration.consensus;

import dpex.action.TaskAction;
import dpex.core.Message;
import lombok.*;

/**
 * An Acknowledgement is sent to indicate that certain TaskCompletion is acceptable.
 * The reception of an acknowledgement message triggers the Consensus component to check weather global acceptance can be assumed.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Acknowledgement extends Message {

    private TaskAction action;

}
