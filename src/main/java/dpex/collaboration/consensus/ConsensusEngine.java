package dpex.collaboration.consensus;

import dpex.collaboration.Collaboration;
import dpex.collaboration.CandidateLog;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * The ConsensusEngine governs all received acknowledgement messages in the CandidateLog field.
 * The ConsensusEngine also provides the functionality to check, if global agreement can be accepted, based on the CandidateLog.
 */
@Data
@NoArgsConstructor
public abstract class ConsensusEngine {

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    protected CandidateLog candidateLog = new CandidateLog();

    //TODO: Add a comment, why we need a backreference to Collaboration here.
    private Collaboration collaboration;

    public abstract boolean isAgreementReached(Acknowledgement a, CandidateLog clog, String... values);

}
