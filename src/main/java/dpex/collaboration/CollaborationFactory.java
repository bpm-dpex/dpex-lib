package dpex.collaboration;

import lombok.Data;

import javax.persistence.*;

/**
 * The CollaborationFactory can be instanced to Collaborations, which are directly assigned to an alliance.
 * The factory enables a strict decoupling of alliances with their collaborations.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@Data
public abstract class CollaborationFactory {

    @Id
    private String name;

    @Column(insertable = false, updatable = false)
    private String type;

    String connection;

    public abstract Collaboration create(SCIConfig config);

}
