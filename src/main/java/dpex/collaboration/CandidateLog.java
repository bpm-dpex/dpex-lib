package dpex.collaboration;

import dpex.core.Event;
import dpex.core.Log;
import lombok.*;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.util.HashMap;
import java.util.Map;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class CandidateLog extends Log {

    private static int timestamp;

    @ElementCollection(fetch = FetchType.EAGER)
    Map<String, Integer> numberOfAcknowledgements;


    @Override
    public void addEvent(Event event) {
        event.setTimestamp(++timestamp);
        super.addEvent(event);
    }

    public void addAcknowledgementToEvent(String task) {
        if(numberOfAcknowledgements.containsKey(task)) {
            numberOfAcknowledgements.put(task, numberOfAcknowledgements.get(task)+1);
        }
        else {
            this.numberOfAcknowledgements.put(task, 1);
        }



    }


}
