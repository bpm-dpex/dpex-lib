package dpex.collaboration;

import dpex.collaboration.consensus.ConsensusEngine;
import dpex.collaboration.network.BaseNetwork;
import dpex.collaboration.security.Security;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import java.net.URL;

/**
 * The Collaboration class governs all tasks related to the collaboration tier, i.e. security, consensus and network.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@Data
@NoArgsConstructor
public abstract class Collaboration {

    private static final Logger logger = LogManager.getLogger(Collaboration.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Discriminator column in database
     */
    @Column(insertable = false, updatable = false)
    private String type;

    private String name;

    //TODO: can usages use getNetwork().getConnection ?
    protected URL connection;

    @OneToOne(cascade = CascadeType.ALL)
    BaseNetwork network;

    Class<? extends ConsensusEngine> consensus;
    Class<? extends Security>  security;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    protected CandidateLog candidateLog = new CandidateLog();

    public Long getId() {
        return id;
    }

    public Collaboration(URL connection) {
        this.connection = connection;
    }

}
