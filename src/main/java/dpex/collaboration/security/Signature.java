package dpex.collaboration.security;

import dpex.core.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * The Signature is the result when a message is signed by a particular user using his private key.
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class Signature {

    @Getter
    private String signature;
    private User user;

}
