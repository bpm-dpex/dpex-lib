package dpex.collaboration.security;

import dpex.core.Message;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A core security requirement is the non-repudiation of messages for instance.
 * This security class provides methods for signing messages and verifying the signature thereof.
 *
 * The security class may be extended when additional security requirements are detected, for instance privacy or non-disclosure of personal data.
 */
@Data
@NoArgsConstructor
public abstract class Security {

    public abstract String sign(Message message);
    public abstract boolean verify(String text, String signature, String publicKey);
    public abstract boolean verify(Message message);

}
