package dpex.collaboration;

public enum CollaborationType {
	ETHEREUM_RAW("Ethereum Raw"),
	HYPERLEDGER("Hyperledger"),
	BFTSMART("BFT Smart");
	
	public final String label;
	
	private CollaborationType(String label) {
		this.label = label;
	}
}
