package dpex.logger;

public enum LogEntryObjectType {
    INSTANTIATION,
    TASK_CLAIM,
    TASK_COMPLETE
}
