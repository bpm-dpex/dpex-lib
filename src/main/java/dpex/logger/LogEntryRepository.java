package dpex.logger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogEntryRepository extends JpaRepository<LogEntry, Long> {

    @Query("SELECT le FROM LogEntry le WHERE le.a.name = ?1")
    List<LogEntry> findByAllianceName(String allianceName);

    @Query("SELECT le FROM LogEntry le WHERE le.id > ?1 ORDER BY le.id desc ")
    List<LogEntry> findLatest(Long id);
}
