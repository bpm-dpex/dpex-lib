package dpex.logger;

import dpex.core.Alliance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime dateTime;

    @ManyToOne
    private Alliance a;

    @Enumerated
    private LogEntryType logEntryType;

    @Enumerated
    private LogEntryObjectType logEntryObjectType;

    private String objectName;

    private String message;



}
