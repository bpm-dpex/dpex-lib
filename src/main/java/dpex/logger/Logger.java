package dpex.logger;

import dpex.core.Alliance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class Logger {

    @Autowired LogEntryRepository log;

    public void log(Alliance alliance, LogEntryType logEntryType, LogEntryObjectType logEntryObjectType, String objectName, String message) {
        log.save(new LogEntry(null,
                LocalDateTime.now(),
                alliance,
                logEntryType,
                logEntryObjectType,
                objectName,
                message));
    }

}
