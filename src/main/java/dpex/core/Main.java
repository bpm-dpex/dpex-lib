package dpex.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * The dpex-lib project is not an application itself but a library that can be included in another application,
 * which provides additional functionality such as user management, user interface or the administration of database connections.
 *
 * //TODO: Check, if this main class is necessary.
 */
@EntityScan("dpex")
@ComponentScan("dpex")
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

    }
}
