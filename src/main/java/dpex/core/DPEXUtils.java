package dpex.core;

import java.lang.reflect.InvocationTargetException;

import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.ClientTransactionManager;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.DefaultGasProvider;

import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.bpm.model.ProcessModel;
import dpex.collaboration.SCIConfig;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.security.Signature;
import dpex.db.AllianceRepository;
import dpex.db.BPMEngineFactoryRepository;
import dpex.db.CollaborationFactoryRepository;
import dpex.db.CommunicationFactoryRepository;
import dpex.db.InstanceRepository;
import dpex.db.ProcessModelFactoryRepository;
import dpex.impl.collaboration.ethereum.raw.DpexSci;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DPEXUtils {

    private final ProcessModelFactoryRepository processModelFactoryRepository;
    private final BPMEngineFactoryRepository BPMEngineFactoryRepository;
    private final CollaborationFactoryRepository collaborationFactoryRepository;
    private final CommunicationFactoryRepository communicationFactoryRepository;
    private final InstanceRepository instanceRepository;
    private final AllianceRepository allianceRepository;


    public String deploySci(String connection, String sciId, String authHeader) {

    	HttpService httpService = new HttpService(String.valueOf(connection));
        httpService.addHeader("Authorization", authHeader);
        Web3j web3 = Web3j.build(httpService);
        ClientTransactionManager ctm = new ClientTransactionManager(web3, sciId);
        RemoteCall<Contract> rcDr = null;
        try {
            rcDr = (RemoteCall<Contract>) DpexSci.class.getDeclaredMethod("deploy", Web3j.class, TransactionManager.class, ContractGasProvider.class)
                    .invoke(Util.emptyObject(), web3, ctm, new DefaultGasProvider());
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        Contract dr = null;
        try {
            dr = rcDr.send();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dr.getContractAddress();

    }

    public Alliance buildAlliance(String name, String model, String engine, String collaboration, String communication, String globalAllianceReference, SCIConfig sciConfig) {

        Alliance alliance = new Alliance();
        alliance.setName(name);
        ProcessModel processModel = processModelFactoryRepository.findById(model).orElseThrow().create();
        alliance.setModel(processModel);
        alliance.setEngine(BPMEngineFactoryRepository.findById(engine).orElseThrow().create(processModel));
        alliance.setCollaboration(collaborationFactoryRepository.findById(collaboration).orElseThrow().create(sciConfig));
        if(communication != null)
        	alliance.setCommunication(communicationFactoryRepository.findById(communication).orElseThrow().create());
        alliance.setGlobalAllianceReference(globalAllianceReference);
        return alliance;

    }

    public Instantiation buildInstantiation(String globalAllianceReference, String modelReference, User user, String JSONVariables, String name) {

        Instantiation instantiation = new Instantiation();
        instantiation.setModelReference(modelReference);
        instantiation.setGlobalInstanceReference("Global Instance Reference");
        instantiation.setGlobalAllianceReference(globalAllianceReference);
        instantiation.setJSONVariables(JSONVariables);
        instantiation.setName(name);
        Signature sig = new Signature();
        sig.setUser(user);
        instantiation.setSender(sig);

        return instantiation;
    }

    public Acknowledgement buildAcknowledgement(String gir, Task task, TaskLifeCycleStage stage, User user, ProcessVariables processVariables) {

        Acknowledgement acknowledgement = new Acknowledgement();

        TaskAction taskAction = TaskAction.builder()
                .task(task)
                .taskLifeCycleStage(stage)
                .processVariables(processVariables)
                .instance(
                        instanceRepository.findByGlobalInstanceReference(gir).orElseThrow()
                ).build();

        acknowledgement.setAction(taskAction);

        Signature signature = new Signature("signature", user);

        //Since we want to save the bpm ID in the log, we need to find the actual user object from the alliance where the bpm ID is saved
        Alliance alliance = allianceRepository.findByGlobalInstanceReference(gir).orElseThrow();
        User userFromRepo = alliance.getModel().getParticipants().stream().filter(user_ -> user_.getSciId().toLowerCase().equals(user.getSciId())).findFirst().orElseThrow();
        String bpmId = userFromRepo.getBpmId();

        //Cannot use userFromRepo here because there the sciId is stored with capital letters but we need them in lower case
        user.setBpmId(bpmId);
        taskAction.setUser(user);

        acknowledgement.setSender(signature);

        return acknowledgement;

    }
}
