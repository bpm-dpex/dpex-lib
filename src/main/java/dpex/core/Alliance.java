package dpex.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dpex.bpm.execution.BPMEngine;
import dpex.bpm.model.ProcessModel;
import dpex.collaboration.Collaboration;
import dpex.collaboration.consensus.ConsensusEngine;
import dpex.collaboration.network.BaseNetwork;
import dpex.collaboration.security.Security;
import dpex.communication.Communication;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import java.lang.reflect.InvocationTargetException;

/**
 * An Alliance represents the joint venture of multiple companies that have agreed upon the specified protocol,
 * i.e. the model with its semantic interpretation in the engine, e.g. BPMN and the collaboration rules, e.g. Ethereum.
 */
@Builder
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Alliance {

    private static final Logger logger = LogManager.getLogger(Alliance.class);

    @Id
    private String name;

    // Alliances are identified by means of the globalAllianceReference which is added to all messages
    private String globalAllianceReference;

    @OneToOne(cascade = CascadeType.ALL)
    private ProcessModel model;

    @OneToOne(cascade = CascadeType.ALL)
    private BPMEngine engine;

    @OneToOne(cascade = CascadeType.ALL)
    private Collaboration collaboration;

    @OneToOne(cascade = CascadeType.ALL)
    private Communication communication;


    /*
    @JsonIgnore
    public BaseNetwork getNetwork() {
        try {
            return this.collaboration.getNetwork().getDeclaredConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

     */

    @JsonIgnore
    public Security getSecurity() {
        try {
            return this.collaboration.getSecurity().getDeclaredConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    @JsonIgnore
    public ConsensusEngine getConsensusEngine() {
        try {
            return this.collaboration.getConsensus().getDeclaredConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

}
