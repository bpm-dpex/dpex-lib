package dpex.core;

import static dpex.logger.LogEntryObjectType.INSTANTIATION;
import static dpex.logger.LogEntryObjectType.TASK_CLAIM;
import static dpex.logger.LogEntryObjectType.TASK_COMPLETE;
import static dpex.logger.LogEntryType.AGREEMENT_REACHED;
import static dpex.logger.LogEntryType.ENGINE_UPDATED;
import static dpex.logger.LogEntryType.HANDLE_ACK;
import static dpex.logger.LogEntryType.HANDLE_INSTANTIATION;
import static dpex.logger.LogEntryType.HANDLE_TASK_COMPLETE;
import static dpex.logger.LogEntryType.INSTANCE_CREATED;
import static dpex.logger.LogEntryType.SEND_ACK;
import static dpex.logger.LogEntryType.SEND_INSTANTIATION;
import static dpex.logger.LogEntryType.SEND_TASK_CLAIM;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bftsmartcommunicationtypes.PEvent;
import dpex.db.TenantContext;
import dpex.events.TaskActionEvent;
import dpex.impl.collaboration.bftsmart.BftSmartFactory;
import dpex.impl.collaboration.bftsmart.BftSmartNetwork;
import dpex.impl.collaboration.ethereum.raw.EthereumRawNetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.common.eventbus.EventBus;

import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.ProcessVariables;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.bpm.model.ProcessModel;
import dpex.collaboration.CandidateLog;
import dpex.collaboration.Collaboration;
import dpex.collaboration.consensus.Acknowledgement;
import dpex.collaboration.consensus.ConsensusEngine;
import dpex.collaboration.network.BaseNetwork;
import dpex.collaboration.security.Security;
import dpex.collaboration.security.Signature;
import dpex.db.AllianceRepository;
import dpex.db.InstanceRepository;
import dpex.error.TaskNotFoundException;
import dpex.events.InstantiationEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class DPEXService {

    private static final Logger logger = LogManager.getLogger(DPEXService.class);
    
    public static final EventBus eventBus = new EventBus();

    private final dpex.logger.Logger dpexLogger;

    private final AllianceRepository allianceRepository;

    private final InstanceRepository instanceRepository;

    /**
     * This is called from the UI, when a User wants to trigger the instantiation of a {@link ProcessModel}.
     * First, the {@link Instantiation} message is signed by the {@link Security} artefact.
     * Then, the respective {@link BaseNetwork} component of the given {@link Alliance} is called to send the message to the SCI.
     *
     * @param alliance The {@link Alliance} that provides the {@link Security} and {@link Collaboration} ({@link BaseNetwork}) artefacts.
     * @param instantiation The {@link Instantiation} that to be sent.
     */
    public void sendInstantiation(Alliance alliance, Instantiation instantiation) {

        dpexLogger.log(alliance, SEND_INSTANTIATION, INSTANTIATION, instantiation.getGlobalInstanceReference(),  "Send Instantiation");

        // sign the message
        Security security = alliance.getSecurity();
        security.sign(instantiation);

        // send the message to each recipient
        logger.info("Call Network to sendInstantiation:\t" + instantiation);
        logger.info("Send Instantiation with Context: " + TenantContext.getTenantId());
        Collaboration collaboration = alliance.getCollaboration();
        BaseNetwork network = collaboration.getNetwork();
        network.sendInstantiation(instantiation);

    }

    /**
     * This method is called from a {@link BaseNetwork} component, when a new event is received from the SCI.
     * First, the {@link Alliance} is fetched, based on the given {@link Instantiation#getGlobalInstanceReference()}.
     * Then, the {@link Security} component checks the {@link Signature} of the {@link Instantiation} message.
     * <b>Currently, the SCI event does not contain an identifier of the {@link ProcessModel} to be instantiated, hence,
     * we extract the model reference from the available {@link Alliance} object. </b>
     * Afterward, the respective {@link BPMEngine} is called to instantiate the {@link ProcessModel} whereby a {@link Instance#getGlobalInstanceReference()} is created.
     * The {@link Instance} is persisted in the database along with the {@link BPMEngine} of the given {@link Alliance}.
     *
     * @param instantiation The {@link Instantiation} that must be handled.
     */
    public void handleInstantiation(Instantiation instantiation) {


        Alliance alliance = allianceRepository.findByGlobalAllianceReference(instantiation.getGlobalAllianceReference()).orElseThrow();

        logger.info("Handle Instantiation:\t" + instantiation);
        dpexLogger.log(alliance, HANDLE_INSTANTIATION, INSTANTIATION, instantiation.getGlobalInstanceReference(),  "Handle Instantiation");

        alliance.getSecurity().verify(instantiation);

        //Create BPM Instance

        // as we come from network, we do not know the modelReference.
        // String modelReference = instantiation.getModelReference();

        logger.info("Advise BPMEngine to instantiate model:\t" + instantiation);

        String modelReference = alliance.getModel().getModelReference();
        BPMEngine bpmEngine = alliance.getEngine();
        Instance instance = bpmEngine.instantiate(modelReference, instantiation.getBusinessKey(), instantiation.getJSONVariables(), instantiation.getName());
        instance.setGlobalInstanceReference(instantiation.getGlobalInstanceReference());
        instance = instanceRepository.save(instance);
        alliance.getEngine().getInstances().add(instance);
        allianceRepository.save(alliance);

        //emitting an event for which the dpex application can register a listener
        InstantiationEvent event = new InstantiationEvent(instance, alliance.getGlobalAllianceReference(), TenantContext.getTenantId());
        eventBus.post(event);

        dpexLogger.log(alliance, INSTANCE_CREATED, INSTANTIATION, instantiation.getGlobalInstanceReference(),  "Instance created");

    }

    /**
     *
     * @param alliance the {@link Alliance} that captures all artefacts
     * @param instance the {@link Instance} the {@link TaskAction} belongs to
     * @param user the {@link User} that represents the initiator of the {@link TaskAction}
     * @param task the particular {@link Task} of this update request
     * @param stage the {@link TaskLifeCycleStage} that the {@link User} wants to set for the given {@link Task}
     */
    public void sendTaskAction(Alliance alliance, Instance instance, User user, Task task, ProcessVariables processVariables, TaskLifeCycleStage stage) {

        logger.info("Send Task Action.");
        dpexLogger.log(alliance, SEND_TASK_CLAIM, TASK_CLAIM, task.getActivity(),  "Send Task Claim");

        TaskAction taskAction = new TaskAction(instance, task, processVariables,stage);
        taskAction.setUser(user);

        // Only conform actions are sent to participants
        if(alliance.getEngine().isConform(alliance.getModel(), instance, taskAction)) {

            String signature = alliance.getSecurity().sign(taskAction);
            taskAction.setSender(new Signature(signature, user));

            logger.info("Advise Network to send Task Action:\t" + taskAction);
            logger.info("Send TaskAction with Context: " + TenantContext.getTenantId());

            alliance.getCollaboration().getNetwork().sendTaskAction(taskAction);

        }
        else {
            logger.warn("The requested action is not model conform. Processing canceled.");
        }

    }

    /**
     * <b>Currently not maintained (v0.0.8)</b>
     * This method is not called in case of blockchain-based SCI adapters such as {@link dpex.impl.collaboration.ethereum.raw.EthereumRaw}.
     * The blockchain network itself cares for finding agreement over the order for example.
     * Consequently, no acknowledgement must be sent manually here, but instead, {@link BaseNetwork#handleTaskAction(Object)} directly calls {@link DPEXService#handleAcknowledgement(Acknowledgement)}.
     *
     * @param taskAction the {@link TaskAction} that is handled
     */
    public void handleTaskAction(TaskAction taskAction) {

        Alliance alliance = allianceRepository.findByLocalInstanceReference(taskAction.getInstance().getLocalInstanceReference()).orElseThrow();

        logger.info("Handle Task Action:\t" + taskAction);
        dpexLogger.log(alliance, HANDLE_TASK_COMPLETE, TASK_COMPLETE, taskAction.getTask().getActivity(), "Handle Task Execution");

        alliance.getSecurity().verify("", taskAction.getSender().getSignature(), taskAction.getUser().getSciId());

        //if(this.alliance.getEngine().isConform(taskCompletion.getInstance(), taskCompletion)) {

        logger.info("Event (" + taskAction.getTask() + ", " + taskAction.getUser().getBpmId() + ") is model conform. Let's add it to the candidate log...");

        CandidateLog clog = alliance.getCollaboration().getCandidateLog();
        Set<Event> cEvents = clog.getEvents();
        cEvents.add(new Event(taskAction));
        clog.setEvents(cEvents);
        allianceRepository.save(alliance);

        logger.info("Send Acknowledgement...");
        Acknowledgement acknowledgement = new Acknowledgement();
        acknowledgement.setAction(taskAction);

        //Sign
        String signature = alliance.getSecurity().sign(acknowledgement);
        acknowledgement.setSender(new Signature(signature, null));

        logger.info("Advise Network to send ACK:\t" + acknowledgement);
        alliance.getCollaboration().getNetwork().sendAcknowledgement(acknowledgement);

        dpexLogger.log(alliance, SEND_ACK, TASK_COMPLETE, taskAction.getTask().getActivity(), "Send Acknowledgement");

    }

    /**
     * When a foreign node approves an {@link TaskAction}, it sends an {@link Acknowledgement} with respect to that action.
     * The {@link Acknowledgement} is received by {@link BaseNetwork#handleAcknowledgement(Object)}.
     * In case of using a blockchain-based {@link Collaboration}, no manual {@link Acknowledgement} is sent.
     * In contrast, the blockchain network itself provides a ordered list of all occurring events.
     * Hence, when a {@link TaskAction} is received in the {@link BaseNetwork#handleTaskAction(Object)} method,
     * it directly builds an {@link Acknowledgement} objects and forwards it to this method.
     * Here, the respective {@link Alliance} is retrieved at first.
     * Then, the validity of the {@link Signature} is checked.
     * For the usage of non-blockchain-based SCI networks, we add the {@link Acknowledgement} to the proper {@link Event} in the {@link CandidateLog}.
     * As stated, this is not necessary for blockchain-based SCIs as received {@link Acknowledgement}s are directly considered accepted globally.
     * The {@link ConsensusEngine} is consulted, if the {@link TaskAction}, referred to in the {@link Acknowledgement}, is considered accepted.
     * Again, for blockchain-based {@link Collaboration}s, the {@link ConsensusEngine} simply returns true, as consensus if found in the blockchain internally.
     * If agreement is reached, i.e. {@link ConsensusEngine#isAgreementReached(Acknowledgement, CandidateLog, String...)} has returned true,
     * the {@link BPMEngine} is updated according to the {@link TaskLifeCycleStage} included in the {@link TaskAction} of the {@link Acknowledgement}.
     *
     * @param acknowledgement the {@link Acknowledgement} that was received
     */
    public void handleAcknowledgement(Acknowledgement acknowledgement) {

        Alliance alliance = allianceRepository.findByGlobalInstanceReference(
                acknowledgement.getAction().getInstance().getGlobalInstanceReference()
        ).orElseThrow();

        logger.info("Handle ACK:\t" + acknowledgement);
        dpexLogger.log(alliance, HANDLE_ACK, TASK_COMPLETE, acknowledgement.getAction().getTask().getActivity(),  "Handle Acknowledgement");

        //validate
        alliance.getSecurity().verify(
                acknowledgement.toString(),
                acknowledgement.getSender().getSignature(),
                acknowledgement.getAction().getUser().getSciId()
        );

        TaskAction taskAction = acknowledgement.getAction();
        Task task = taskAction.getTask();

        //TODO: check if this is persisted
        alliance.getCollaboration().getCandidateLog().addAcknowledgementToEvent(task.getActivity());

        ConsensusEngine consensusEngine = alliance.getConsensusEngine();
        boolean agreementReached = consensusEngine.isAgreementReached(
                acknowledgement,
                alliance.getCollaboration().getCandidateLog(),
                "1");

        if(agreementReached) {
            logger.info("Agreement Reached:\t" + acknowledgement);

            switch (taskAction.getTaskLifeCycleStage()) {
                case CLAIM -> {
                    dpexLogger.log(alliance, AGREEMENT_REACHED, TASK_CLAIM, acknowledgement.getAction().getTask().getActivity(), "Agreement reached.");

                    if (alliance.getEngine().isConform(alliance.getModel(), taskAction.getInstance(), taskAction)) {
                        try {
                            logger.info("Advise Engine to claim Task:\t" + taskAction);

                            alliance.getEngine().claim(acknowledgement.getAction().getInstance(), taskAction);
                            alliance.getEngine().getInstances().stream().filter(i ->
                                    i.getGlobalInstanceReference().equals(taskAction.getInstance().getGlobalInstanceReference()
                                    )).findAny().orElseThrow().getEventLog().addEvent(new Event(taskAction));
                            //emitting an event for which the dpex application can register a listener
                            eventBus.post(new TaskActionEvent(taskAction, TenantContext.getTenantId()));
                        } catch (TaskNotFoundException tnfe) {
                            logger.error("The task " + taskAction.getTask().getActivity() + " was not found.");
                            tnfe.printStackTrace();
                            return;
                        }
                    } else {
                        throw new RuntimeException("NOT CONFORM.");
                    }

                    dpexLogger.log(alliance, ENGINE_UPDATED, TASK_CLAIM, acknowledgement.getAction().getTask().getActivity(), "Engine updated.");
                }
                case COMPLETE -> {
                    dpexLogger.log(alliance, AGREEMENT_REACHED, TASK_COMPLETE, acknowledgement.getAction().getTask().getActivity(), "Agreement reached.");

                    if (alliance.getEngine().isConform(alliance.getModel(), taskAction.getInstance(), taskAction)) {

                        try {
                            logger.info("Advise Engine to complete Task:\t" + taskAction);
                            alliance.getEngine().execute(acknowledgement.getAction().getInstance(), taskAction);
                            
                            //emitting an event for which the dpex application can register a listener
                            eventBus.post(new TaskActionEvent(taskAction, TenantContext.getTenantId()));
                        } catch (TaskNotFoundException tnfe) {
                            logger.error("TASK NOT FOUND: " + tnfe);
                            return;
                        }
                        logger.info("Machined updated: (" + taskAction.getTask() + ", " + taskAction.getUser().getBpmId() + ") was executed.");
                        alliance.getEngine().getInstances().stream().filter(i ->
                                i.getGlobalInstanceReference().equals(taskAction.getInstance().getGlobalInstanceReference()
                                )).findAny().orElseThrow().getEventLog().addEvent(new Event(taskAction));

                        dpexLogger.log(alliance, ENGINE_UPDATED, TASK_COMPLETE, acknowledgement.getAction().getTask().getActivity(), "Engine updated.");
                    } else {
                        throw new RuntimeException("NOT CONFORM. NOT CLAIMED?");
                    }
                }
            }

        }
        allianceRepository.save(alliance);

    }

    /**
     * Accepts an instantiation and saves it to the database by adding it to the appropriate network instance.
     * This method is used to register a new instantiation in a network after it has been confirmed by the network.
     *
     * @param globalallreference The global alliance reference to identify the specific alliance.
     * @param instantiation The identifier of the instantiation to be added to the network.
     */
    @Transactional
    public void acceptInstantiationAndSaveToDB(String globalallreference, String instantiation) {
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(globalallreference).orElseThrow();
        ((BftSmartNetwork) alliance.getCollaboration().getNetwork()).add_instantiation(instantiation);
    }

    /**
     * Accepts a task action and saves it to the database by recording the event as noticed in the network instance.
     * This method is crucial for maintaining a record of which task actions have been processed, avoiding duplicate processing.
     *
     * @param globalallreference The global alliance reference to identify the specific alliance.
     * @param id The identifier of the task action event.
     * @param current_eventlist The current list of events from which the task action is derived.
     * @param processIdToPoll The process instance identifier to which the event relates.
     */
    @Transactional
    public void acceptTaskActionAndSaveToDB(String globalallreference, int id, Map<Integer, PEvent> current_eventlist, String processIdToPoll) {
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(globalallreference).orElseThrow();
        ((BftSmartNetwork) alliance.getCollaboration().getNetwork()).addNoticedEvent(id, current_eventlist, processIdToPoll);
    }

    /**
     * Saves a newly instantiated process to the database and marks it as processed within the Ethereum network.
     * This method ensures that new instantiations are tracked from the moment they are recognized by the system.
     *
     * @param globalinstref The global instance reference of the new instantiation.
     * @param globalalreference The global alliance reference to identify the specific alliance.
     */
    @Transactional
    public void saveChangeInstantiationEthereum(String globalinstref, String globalalreference) {
        Alliance alliance = allianceRepository.findByGlobalAllianceReference(globalalreference).orElseThrow();
        ((EthereumRawNetwork) alliance.getCollaboration().getNetwork()).addInstantiation(globalinstref);
    }

    /**
     * Saves changes related to a specific task action event within the Ethereum network to the database.
     * This method adds the task action to the list of processed actions, ensuring that it is not processed again.
     *
     * @param globalinstref The global instance reference associated with the task action.
     * @param uniqueEventIdentifier A unique identifier for the task action event.
     */
    @Transactional
    public void saveChangeTaskActionEthereum(String globalinstref, String uniqueEventIdentifier) {
        Alliance alliance = allianceRepository.findByGlobalInstanceReference(globalinstref).orElseThrow();
        ((EthereumRawNetwork) alliance.getCollaboration().getNetwork()).addTaskAction(globalinstref, uniqueEventIdentifier);
    }

    public List<Task> getWorkList(Alliance alliance, Instance instance) {
        return alliance.getEngine().getWorkList(instance);
    }

    //Returns all claimed tasks of a user
    public List<Task> getWorkListByUser(Alliance alliance, Instance instance, User user) {
        return alliance.getEngine().getWorkListByUser(instance, user.getBpmId());
    }
    
    //Returns all claimed tasks of a user and all unclaimed tasks for which the user is eligible to claim now
    public List<Task> getIndividualWorklist(Alliance alliance, Instance instance, String bpmId) {
    	return alliance.getEngine().getIndividualWorkList(alliance.getModel(), instance, bpmId);
    }

}
