package dpex.core;

import dpex.action.TaskAction;
import dpex.bpm.execution.TaskLifeCycleStage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nullable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Nullable
    int timestamp;
    String activity;
    String resource;

    TaskLifeCycleStage taskLifeCycleStage;

    public Event(TaskAction taskAction) {
        this.activity = taskAction.getTask().getActivity();
        this.resource = taskAction.getUser().getBpmId();
        this.taskLifeCycleStage = taskAction.getTaskLifeCycleStage();
    }
}
