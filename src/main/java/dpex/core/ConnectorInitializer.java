package dpex.core;

import dpex.db.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * This is not implemented fully yet. When the application restarts, the event listeners
 * to Blockchain events for example are not registered and must be reconnected.
 *
 * This class should fetch all Ethereum-based collaborations and reregister the event listener.
 *
 * //TODO: To be implemented
 */
@Component
@RequiredArgsConstructor
public class ConnectorInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private final CollaborationFactoryRepository collaborationFactoryRepository;

    private final InitializerRepository initializerRepository;

    private final DPEXService dpexService;

    private final BPMEngineRepository beer;

    private final AllianceRepository allianceRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
/*
        Set<Alliance> ethereumNetworkAlliances = allianceRepository.findByCollaborationType("EthereumCollaboration");
        EthereumNetwork en = new EthereumNetwork();

        ethereumNetworkAlliances.forEach(ena -> {
            ena.getEngine().getInstances().forEach(i -> {
                //TODO: connection
                en.initialize(dpexService, "connection", i.getGlobalInstanceReference());
            });
        });
*/
    }
}
