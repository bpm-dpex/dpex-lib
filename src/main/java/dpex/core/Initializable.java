package dpex.core;

public interface Initializable {
    public void initialize(DPEXService service, String arg);
}
