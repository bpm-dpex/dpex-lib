package dpex.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nullable;
import javax.persistence.*;

/**
 * This class describes users (human resources) of a process model in a collaboration.
 * //TODO: Future Work: Extend with more attributes or zero-knowledge proofs etc.
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String bpmId;
    private String sciId;
    private String communicationId;
    //Nullable if a communication module does not need this information
    private String homeServerUrl;

    // Nullable for foreign users
    //private String privateKey;

}
