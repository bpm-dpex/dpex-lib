package dpex.core;

public interface Factory<T> {

    public String getName();

    public T create();
}
