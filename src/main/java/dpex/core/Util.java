package dpex.core;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Util {

    public static String getTextFromInputStream(InputStream is) throws IOException {
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName(StandardCharsets.UTF_8.name())
                ))) {

            int c;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }

        return textBuilder.toString();
    }

    public static String[] emptyStringArray() {
        return new String[]{};
    }

    public static Object[] emptyObjectArray() {
        return new Object[]{};
    }

    public static Class<?>[] emptyClassArray() {
        return new Class<?>[]{};
    }

    public static Object emptyObject() {
        return new Object();
    }
}
