package dpex.core;

import dpex.collaboration.security.Signature;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
public abstract class Message {

    private String allianceReference;

    private Signature sender;

}
