package dpex.core;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Initializer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //Mappt Types zu argumenten
    // ruft für alle types INIT mit allen argumenten auf
    //
    //private Object type;

    //@ElementCollection
    //private Set<String> arguments;


}
