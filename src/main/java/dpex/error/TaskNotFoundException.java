package dpex.error;

public class TaskNotFoundException extends Exception {

    public TaskNotFoundException(String task) {
        super("The Engine cannot found the given task: " + task);
    }

}

