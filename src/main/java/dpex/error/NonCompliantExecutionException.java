package dpex.error;

import dpex.action.Action;
import dpex.bpm.execution.BPMEngine;

public class NonCompliantExecutionException extends RuntimeException {

    public NonCompliantExecutionException(Action a, BPMEngine machineInstance) {
        super("The given Action is not process conform but was still tried to be executed.");
    }

}
