package dpex.error;

public class UnknownProcessModelException extends RuntimeException {

    public UnknownProcessModelException(String modelReference) {
        super("The received Instantiation has got an unknown model reference: " + modelReference);
    }

}
