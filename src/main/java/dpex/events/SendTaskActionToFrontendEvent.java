package dpex.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents a task action event that should be signed with an ethereum private key in the frontend.
 */
@Data
@AllArgsConstructor
public class SendTaskActionToFrontendEvent {
    private String sciAddress;
    private String sciAuthHeader;
    private String instanceReference;
    private String activity;
    private String taskLifeCycleStage;
    private String processVariablesJSON;
    private String sciID;
    private String tenantID;
}
