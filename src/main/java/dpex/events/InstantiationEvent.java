package dpex.events;

import dpex.bpm.execution.Instance;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents process instance instantiation event used to update the frontend.
 * Holds the instance, its global alliance reference (GAR) and the tenant ID.
 */
@Data
@AllArgsConstructor
public class InstantiationEvent {
	Instance instance;
	String gar;
	String tenantID;
}
