package dpex.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents an instantiation event that should be signed with an ethereum private key in the frontend.
 */
@Data
@AllArgsConstructor
public class SendInstantiationToFrontendEvent {
    private String sciAddress;
    private String sciAuthHeader;
    private String globalAllianceReference;
    private String JSONVariables;
    private String name;
    private String sciID;
    private String tenantID;
}
