package dpex.events;

import dpex.action.TaskAction;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents task action event used to update the frontend.
 * Holds the task action and the tenant ID.
 */
@Data
@AllArgsConstructor
public class TaskActionEvent {
    TaskAction taskaction;
    String tenantID;
}
