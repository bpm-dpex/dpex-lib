package dpex.bpm.model;

import dpex.core.Factory;
import dpex.core.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@Data
public abstract class ProcessModelFactory implements Factory<ProcessModel> {

    @Id
    private String name;

    @Column(insertable = false, updatable = false)
    private String type;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> participants;

    public abstract ProcessModel create();
}
