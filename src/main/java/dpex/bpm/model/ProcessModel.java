package dpex.bpm.model;

import dpex.core.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Set;

/**
 * This is a core class that must be extended for any model class that is used with the DPEX concept.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@NoArgsConstructor
@Data
public abstract class ProcessModel {

    @Id
    private String modelReference;

    @Column(insertable = false, updatable = false)
    private String type;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> participants;

}
