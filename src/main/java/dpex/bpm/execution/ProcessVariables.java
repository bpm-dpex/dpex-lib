package dpex.bpm.execution;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Class to store the process Variables to be updated in the next complete Task
 * Variable Map contains the names and the values of the Variables
 *
 * @author Tobias Gretsch
 * @version 0.0.11
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProcessVariables {

    private static final Logger logger = LogManager.getLogger(ProcessVariables.class);
    private HashMap<String, Object> variables = new HashMap<>();

    public ProcessVariables(String jsonVariables) {
        ObjectMapper objectMapper = new ObjectMapper();
        if (!jsonVariables.isEmpty()) {
            try {
                JsonNode variables = objectMapper.readTree(jsonVariables);
                Iterator<Map.Entry<String, JsonNode>> fields = variables.fields();
                fields.forEachRemaining(field -> this.addVariable(field.getKey(), convertType(field.getValue())));
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void addVariable(String varName, Object value) {
        variables.put(varName, value);
    }

    public void addFromJSON(String JSONString) {

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            JsonNode node = objectMapper.readTree(JSONString);
            Iterator<Map.Entry<String, JsonNode>> fields = node.fields();
            while (fields.hasNext()) {
                Map.Entry<String, JsonNode> entry = fields.next();
                String variableName = entry.getKey();
                JsonNode valueNode = entry.getValue().get("value");
                Object value = objectMapper.convertValue(valueNode, Object.class);

                addVariable(variableName, value);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Variable response could not be handled:" + e);
        }
    }

    public void clearMap() {
        variables.clear();
    }

    public String generateJSONString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(variables);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public String generateJSONCamundaString(){
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode variablesNode = JsonNodeFactory
                                   .instance.objectNode();
        for(Map.Entry<String,Object> entry: variables.entrySet()) {
            String varName = entry.getKey();
            Object varValue = entry.getValue();
            String varType = varValue.getClass().getSimpleName();

            ObjectNode varNode = JsonNodeFactory
                                 .instance.objectNode();
            varNode.putPOJO("value", varValue);
            varNode.put("type", varType);

            variablesNode.set(varName, varNode);
        }

        ObjectNode jsonObject = JsonNodeFactory
                                .instance.objectNode();
        jsonObject.set("variables", variablesNode);

        try {
           return  objectMapper.writeValueAsString(jsonObject);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private Object convertType(JsonNode node) throws RuntimeException {
        Object value;

        if(node.isBoolean()) {
            value = node.booleanValue();
        } else if (node.isLong()) {
            value = node.longValue();
        } else if (node.isInt()) {
            value = node.intValue();
        } else if (node.isTextual()) {
            value = node.textValue();
        } else if (node.isDouble()) {
            value = node.doubleValue();
        } else {
            value = node.textValue();
        }
        return value;
    }

}
