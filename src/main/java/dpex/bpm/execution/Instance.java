package dpex.bpm.execution;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A reference value to identify the process model instance in the collaboration.
 * Could be a contract address in case of ethereum.
 */
@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Instance  {

	@Id
    private String localInstanceReference;
    private String globalInstanceReference;
    private String name;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    EventLog eventLog = new EventLog();

    public Instance(String localInstanceReference, String globalInstanceReference, String name) {
        this.localInstanceReference = localInstanceReference;
        this.globalInstanceReference = globalInstanceReference;
        this.name = name;
    }
}
