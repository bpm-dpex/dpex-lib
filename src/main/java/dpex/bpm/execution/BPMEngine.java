package dpex.bpm.execution;

import dpex.action.Action;
import dpex.action.TaskAction;
import dpex.bpm.model.ProcessModel;
import dpex.error.TaskNotFoundException;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The BPMEngine represents the execution layer within the process tier of the dpex framework.
 * Thereby, it can be extended by any adapter implementation for specific external workflow management systems such as Camunda.
 * If no external system is used, the respective adapter can also be a manual implementation of the provided methods.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@Data
public abstract class BPMEngine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL,  fetch = FetchType.EAGER)
    Set<Instance> instances = new HashSet<>();

    public Long getId() {
        return id;
    }

    public abstract Instance instantiate(String model, String businessKey, String JSONObject, String name);

    public abstract void claim(Instance instance, TaskAction taskClaim) throws TaskNotFoundException;

    public abstract void execute(Instance instance, Action action) throws TaskNotFoundException;

    public abstract boolean isConform(ProcessModel processModel, Instance instance, TaskAction taskAction);

    public abstract List<Task> getWorkList(Instance instanceReference);

    //Returns the claimed tasks of a given user
    public abstract List<Task> getWorkListByUser(Instance instanceReference, String userReference);
    
    //Like getWorklistByUser but returns additionally the tasks for which the given user is eligible to claim according to organizational constraints
    public abstract List<Task> getIndividualWorkList(ProcessModel processModel, Instance instanceReference, String userReference);

    /**
     *
     * @param localInstanceReference
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    public abstract ProcessVariables getInstanceVariables(String localInstanceReference);

    /**
     *
     * @param localInstanceReference
     * @param varName
     * @return
     *
     * @author Tobias Gretsch
     * @version v0.0.11
     */
    public abstract String deleteInstanceVariable(String localInstanceReference, String varName);

   }
