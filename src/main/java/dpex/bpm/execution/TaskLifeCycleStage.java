package dpex.bpm.execution;

public enum TaskLifeCycleStage {
    CLAIM,
    COMPLETE
}
