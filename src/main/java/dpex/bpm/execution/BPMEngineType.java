package dpex.bpm.execution;

public enum BPMEngineType {
	CAMUNDA_BPM_ENGINE("Camunda BPM Engine"),
	MALICIOUS_ENGINE("Malicious Engine");
	
	public final String label;
	
	private BPMEngineType(String label) {
		this.label = label;
	}
}
