package dpex.bpm.execution;

import dpex.bpm.model.ProcessModel;
import dpex.core.Factory;
import lombok.Data;

import javax.persistence.*;

/**
 * The BPMEngineFactory can be instanced to BPMEngines, which are directly assigned to an alliance.
 * The factory enables a strict decoupling of alliances with their models and instances respectively.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@Data
public abstract class BPMEngineFactory {

    @Id
    private String name;

    @Column(insertable = false, updatable = false)
    private String type;

    public abstract BPMEngine create(ProcessModel processModel);

}
