package dpex.bpm.execution;

import dpex.core.Log;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * The EventLog records historical footprints during process execution.
 * It captures an ordered list of events that is accepted in the whole collaboration.
 * Hence, events are only recorded, if accepted in the network.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class EventLog extends Log {


}


