package dpex.bpm.execution;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is a container class that represents a specific work item.
 * It captures the name of the activity and the resource that is/was assigned to this work item.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    private String activity;
    private String resource;
}
