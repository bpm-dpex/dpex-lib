package dpex.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * The {@code EncryptionUtil} class provides encryption and decryption functionalities using the AES/CBC/PKCS5Padding cipher.
 * It uses a secret key and an initialization vector (IV) to encrypt and decrypt strings.
 *
 * used e.g. for encrypting the client secrets for the tenants before saving them in the MySQL database.
 */
public class EncryptionUtil {

    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final Charset CHARSET_NAME = StandardCharsets.UTF_8;
    private static final String KEY_SPEC_ALGORITHM = "AES";

    private final SecretKeySpec keySpec;
    private final IvParameterSpec ivParameterSpec;

    /**
     * Constructs an {@code EncryptionUtil} with the specified encryption key.
     * Initializes the key specification and IV parameter spec.
     *
     * @param encryptionKey the secret key used for encryption and decryption.
     *                      The key data should be sufficient to use with the AES algorithm.
     */
    public EncryptionUtil(String encryptionKey) {
        this.keySpec = new SecretKeySpec(encryptionKey.getBytes(CHARSET_NAME), KEY_SPEC_ALGORITHM);
        this.ivParameterSpec = new IvParameterSpec(new byte[16]);
    }

    /**
     * Encrypts the specified plaintext using AES/CBC/PKCS5Padding cipher.
     *
     * @param input the plaintext string to be encrypted.
     * @return the encrypted string in Base64 encoding.
     * @throws Exception if an error occurs during the encryption process.
     */
    public String encrypt(String input) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(input.getBytes());
        return Base64.getEncoder().encodeToString(encrypted);
    }

    /**
     * Decrypts the specified encrypted text using AES/CBC/PKCS5Padding cipher.
     *
     * @param input the encrypted string in Base64 encoding to be decrypted.
     * @return the decrypted plaintext string.
     * @throws Exception if an error occurs during the decryption process.
     */
    public String decrypt(String input) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivParameterSpec);
        byte[] original = cipher.doFinal(Base64.getDecoder().decode(input));
        return new String(original);
    }
}
