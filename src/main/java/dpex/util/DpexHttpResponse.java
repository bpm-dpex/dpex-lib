package dpex.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DpexHttpResponse {
    private int code;
    private String body;
}
