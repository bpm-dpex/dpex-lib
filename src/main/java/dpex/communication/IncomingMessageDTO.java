package dpex.communication;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncomingMessageDTO{
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = true)
	//communication ID
	private String sender;
	
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private long timestamp;
	
	@JsonProperty(required = true)
	private String communicationModule;
	
	@JsonProperty(required = false)
	private String fileUrl;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}