package dpex.communication;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncomingMessageEvent {
	private String accessToken;
	
	private IncomingMessageDTO incomingMessageDTO;
}
