package dpex.communication;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class GetRoomMessageDTO {
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = true)
	private String sender;
	
	@JsonProperty(required = true)
	private long timestamp;
	
	@JsonProperty(required = false)
	private String fileUrl;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}
