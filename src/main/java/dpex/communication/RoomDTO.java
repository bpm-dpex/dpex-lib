package dpex.communication;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoomDTO extends ChildDTO{
	//only specified for spaces
	@JsonProperty(required = false)
	private String gar;
	
	//only specified for spaces
	@JsonProperty(required = false)
	private ChildDTO[] childRooms;
}

@Data
class ChildDTO {
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private String name;
}