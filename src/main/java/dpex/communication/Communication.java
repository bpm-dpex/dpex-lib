package dpex.communication;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@Data
@NoArgsConstructor
public abstract class Communication {

    private static final Logger logger = LogManager.getLogger(Communication.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String name;

    /**
     * Logs in the user in the communication module
     * @param userId the communication ID of the user
     * @param password the password for the communication service
     * @param homeserverUrl the homeserver url if the communication protocol supports them, may be null otherwise
     * @return the access token to authenticate for further requests
     */
    public abstract String login(String userId, String password, String homeserverUrl);
    
    /**
     * Logs out the user from the communication module
     * @param accessToken the current access token of the user
     */
    public abstract void logout(String accessToken);
    
    /**
     * Sends a direct message
     * @param accessToken the current access token of the user
     * @param recipient the communication ID of the recipient
     * @param message the message to be sent
     * @param file the file to be attached
     * @param fileIsImage whether the file should be displayed as an image
     * @return the room ID in which the message has been sent
     */
    public abstract String sendDirectMessage(String accessToken, String recipient, String message, String file, boolean fileIsImage);
    
    /**
     * Sends a message in a room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room in which the message should be sent
     * @param message the message to be sent
     * @param file the file to be attached
     * @param fileIsImage whether the file should be displayed as an image
     */
    public abstract void sendRoomMessage(String accessToken, String roomId, String message, String file, boolean fileIsImage);
    
    /**
     * Creates a new space (or equivalent depending on the used protocol)
     * A space is a general construct that groups multiple rooms
     * @param accessToken the current access token of the user
     * @param name the name of the new space
     * @param invite the users that shall be invited to the space identified by their communication IDs
     * @param gar the global alliance reference in order to map the space to an alliance
     * @return the ID of the newly created space
     */
    public abstract String createSpace(String accessToken, String name, String[] invite, String gar);
    
    /**
     * Creates a new room (or equivalent depending on the used protocol)
     * A room is a general construct in which multiple users participate in to exchange messages among them
     * @param accessToken the current access token of the user
     * @param spaceId the ID of the space in which the room is supposed to be created
     * @param name the name of the new room
     * @param invite the users that shall be invited to the room identified by their communication IDs
     * @return the ID of the newly created room
     */
    public abstract String createRoom(String accessToken, String spaceId, String name, String[] invite);
    
    /**
     * Leaves a room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room that shall be left
     */
    public abstract void leaveRoom(String accessToken, String roomId);
    
    /**
     * Invite new members to a room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room to which the new members shall be invited
     * @param invite the new members that will be invited identified by their communication IDs
     */
    public abstract void inviteToRoom(String accessToken, String roomId, String[] invite);
    
    /**
     * Returns a list of rooms in which the user is a member of
     * @param accessToken the current access token of the user
     * @return the list of rooms in which the user is a member of
     */
    public abstract RoomDTO[] getRooms(String accessToken);
    
    /**
     * Retrieves all messages that have been sent into a given room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room for which the messages should be retrieved
     * @return an array of objects, each containing the message and its sender
     */
    public abstract GetRoomMessageDTO[] getRoomMessages(String accessToken, String roomId);
    
    /**
     * Retrieves all members (identified by communication IDs) of a given room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room for which the members should be retrieved
     * @return an array of communication IDs
     */
    public abstract String[] getRoomMembers(String accessToken, String roomId);
}
