package dpex.communication;

public enum CommunicationModuleType {
	MATRIX("Matrix"),
	DISCORD("Discord");
	
	public final String label;
	
	private CommunicationModuleType(String label) {
		this.label = label;
	}
}
