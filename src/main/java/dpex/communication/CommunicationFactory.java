package dpex.communication;

import lombok.Data;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
@Data
public abstract class CommunicationFactory {

    @Id
    private String name;

    @Column(insertable = false, updatable = false)
    private String type;

    public abstract Communication create();
}
