package dpex.communication;

import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

import dpex.core.DPEXService;
import dpex.db.CommunicationFactoryRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequestMapping("/incoming")
@RequiredArgsConstructor
public class IncomingMessageAPI {
	private final CommunicationFactoryRepository communicationFactoryRepository;
    private final DPEXCommunicationService dpexCommunicationService;

	
	@RequestMapping(value = "/receiveMessage", method = RequestMethod.POST)
    public ResponseEntity<?> receiveMessage(@RequestBody ReceiveMessageDTO receiveMessageDTO){
		Optional<CommunicationFactory> factory = communicationFactoryRepository.findById(receiveMessageDTO.getCommunicationModule());
		if(factory.isEmpty())
			return ResponseEntity.badRequest().body("Unknown communication module");
    	
    	String senderBpmId = dpexCommunicationService.getBpmIdFromCommunicationId(receiveMessageDTO.getSender());
    	
    	IncomingMessageDTO incomingMessageDTO = new IncomingMessageDTO(
    		receiveMessageDTO.getMessage(),
    		senderBpmId,
    		receiveMessageDTO.getRoomId(),
    		receiveMessageDTO.getTimestamp(),
    		receiveMessageDTO.getCommunicationModule(),
    		receiveMessageDTO.getFileUrl(),
    		receiveMessageDTO.isFileIsImage()
    	);
    	
    	DPEXService.eventBus.post(new IncomingMessageEvent(receiveMessageDTO.getRecipientAccessToken(), incomingMessageDTO));
    	
    	return ResponseEntity.ok(null);
    }
}

@Data
class ReceiveMessageDTO{
	@JsonProperty(required = true)
	private String message;
	
	@JsonProperty(required = true)
	//communication ID
	private String sender;
	
	@JsonProperty(required = true)
	private String recipientAccessToken;
	
	@JsonProperty(required = true)
	private String roomId;
	
	@JsonProperty(required = true)
	private long timestamp;
	
	@JsonProperty(required = true)
	private String communicationModule;
	
	@JsonProperty(required = false)
	private String fileUrl;
	
	@JsonProperty(required = false)
	private boolean fileIsImage;
}