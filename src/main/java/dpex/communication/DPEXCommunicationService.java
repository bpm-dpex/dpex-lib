package dpex.communication;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import dpex.bpm.model.ProcessModel;
import dpex.core.Alliance;
import dpex.core.User;
import dpex.db.AllianceRepository;
import dpex.db.CommunicationFactoryRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DPEXCommunicationService {

    private static final Logger logger = LogManager.getLogger(DPEXCommunicationService.class);
    
    private final AllianceRepository allianceRepository;
    private final CommunicationFactoryRepository communicationFactoryRepository;
    
    /**
     * Logs in the user in the communication module
     * @param userId the communication ID of the user
     * @param password the password for the communication service
     * @param homeserverUrl the homeserver url if the communication protocol supports them, may be null otherwise
     * @param communicationModule the name of the used communication module
     * @return the access token to authenticate for further requests
     */
    public String login(String userId, String password, String homeserverUrl, String communicationModule) {
        Communication communication = communicationFactoryRepository.findById(communicationModule).orElseThrow().create();
        return communication.login(userId, password, homeserverUrl);
    }
    
    /**
     * Logs out the user from the communication module
     * @param accessToken the current access token of the user
     * @param communicationModule the name of the used communication module
     */
    public void logout(String accessToken, String communicationModule) {
        Communication communication = communicationFactoryRepository.findById(communicationModule).orElseThrow().create();
        communication.logout(accessToken);
    }
    
    /**
     * Sends a direct message
     * @param accessToken the current access token of the user
     * @param recipient the bpm ID of the recipient
     * @param message the message to be sent
     * @param gar the global alliance reference
     */
    public String sendDirectMessage(String accessToken, String recipient, String message, String gar, String file, boolean fileIsImage) {
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
        Communication communication = alliance.getCommunication();
        String communicationId = getCommunicationIdFromBpmId(alliance.getModel(), recipient);
        return communication.sendDirectMessage(accessToken, communicationId, message, file, fileIsImage);
    }
    
    /**
     * Sends a message in a room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room in which the message should be sent
     * @param message the message to be sent
     * @param communicationModule the name of the used communication module
     */
    public void sendRoomMessage(String accessToken, String roomId, String message, String communicationModule, String file, boolean fileIsImage) {
        Communication communication = communicationFactoryRepository.findById(communicationModule).orElseThrow().create();
        communication.sendRoomMessage(accessToken, roomId, message, file, fileIsImage);
    }
    
    /**
     * Creates a new space (or equivalent depending on the used protocol)
     * A space is a general construct that groups multiple rooms
     * The space will automatically be named as the alliance that is referenced by the gar
     * All participants of the gar will automatically be invited to the new space
     * @param accessToken the current access token of the user
     * @param gar the global alliance reference
     * @return the ID of the newly created space
     */
    public String createSpace(String accessToken, String gar) {
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
        Communication communication = alliance.getCommunication();
        String[] communicationIds = getAllCommunicationIds(alliance.getModel());
        return communication.createSpace(accessToken, alliance.getName(), communicationIds, gar);
    }
    
    /**
     * Creates a new room (or equivalent depending on the used protocol)
     * A room is a general construct in which multiple users participate in to exchange messages among them
     * @param accessToken the current access token of the user
     * @param spaceId the ID of the space in which the room is supposed to be created
     * @param name the name of the new room
     * @param invite the users that shall be invited to the room identified by their bpm IDs
     * @param gar the global alliance reference
     * @return the ID of the newly created room
     */
    public String createRoom(String accessToken, String spaceId, String name, String[] invite, String gar) {
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
        Communication communication = alliance.getCommunication();
        String[] communicationIds = getCommunicationIdsFromBpmIds(alliance.getModel(), invite);
        return communication.createRoom(accessToken, spaceId, name, communicationIds);
    }
    
    /**
     * Leaves a room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room that shall be left
     * @param communicationModule the name of the used communication module
     */
    public void leaveRoom(String accessToken, String roomId, String communicationModule) {
        Communication communication = communicationFactoryRepository.findById(communicationModule).orElseThrow().create();
        communication.leaveRoom(accessToken, roomId);
    }
    
    /**
     * Invite new members to a room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room to which the new members shall be invited
     * @param invite the new members that will be invited identified by their bpm IDs
     * @param gar the global alliance reference
     */
    public void inviteToRoom(String accessToken, String roomId, String[] invite, String gar) {
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
        Communication communication = alliance.getCommunication();
        String[] communicationIds = getCommunicationIdsFromBpmIds(alliance.getModel(), invite);
        communication.inviteToRoom(accessToken, roomId, communicationIds);
    }
    
    /**
     * Returns a list of rooms in which the user is a member of
     * @param accessToken the current access token of the user
     * @param communicationModule the name of the used communication module
     * @return the list of rooms in which the user is a member of
     */
    public RoomDTO[] getRooms(String accessToken, String communicationModule) {
        Communication communication = communicationFactoryRepository.findById(communicationModule).orElseThrow().create();
        return communication.getRooms(accessToken);
    }
    
    /**
     * Retrieves all messages that have been sent into a given room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room for which the messages should be retrieved
     * @param communicationModule the name of the used communication module
     * @return an array of objects, each containing the message and its sender
     */
    public GetRoomMessageDTO[] getRoomMessages(String accessToken, String roomId, String communicationModule) {
        Communication communication = communicationFactoryRepository.findById(communicationModule).orElseThrow().create();
        GetRoomMessageDTO[] messages = communication.getRoomMessages(accessToken, roomId);
        for(GetRoomMessageDTO message : messages)
        	message.setSender(getBpmIdFromCommunicationId(message.getSender()));
        return messages;
    }
    
    /**
     * Retrieves all members (identified by communication IDs) of a given room
     * @param accessToken the current access token of the user
     * @param roomId the ID of the room for which the members should be retrieved
     * @param gar the global alliance reference
     * @return an array of communication IDs
     */
    public String[] getRoomMembers(String accessToken, String roomId, String gar) {
    	Alliance alliance = allianceRepository.findByGlobalAllianceReference(gar).orElseThrow();
    	ProcessModel model = alliance.getModel();
        Communication communication = alliance.getCommunication();
        String[] communicationIds = communication.getRoomMembers(accessToken, roomId);
        String[] bpmIds = new String[communicationIds.length];
        for(int i = 0; i < communicationIds.length; i++)
        	bpmIds[i] = getBpmIdFromCommunicationId(communicationIds[i], model);
        return bpmIds;
    }
        
    /**
     * Retrieves the corresponding bpm ID from a given communication ID by searching in all available alliances
     * Important: This method assumes that the bpm ID of a user is consistent throughout all alliances
     * @param communicationId the communication ID that is supposed to be converted
     * @return the corresponding bpm ID or "*Unknown User*" if none has been found
     */
    public String getBpmIdFromCommunicationId(String communicationId) {
    	List<Alliance> allAlliances = allianceRepository.findAll();
    	for(Alliance alliance : allAlliances) {
    		String bpmId = getBpmIdFromCommunicationId(communicationId, alliance.getModel());
    		if(bpmId == null)
        		//user could not be found in this alliance; try the next one
    			continue;
    		return bpmId;
    	}
    	return "*Unknown User*";
    }
    
    /**
     * Retrieves the corresponding bpm ID from a given communication ID by searching in the given process model
     * @param communicationId the communication ID that is supposed to be converted
     * @param processModel the process model in which the bpm ID will be searched
     * @return the corresponding bpm ID or null if none has been found
     */
    public String getBpmIdFromCommunicationId(String communicationId, ProcessModel processModel) {
		Set<User> allUsersOfAlliance = processModel.getParticipants();
    	Optional<User> optionalUser = allUsersOfAlliance.stream().filter(user_ -> user_.getCommunicationId().equals(communicationId)).findFirst();
    	if(optionalUser.isEmpty())
    		return null;
    	String bpmId = optionalUser.get().getBpmId();
    	return bpmId;
    }

    /**
     * Retrieves the corresponding communication IDs from given bpm IDs
     * @param processModel the process model
     * @param bpmIds the bpm IDs that are supposed to be converted
     * @return the corresponding communication IDs
     */
    public String[] getCommunicationIdsFromBpmIds(ProcessModel processModel, String[] bpmIds) {
        Set<User> allUsersOfAlliance = processModel.getParticipants();
        return Arrays.stream(bpmIds).map(bpmId -> {
            User user = allUsersOfAlliance.stream().filter(user_ -> user_.getBpmId().equals(bpmId)).findFirst().orElseThrow();
            String communicationId = user.getCommunicationId();
            return communicationId;
        }).toArray(String[]::new);
    }
    
    /**
     * Retrieves the corresponding communication ID from a given bpm ID
     * @param processModel the process model
     * @param bpmId the bpm ID that is supposed to be converted
     * @return the corresponding communication ID
     */
    public String getCommunicationIdFromBpmId(ProcessModel processModel, String bpmId) {
    	Set<User> allUsersOfAlliance = processModel.getParticipants();
    	User user = allUsersOfAlliance.stream().filter(user_ -> user_.getBpmId().equals(bpmId)).findFirst().orElseThrow();
        String communicationId = user.getCommunicationId();
        return communicationId;
    }
    
    /**
     * Retrieves all communication IDs from a given process model
     * @param processModel the process model
     * @return all communication IDs defined in the process model
     */
    public String[] getAllCommunicationIds(ProcessModel processModel) {
    	Set<User> allUsersOfAlliance = processModel.getParticipants();
    	String[] communicationIds = new String[allUsersOfAlliance.size()];
    	int i = 0;
    	for(User user : allUsersOfAlliance)
    		communicationIds[i++] = user.getCommunicationId();
    	return communicationIds;
    }
}