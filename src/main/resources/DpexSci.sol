// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract DpexSci {

    event EventEmitter(DpexProcessInstance processInstanceContract, string variables, string name);

    DpexProcessInstance[] public instances;

    function instantiate(string memory variables, string memory name) public returns (address) {
        DpexProcessInstance dpexProcessInstance = new DpexProcessInstance();
        instances.push(dpexProcessInstance);
        emit EventEmitter(dpexProcessInstance, variables, name);
        return address(dpexProcessInstance);
    }

    function get() public view returns (uint) {
        return instances.length;
    }

    function getAddress() public view returns (address) {
        return address(instances[0]);
    }

}

contract DpexProcessInstance {

    struct Event {
        string activity;
		string stage;
        address resource;
        string variables;
    }

    string globalInstanceRef;

    address instantiator;

    event EventEmitter(Event logEntry);

    Event[] log;


    function store(string memory activity, string memory stage, string memory variables) public {
        Event memory logEntry = Event(activity, stage, msg.sender, variables);
        log.push(logEntry);
        emit EventEmitter(logEntry);
    }

    function getGlobalInstanceRef() public view returns (string memory) {
        return globalInstanceRef;
    }

    function getLog() public view returns (Event[] memory) {
        return log;
    }

}