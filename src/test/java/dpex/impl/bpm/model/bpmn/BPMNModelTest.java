package dpex.impl.bpm.model.bpmn;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
public class BPMNModelTest {

    @Test
    public void fromStringTest() throws URISyntaxException, IOException {



        Path path = Paths.get(getClass().getClassLoader()
                .getResource("unit-test-org.bpmn").toURI());

        Stream<String> lines = Files.lines(path);
        String data = lines.collect(Collectors.joining("\n"));
        lines.close();

        BPMNModel deserializedBpmn = BPMNModel.fromString(data);

        assertThat(deserializedBpmn.getStart()).isNotNull();
        assertThat(deserializedBpmn.getEnd()).isNotNull();
        assertThat(deserializedBpmn.getActivities()).isNotNull();
        assertThat(deserializedBpmn.getGateways()).isNotNull();
        assertThat(deserializedBpmn.getSequenceFlows()).isNotNull();

        assertThat(deserializedBpmn.getActivities().size()).isEqualTo(4);
        assertThat(deserializedBpmn.getGateways().size()).isEqualTo(2);
        assertThat(deserializedBpmn.getSequenceFlows().size()).isEqualTo(8);

        assertThat(deserializedBpmn.getStart().getOutgoing().targetRef).isEqualTo(new Activity("A"));
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("A")).findFirst().orElseThrow().getAnnotation()).isEqualTo("christian");
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("B")).findFirst().orElseThrow().getAnnotation()).isEqualTo("christian");
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("C")).findFirst().orElseThrow().getAnnotation()).isEqualTo("stefan");
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("D")).findFirst().orElseThrow().getAnnotation()).isEqualTo("kerstin");

    }

    @Test
    public void businessTripFromStringTest() throws URISyntaxException, IOException {



        Path path = Paths.get(getClass().getClassLoader()
                .getResource("business-trip.bpmn").toURI());

        Stream<String> lines = Files.lines(path);
        String data = lines.collect(Collectors.joining("\n"));
        lines.close();

        BPMNModel deserializedBpmn = BPMNModel.fromString(data);

        assertThat(deserializedBpmn.getStart()).isNotNull();
        assertThat(deserializedBpmn.getEnd()).isNotNull();
        assertThat(deserializedBpmn.getActivities()).isNotNull();
        assertThat(deserializedBpmn.getGateways()).isNotNull();
        assertThat(deserializedBpmn.getSequenceFlows()).isNotNull();

        assertThat(deserializedBpmn.getActivities().size()).isEqualTo(7);
        assertThat(deserializedBpmn.getGateways().size()).isEqualTo(2);
        assertThat(deserializedBpmn.getSequenceFlows().size()).isEqualTo(11);

        assertThat(deserializedBpmn.getStart().getOutgoing().targetRef).isEqualTo(new Activity("Apply for trip"));
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("Apply for trip")).findFirst().orElseThrow().getAnnotation()).isNullOrEmpty();
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("Prepare Application")).findFirst().orElseThrow().getAnnotation()).isEqualTo("Role: Secretary\n$1.chair = this.chair");
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("Book Transport")).findFirst().orElseThrow().getAnnotation()).isEqualTo("=$1");
        assertThat(deserializedBpmn.getActivities().stream().filter(activity -> activity.getName().equals("Book Accommodation")).findFirst().orElseThrow().getAnnotation()).isEqualTo("=$1");

    }


}
