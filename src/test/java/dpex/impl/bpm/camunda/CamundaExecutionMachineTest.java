package dpex.impl.bpm.camunda;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dpex.action.TaskAction;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.Task;
import dpex.util.DpexHttpResponse;
import dpex.impl.bpm.camunda.dto.GetWorkListDTO;
import dpex.impl.bpm.camunda.dto.GetWorkListResponse;
import fixture.*;
import lombok.Builder;
import lombok.Data;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@Builder
@Data
class CamundaInstantiationResponse {

    /**
     * "method":	"GET",
     * "href":		"http://localhost:8080/engine-rest/process-instance/d86c5058-4c5d-11ed-855d-005056c00001",
     * "rel":		"self"
     */
    Object links;
    String id;
    String definitionId;
    String businessKey;
    String caseInstanceId;
    boolean ended;
    boolean suspended;
    String tenantId;
}

/**
 * Test Plan
 * (1) Model a process in Camunda Modeler
 *      (1a) USE DPEX to deploy process in Camunda so that the globalModelRef is set correctly
 *
 */
@ExtendWith(MockitoExtension.class)
public class CamundaExecutionMachineTest {

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @Disabled
    public void instantiationTest() throws URISyntaxException, IOException {

        // Setup
        CamundaModel model = CamundaModelFixture.unitTestModel();

        CamundaExecutionEngine cem = CamundaExecutionEngineFixture.predefined();
        CamundaExecutionEngine cemSpy = Mockito.spy(cem);

        // Mocks
        CamundaInstantiationResponse responseBody = CamundaInstantiationResponse.builder()
                .id(UUID.randomUUID().toString())
                .definitionId(model.getModelReference() + ":1:" + UUID.randomUUID())
                .build();

        DpexHttpResponse mockResponse = new DpexHttpResponse(200, objectMapper.writeValueAsString(responseBody));

        doReturn(mockResponse).when(cemSpy).sendRequest(
                "http://localhost:8080/engine-rest/process-definition/key/" + model.getModelReference() + "/start",
                "{}");

        // Test
        Instance instance = cemSpy.instantiate(model.getModelReference(), "businessKey", "[{}]", "name");

        // Assert
        assertThat(instance.getLocalInstanceReference()).isEqualTo(responseBody.getId());
        assertThat(instance.getGlobalInstanceReference()).isNull();

    }

    @Test
    @Disabled
    public void isConformTest() throws JsonProcessingException {
        // Setup
        CamundaModel model = CamundaModelFixture.unitTestModel();

        CamundaExecutionEngine cem = CamundaExecutionEngineFixture.predefined();
        CamundaExecutionEngine cemSpy = Mockito.spy(cem);

        // Mocks
        TaskAction taskAction = new TaskAction();
        taskAction.setUser(UserFixture.predefined());
        taskAction.setTask(Task.builder().activity("X").build());
        taskAction.setInstance(InstanceFixture.predefined());

        GetWorkListResponse workListItem = GetWorkListResponse.builder()
                .id(UUID.randomUUID().toString())
                .name("X")
                .assignee("christian")
                .processInstanceId(taskAction.getInstance().getLocalInstanceReference())
                .build();
        DpexHttpResponse mockResponse = new DpexHttpResponse(200, objectMapper.writeValueAsString(List.of(workListItem)));

        doReturn(mockResponse).when(cemSpy).sendRequest(
                "http://localhost:8080/engine-rest/task",
                objectMapper.writeValueAsString(new GetWorkListDTO(taskAction.getInstance().getLocalInstanceReference(),  taskAction.getUser().getBpmId())));

        // Test
        boolean isConform = cemSpy.isConform(model, taskAction.getInstance(), taskAction);

        // Assert
        assertThat(isConform).isTrue();
    }

/*
    @Test
    public void executeTest() {

        CamundaExecutionEngine cem = CamundaExecutionEngineFixture.predefined();
        String processDefinitionId = "demo-process:1:51718d91-f849-11ec-99a3-005056c00001";
        String processInstanceId = "a7244a97-f853-11ec-99a3-005056c00001";
        String taskId = "93e48c5d-f855-11ec-99a3-005056c00001";

        Instance instance = new Instance(null, processInstanceId);

        cem.execute(instance, new TaskCompletion(instance, taskId));

    }*/


    @Test
    public void getWorkListTest() throws JsonProcessingException {

        // Setup
        CamundaExecutionEngine cem = CamundaExecutionEngineFixture.predefined();
        CamundaExecutionEngine cemSpy = Mockito.spy(cem);

        // Mocks
        String processInstanceId = UUID.randomUUID().toString();
        Instance instance = new Instance(processInstanceId, null, "name");

        GetWorkListResponse workListItem1 = GetWorkListResponse.builder()
                .id(UUID.randomUUID().toString())
                .name("X")
                .assignee("christian")
                .processInstanceId(processInstanceId)
                .build();
        GetWorkListResponse workListItem2 = GetWorkListResponse.builder()
                .id(UUID.randomUUID().toString())
                .name("Y")
                .assignee("stefan")
                .processInstanceId(processInstanceId)
                .build();

        DpexHttpResponse mockResponse = new DpexHttpResponse(200, objectMapper.writeValueAsString(List.of(workListItem1, workListItem2)));

        doReturn(mockResponse).when(cemSpy).sendRequest(
                "http://localhost:8080/engine-rest/task",
                objectMapper.writeValueAsString(new GetWorkListDTO(instance.getLocalInstanceReference(), null)));

        // Test
        List<Task> workList = cemSpy.getWorkList(instance);

        // Assert
        assertThat(workList.size()).isEqualTo(2);
        assertThat(workList).containsExactly(new Task("X", "christian"), new Task("Y", "stefan"));
    }

    @Test
    public void getWorkListByUserTest() throws JsonProcessingException {
    }

}
