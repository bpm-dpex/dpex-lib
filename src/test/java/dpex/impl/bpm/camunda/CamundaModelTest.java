package dpex.impl.bpm.camunda;

import dpex.bpm.model.ProcessModel;
import fixture.CamundaModelFixture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class CamundaModelTest {

    private static final Logger logger = LogManager.getLogger(CamundaModelTest.class);

    @Test
    public void getModelReferenceTest() {

        logger.error("Start test");

        ProcessModel demoProcessModel = CamundaModelFixture.demoProcessModel();
        assertEquals("demo-process", demoProcessModel.getModelReference());

        ProcessModel unitTestModel = CamundaModelFixture.unitTestModel();
        assertEquals("unit-test", unitTestModel.getModelReference());

    }

    @Test
    public void getParticipantsTest() {

       ProcessModel demoProcessModel =  CamundaModelFixture.demoProcessModel();
       assertNull(demoProcessModel.getParticipants());

        ProcessModel unitTestModel = CamundaModelFixture.unitTestModel();
        assertNull(unitTestModel.getParticipants());
    }

}
