package dpex.core;

import dpex.action.Instantiation;
import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.Instance;
import dpex.collaboration.Collaboration;
import dpex.collaboration.network.BaseNetwork;
import dpex.collaboration.security.Security;
import dpex.db.AllianceRepository;
import dpex.db.InstanceRepository;
import dpex.logger.Logger;
import fixture.InstantiationFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static dpex.logger.LogEntryObjectType.INSTANTIATION;
import static dpex.logger.LogEntryType.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DPEXServiceInstantiationTest {

    @InjectMocks
    DPEXService dpexService;

    @Mock
    AllianceRepository allianceRepository;

    @Mock
    InstanceRepository instanceRepository;

    @Mock
    Logger dpexLogger;

    Instance instanceMock;
    Instantiation instantiation;
    Alliance allianceSpy;
    BaseNetwork networkMock;
    Security securityMock;
    Collaboration collaborationMock;
    BPMEngine engineMock;

    /**
     * lenient() is used to avoid UnnecessaryStubbingException.
     * These mocks are required by the handleInstantiation-Test, but considered unnecessary in the sendInstantiation-Test
     */
    @BeforeEach
    public void setup() {
        String globalAllianceReference = "test-gar";

        // Mock Base classes of alliance object
        engineMock = Mockito.mock(BPMEngine.class, RETURNS_MOCKS);
        lenient().doReturn(new HashSet<>()).when(engineMock).getInstances();
        collaborationMock = Mockito.mock(Collaboration.class, Mockito.RETURNS_MOCKS);
        //lenient().doReturn(Set.of("http://localhost:8081/collab", "http://localhost:9081/collab")).when(collaborationMock).getRecipients();
        networkMock = Mockito.mock(BaseNetwork.class, Mockito.RETURNS_MOCKS);
        securityMock = Mockito.mock(Security.class, Mockito.RETURNS_MOCKS);

        // Create alliance object with spy object (so that getters of alliance object can be mocked. we cannot inject mocks into the alliance object itself)
        Alliance alliance = new Alliance();
        alliance.setGlobalAllianceReference(globalAllianceReference);
        alliance.setCollaboration(collaborationMock);

        allianceSpy = Mockito.spy(alliance);

        // define the mocked return objects for required alliance getters
        lenient().doReturn(engineMock).when(allianceSpy).getEngine();
//        doReturn(networkMock).when(allianceSpy).getNetwork();
        doReturn(securityMock).when(allianceSpy).getSecurity();

        // mock the allianceRepository
        lenient().when(allianceRepository.findByGlobalAllianceReference(globalAllianceReference)).thenReturn(Optional.of(allianceSpy));

        // further mocks
        instanceMock = new Instance();
        instanceMock.setLocalInstanceReference("local is set");
        lenient().when(engineMock.instantiate("demo-model", "test-key", "[{}]", "name")).thenReturn(instanceMock);
        lenient().when(instanceRepository.save(instanceMock)).thenReturn(instanceMock);

        instantiation = InstantiationFixture.predefined();
    }

    @Test
    public void sendInstantiationTest() {

        instantiation.setGlobalInstanceReference(null);
        Instantiation instantiationAfter = InstantiationFixture.predefined();

        //when(collaborationMock.init(instantiation)).thenReturn(instantiationAfter);

        // test
        dpexService.sendInstantiation(allianceSpy, instantiation);

        verify(dpexLogger).log(allianceSpy, SEND_INSTANTIATION, INSTANTIATION, instantiation.getGlobalInstanceReference(),  "Send Instantiation");

        // initialization issues are handled, e.g. contract creation
        //verify(collaborationMock, times(1)).init(instantiation);

        // security concerns are executed. instantiationAfter is used, because at this time, collaboration.init() should have been executed
        verify(securityMock, times(1)).sign(instantiationAfter);

        // we have to use eq(..) here, because we need to use anyString() and consequently, all arguments have to be provided by matchers (according to Mockito)
        //verify(networkMock, times(collaborationMock.getRecipients().size())).sendInstantiation(eq(instantiationAfter), Mockito.anyString(), eq(""));

    }

    //TODO: Repair Test
    @Test
    @Disabled
    public void connectorUrlIsSetInSendInstantiation() throws MalformedURLException {

        Instantiation instantiationAfter = InstantiationFixture.predefined();
        //when(collaborationMock.init(instantiation)).thenReturn(instantiationAfter);

        //when(collaborationMock.getConnection()).thenReturn(new URL("http://localhost:8545"));

        // start test
        dpexService.sendInstantiation(allianceSpy, instantiation);

        // connection url is set, when collaboration object defines a connection
        //verify(networkMock, times(collaborationMock.getRecipients().size())).sendInstantiation(eq(instantiationAfter), Mockito.anyString(), eq("http://localhost:8545"));

    }

    //TODO: Repair test
    @Test
    @Disabled
    public void instantiationTest() {

        when(collaborationMock.getConnection()).thenReturn(null);

        // start test
        dpexService.handleInstantiation(instantiation);

        // assertions
        // Logger is called
        verify(dpexLogger, times(1)).log(allianceSpy, HANDLE_INSTANTIATION, INSTANTIATION, instantiation.getGlobalInstanceReference(), "Handle Instantiation");

        // network::handleInstantiation is called
        //TODO: listen to ethereum events. should this be located in collaboration object instead of network object?
        verify(networkMock, times(1)).handleInstantiation(instantiation);

        // security concerns are checked
        verify(securityMock, times(1)).verify(instantiation);

        // engine is called to instantiate model
        verify(engineMock, times(1)).instantiate("demo-model", "test-key", "[{}]", "name");

        // TODO: TEST localInstanceReference is set OR setGlobalInstanceRef is called

        // new instance is persisted
        verify(instanceRepository, times(1)).save(instanceMock);

        // new instance is added to the respective engine
        assertThat(engineMock.getInstances()).contains(instanceMock);
        verify(allianceRepository, times(1)).save(allianceSpy);

        verify(dpexLogger, times(1)).log(allianceSpy, INSTANCE_CREATED, INSTANTIATION, instantiation.getGlobalInstanceReference(),  "Instance created");

    }

    //TODO: Repair test
  /*
    @Test
    @Disabled
    public void connectorUrlIsSetInHandleInstantiation() throws MalformedURLException {

       // when(collaborationMock.getConnection()).thenReturn(new URL("http://localhost:8545"));

        // start test
       //  dpexService.handleInstantiation(instantiation);

        // connection url is set, when collaboration object defines a connection
       // verify(networkMock, times(1)).handleInstantiation(instantiation);
    }
*/

}
