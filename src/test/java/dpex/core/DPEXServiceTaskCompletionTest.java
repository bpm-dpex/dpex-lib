package dpex.core;

import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.*;
import dpex.collaboration.Collaboration;
import dpex.collaboration.network.BaseNetwork;
import dpex.collaboration.security.Security;
import dpex.db.AllianceRepository;
import dpex.db.InstanceRepository;
import dpex.logger.Logger;
import fixture.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.MalformedURLException;
import java.net.URL;

import static dpex.logger.LogEntryObjectType.TASK_COMPLETE;
import static dpex.logger.LogEntryType.SEND_TASK_COMPLETE;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DPEXServiceTaskCompletionTest {

    @InjectMocks
    DPEXService dpexService;

    @Mock
    AllianceRepository allianceRepository;

    @Mock
    InstanceRepository instanceRepository;

    @Mock
    Logger dpexLogger;

    User user = UserFixture.predefined();
    Task task = TaskFixture.predefined();

    ProcessVariables processVariables = ProcessVariablesFixture.predefined();

    TaskAction taskAction;

    Instance instance = InstanceFixture.predefined();
    Instantiation instantiation;
    Alliance allianceSpy;
    BaseNetwork networkMock;
    Security securityMock;
    Collaboration collaborationMock;
    BPMEngine engineMock;

    @BeforeEach
    public void setup() throws ClassNotFoundException {

        String globalAllianceReference = "test-gar";

        // Mock Base classes of alliance object
        networkMock = Mockito.mock(BaseNetwork.class, Mockito.RETURNS_MOCKS);
        securityMock = Mockito.mock(Security.class, Mockito.RETURNS_MOCKS);
        engineMock = Mockito.mock(BPMEngine.class, RETURNS_MOCKS);
        collaborationMock = Mockito.mock(Collaboration.class, Mockito.RETURNS_MOCKS);

        taskAction = TaskActionFixture.predefined();

        // Create alliance object with spy object (so that getters of alliance object can be mocked. we cannot inject mocks into the alliance object itself)

        Alliance alliance = new Alliance();
        alliance.setGlobalAllianceReference(globalAllianceReference);
        alliance.setCollaboration(collaborationMock);
        alliance.setEngine(engineMock);

        allianceSpy = Mockito.spy(alliance);

        doReturn(securityMock).when(allianceSpy).getSecurity();
        doReturn(networkMock).when(allianceSpy).getCollaboration().getNetwork();


    }

    @Test
    @Disabled
    public void sendTaskActionTest() throws MalformedURLException {

        when(securityMock.sign(taskAction)).thenReturn("Valid Signature");
        when(collaborationMock.getConnection()).thenReturn(new URL("http://localhost:80"));
        // Only conform actions are sent to participants
        doReturn(true).when(engineMock).isConform(allianceSpy.getModel(), instance, taskAction);

        dpexService.sendTaskAction(allianceSpy, instance, user, task,  processVariables, TaskLifeCycleStage.COMPLETE);

        verify(dpexLogger).log(allianceSpy, SEND_TASK_COMPLETE, TASK_COMPLETE, task.getActivity(),  "Send Task Execution");

        taskAction.setUser(user);

        verify(securityMock, times(1)).sign(taskAction);
        //verify(networkMock, times(1)).sendTaskAction(taskAction, collaborationMock.getRecipients() /*rec + "action"*/, allianceSpy.getCollaboration().getConnection().toString());

    }

}
