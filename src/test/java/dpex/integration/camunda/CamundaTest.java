package dpex.integration.camunda;

import dpex.action.Instantiation;
import dpex.action.TaskAction;
import dpex.bpm.execution.BPMEngine;
import dpex.bpm.execution.Instance;
import dpex.bpm.execution.Task;
import dpex.bpm.execution.TaskLifeCycleStage;
import dpex.bpm.model.ProcessModel;
import dpex.collaboration.Collaboration;
import dpex.collaboration.security.Signature;
import dpex.core.Alliance;
import dpex.core.DPEXService;
import dpex.core.User;
import dpex.db.*;
import fixture.CamundaExecutionEngineFixture;
import fixture.CamundaModelFixture;
import fixture.SimpleCollaborationFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * It is important that a camunda instance is running...
 */
@SpringBootTest
@Disabled
public class CamundaTest {

    @Autowired
    ProcessModelRepository modelRepository;

    @Autowired
    BPMEngineRepository executionMachineRepository;

    @Autowired
    CollaborationRepository collaborationRepository;

    @Autowired
    AllianceRepository allianceRepository;

    @Autowired
    DPEXService dpexService;

    Alliance alliance;

    @BeforeEach
    public void setup() throws IOException, URISyntaxException {
        // 1. Create and store a Model
        ProcessModel model = CamundaModelFixture.unitTestModel();
        //model.setParticipants(Set.of("http://localhost:4000"));
        modelRepository.save(model);
        assertThat(modelRepository.count()).isEqualTo(1L);

        // 1b)
        BPMEngine engine = CamundaExecutionEngineFixture.predefined();
        executionMachineRepository.save(engine);

        Collaboration collaboration = SimpleCollaborationFixture.predefined();
        collaborationRepository.save(collaboration);

        // 2. Instantiate a model
        // i.e. notify all others and agree on global identifier
        // DECIDE: A) everybody must manually persist the respective ProcessModel
        //  B) the model is sent with this step

        alliance = new Alliance();
        alliance.setName("Test Alliance.");
        ProcessModel storedModel = modelRepository.findById("unit-test").get();
        //alliance.setModel(storedModel);
        BPMEngine storedEngine = executionMachineRepository.findById(1L).get();
        alliance.setEngine(storedEngine);
        Collaboration storedCollaboration = collaborationRepository.findById(1L).get();
        //alliance.setCollaboration(storedCollaboration);
        alliance = allianceRepository.save(alliance);
        collaborationRepository.save(storedCollaboration);

    }

    @Test
    @Disabled
    public void test() throws URISyntaxException, IOException {

        ProcessModel demoProcessModel = CamundaModelFixture.demoProcessModel();
        modelRepository.save(demoProcessModel);
        assertThat(modelRepository.findById("unit-test").get().getModelReference()).isEqualTo("unit-test");
    }



    @Test
    public void receiveInstantiationTest() {

        Instantiation instantiation = new Instantiation();
        instantiation.setGlobalInstanceReference("global");
        instantiation.setBusinessKey("CCR");
        //alliance.getCollaboration().instantiationReceived(instantiation);

    }



    /*
    @Test
    public void receiveTaskExecutionTest() {

        //instantiate
        Instantiation instantiation = new Instantiation();
        instantiation.setModelReference(new ModelReference("demo-process"));
        instantiation.setInstance(new Instance("local", "global"));
        instantiation.setBusinessKey("CCR");
        //Instance instance = alliance.getCollaboration().instantiationReceived(instantiation);


        TaskCompletion taskExecution = new TaskCompletion(instance, "A");
        User user = new User();
        user.setPrivateKey("");
        user.setPublicKey("");
        UserReference userReference = new UserReference();
        userReference.setName("Name");
        user.setUserReference(userReference);
        taskExecution.setUser(user);
        Signature signature = new Signature();
        signature.setSignature("");
        taskExecution.setSender(signature);

        alliance.getCollaboration().taskExecutionReceived(taskExecution);

    }*/

    @Test
    public void getWorkListTest() {
        /*Instantiation instantiation = new Instantiation();
        instantiation.setModelReference(new ModelReference("demo-process"));
        instantiation.setInstance(new Instance("local", "global"));
        instantiation.setBusinessKey("CCR");
        this.dpexService.hanldeInstantiation(instantiation);*/

        Instance instance = new Instance();
        instance.setLocalInstanceReference("dd0e4523-4250-11ed-9e59-005056c00001");

        String userReference = "christian";

        alliance.getEngine().getWorkListByUser(instance, userReference);
    }


    @Test
    public void handleTaskExecutionTest() throws URISyntaxException, IOException {


        Instantiation instantiation = new Instantiation();
        instantiation.setModelReference("unit-test");
        instantiation.setBusinessKey("b-key");

        dpexService.handleInstantiation(instantiation);

        Alliance storedAlliance = allianceRepository.findById("test alliance").get();

        Instance instance = storedAlliance.getEngine().getInstances().stream().findAny().get();

        TaskAction taskAction = new TaskAction(instance, Task.builder().activity("A").build(),null,  TaskLifeCycleStage.COMPLETE);
        User user = new User();
        user.setSciId("");
        user.setBpmId("Name");
        taskAction.setUser(user);
        Signature signature = new Signature();
        signature.setSignature("");
        taskAction.setSender(signature);

        dpexService.handleTaskAction(taskAction);




    }
}
