package fixture;

import dpex.impl.bpm.camunda.CamundaExecutionEngine;
import dpex.impl.bpm.camunda.CamundaModel;

public class CamundaExecutionEngineFixture {

    public static CamundaExecutionEngine.CamundaExecutionEngineBuilder builder() {
        return CamundaExecutionEngine.builder()
                .camundaUrl("http://localhost:8080/engine-rest/");
    }

    public static CamundaExecutionEngine predefined() {
        return CamundaExecutionEngineFixture.builder().build();
    }

}
