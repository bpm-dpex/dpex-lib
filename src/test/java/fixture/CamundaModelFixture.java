package fixture;

import dpex.core.Alliance;
import dpex.impl.bpm.camunda.CamundaModel;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CamundaModelFixture {

    public static CamundaModel.CamundaModelBuilder builder() {
        return CamundaModel.builder();
    }

    public static CamundaModel predefined() {
        return CamundaModelFixture.builder().build();
    }

    public static CamundaModel unitTestModel() {
        try {
            CamundaModel model = CamundaModelFixture.builder()
                    .bpmnModel( Files.readAllBytes(Paths.get(CamundaModelFixture.class.getClassLoader().getResource("unit-test.bpmn").toURI())))
                    .build();
            model.setModelReference("unit-test");
            return model;
        } catch (Exception e) {
            throw new RuntimeException("Error while loading CamundaFixture:" + e.getMessage());
        }
    }

    public static CamundaModel demoProcessModel() {
        try {
            CamundaModel model = CamundaModelFixture.builder()
                    .bpmnModel( Files.readAllBytes(Paths.get(CamundaModelFixture.class.getClassLoader().getResource("demo-process.bpmn").toURI())))
                    .build();
            model.setModelReference("demo-process");
            return model;
        } catch (Exception e) {
            throw new RuntimeException("Error while loading CamundaFixture:" + e.getMessage());
        }
    }

}
