package fixture;

import dpex.bpm.execution.ProcessVariables;


/**
 *
 * @author Tobias Gretsch
 * @version v.0.0.11
 */
public class ProcessVariablesFixture {

    public static ProcessVariables.ProcessVariablesBuilder builder() {
        return ProcessVariables.builder();
    }

    public static ProcessVariables predefined() {
        return ProcessVariablesFixture.builder().build();
    }
}
