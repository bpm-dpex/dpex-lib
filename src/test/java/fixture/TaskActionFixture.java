package fixture;

import dpex.action.TaskAction;

public class TaskActionFixture {

    public static TaskAction.TaskActionBuilder builder() {
        return TaskAction.builder();
    }

    public static TaskAction predefined() {
        return (TaskAction) TaskActionFixture.builder()
                .instance(InstanceFixture.predefined())
                .task(TaskFixture.predefined())
                .user(UserFixture.predefined())
                .build();
    }

}
