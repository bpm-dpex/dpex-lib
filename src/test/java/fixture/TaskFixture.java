package fixture;

import dpex.bpm.execution.Task;


public class TaskFixture {

    public static Task.TaskBuilder builder() {
        return Task.builder();
    }

    public static Task predefined() {
        return TaskFixture.builder()
                .activity("Example Task")
                .resource("X")
                .build();
    }

}
