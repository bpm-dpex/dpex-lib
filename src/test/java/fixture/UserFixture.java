package fixture;

import dpex.core.User;

public class UserFixture {

    public static User.UserBuilder builder() {
        return User.builder();
    }

    public static User predefined() {
        return UserFixture.builder()
                .sciId("0xPublic")
                .bpmId("christian")
                .build();
    }

}
