package fixture;

import dpex.action.Instantiation;

public class InstantiationFixture {

    public static Instantiation.InstantiationBuilder builder() {
        return Instantiation.builder();
    }

    public static Instantiation predefined() {
        return InstantiationFixture.builder()
                .globalAllianceReference("test-gar")
                .modelReference("demo-model")
                .globalInstanceReference("globalInstanceReference")
                .businessKey("test-key")
                .build();
    }

}
