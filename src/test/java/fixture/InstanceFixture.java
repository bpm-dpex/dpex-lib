package fixture;

import dpex.bpm.execution.Instance;

public class InstanceFixture {

    public static Instance.InstanceBuilder builder() {
        return Instance.builder();
    }

    public static Instance predefined() {
        return InstanceFixture.builder()
                .localInstanceReference("local")
                .globalInstanceReference("global")
                .build();
    }

}
