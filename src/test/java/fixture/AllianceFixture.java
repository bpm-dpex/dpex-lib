package fixture;

import dpex.core.Alliance;

public class AllianceFixture {

    public static Alliance.AllianceBuilder builder() {
        return Alliance.builder();
    }

    public static Alliance predefined() {
        return AllianceFixture.builder().build();
    }

}
