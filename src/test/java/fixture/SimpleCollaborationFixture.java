package fixture;

import dpex.impl.collaboration.SimpleCollaboration;

import java.net.MalformedURLException;
import java.net.URL;

public class SimpleCollaborationFixture {

    public static SimpleCollaboration.SimpleCollaborationBuilder builder() {
        return SimpleCollaboration.builder()
                .network_port(3000)
                .consensus_numberOfAgreementsRequired(1);


    }

    public static SimpleCollaboration predefined() {
        SimpleCollaboration fixture = SimpleCollaborationFixture.builder().build();
        try {
            fixture.setConnection(new URL("http://localhost:3001"));
            return fixture;
        } catch (Exception e) {
            throw new RuntimeException("Error while loading SimpleCollaboration:" + e.getMessage());
        }
    }

}
